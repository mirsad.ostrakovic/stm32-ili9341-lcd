	.syntax unified
	.cpu cortex-m4
	.eabi_attribute 27, 3
	.eabi_attribute 28, 1
	.fpu fpv4-sp-d16
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 1
	.eabi_attribute 30, 6
	.eabi_attribute 34, 1
	.eabi_attribute 18, 4
	.thumb
	.file	"print.c"
	.text
.Ltext0:
	.cfi_sections	.debug_frame
	.align	2
	.global	print_integer
	.thumb
	.thumb_func
	.type	print_integer, %function
print_integer:
.LFB110:
	.file 1 "print.c"
	.loc 1 4 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r7, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	sub	sp, sp, #48
	.cfi_def_cfa_offset 56
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	str	r0, [r7, #4]
	.loc 1 6 0
	mov	r3, #-1
	str	r3, [r7, #44]
	.loc 1 9 0
	ldr	r3, [r7, #4]
	cmp	r3, #0
	bne	.L2
	.loc 1 11 0
	movs	r0, #48
	bl	print_char
	b	.L1
.L2:
	.loc 1 15 0
	ldr	r3, [r7, #4]
	cmp	r3, #0
	bge	.L4
	.loc 1 17 0
	ldr	r3, [r7, #44]
	adds	r3, r3, #1
	str	r3, [r7, #44]
	add	r2, r7, #8
	ldr	r3, [r7, #44]
	add	r3, r3, r2
	movs	r2, #45
	strb	r2, [r3]
	.loc 1 18 0
	ldr	r3, [r7, #4]
	negs	r3, r3
	str	r3, [r7, #4]
.L4:
	.loc 1 21 0
	ldr	r3, [r7, #4]
	str	r3, [r7, #40]
	.loc 1 23 0
	b	.L5
.L6:
	.loc 1 25 0
	ldr	r3, [r7, #40]
	ldr	r2, .L9
	smull	r1, r2, r2, r3
	asrs	r2, r2, #2
	asrs	r3, r3, #31
	subs	r3, r2, r3
	str	r3, [r7, #40]
	.loc 1 26 0
	ldr	r3, [r7, #44]
	adds	r3, r3, #1
	str	r3, [r7, #44]
.L5:
	.loc 1 23 0
	ldr	r3, [r7, #40]
	cmp	r3, #0
	bne	.L6
	.loc 1 29 0
	ldr	r3, [r7, #44]
	adds	r3, r3, #1
	add	r2, r7, #48
	add	r3, r3, r2
	movs	r2, #0
	strb	r2, [r3, #-40]
	.loc 1 31 0
	b	.L7
.L8:
	.loc 1 33 0
	ldr	r0, [r7, #44]
	subs	r3, r0, #1
	str	r3, [r7, #44]
	ldr	r2, [r7, #4]
	ldr	r3, .L9
	smull	r1, r3, r3, r2
	asrs	r1, r3, #2
	asrs	r3, r2, #31
	subs	r1, r1, r3
	mov	r3, r1
	lsls	r3, r3, #2
	add	r3, r3, r1
	lsls	r3, r3, #1
	subs	r1, r2, r3
	uxtb	r3, r1
	adds	r3, r3, #48
	uxtb	r2, r3
	add	r3, r7, #48
	add	r3, r3, r0
	strb	r2, [r3, #-40]
	.loc 1 34 0
	ldr	r3, [r7, #4]
	ldr	r2, .L9
	smull	r1, r2, r2, r3
	asrs	r2, r2, #2
	asrs	r3, r3, #31
	subs	r3, r2, r3
	str	r3, [r7, #4]
.L7:
	.loc 1 31 0
	ldr	r3, [r7, #4]
	cmp	r3, #0
	bne	.L8
	.loc 1 37 0
	add	r3, r7, #8
	mov	r0, r3
	bl	print_string
.L1:
	.loc 1 39 0
	adds	r7, r7, #48
	.cfi_def_cfa_offset 8
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	pop	{r7, pc}
.L10:
	.align	2
.L9:
	.word	1717986919
	.cfi_endproc
.LFE110:
	.size	print_integer, .-print_integer
	.align	2
	.global	print_hex
	.thumb
	.thumb_func
	.type	print_hex, %function
print_hex:
.LFB111:
	.loc 1 42 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r7, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	sub	sp, sp, #16
	.cfi_def_cfa_offset 24
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	str	r0, [r7, #4]
	.loc 1 44 0
	movs	r3, #8
	str	r3, [r7, #12]
	.loc 1 46 0
	movs	r0, #48
	bl	print_char
	.loc 1 47 0
	movs	r0, #120
	bl	print_char
	.loc 1 49 0
	b	.L12
.L13:
	.loc 1 50 0 discriminator 2
	ldr	r3, [r7, #12]
	lsls	r3, r3, #2
	ldr	r2, [r7, #4]
	asr	r3, r2, r3
	and	r3, r3, #15
	ldr	r2, .L14
	ldrb	r3, [r2, r3]	@ zero_extendqisi2
	mov	r0, r3
	bl	print_char
	.loc 1 49 0 discriminator 2
	ldr	r3, [r7, #12]
	subs	r3, r3, #1
	str	r3, [r7, #12]
.L12:
	.loc 1 49 0 is_stmt 0 discriminator 1
	ldr	r3, [r7, #12]
	cmp	r3, #0
	bge	.L13
	.loc 1 52 0 is_stmt 1
	adds	r7, r7, #16
	.cfi_def_cfa_offset 8
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	pop	{r7, pc}
.L15:
	.align	2
.L14:
	.word	hex_map.5401
	.cfi_endproc
.LFE111:
	.size	print_hex, .-print_hex
	.align	2
	.global	print_string
	.thumb
	.thumb_func
	.type	print_string, %function
print_string:
.LFB112:
	.loc 1 56 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r7, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	sub	sp, sp, #8
	.cfi_def_cfa_offset 16
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	str	r0, [r7, #4]
	.loc 1 57 0
	b	.L17
.L18:
	.loc 1 58 0
	ldr	r3, [r7, #4]
	adds	r2, r3, #1
	str	r2, [r7, #4]
	ldrb	r3, [r3]	@ zero_extendqisi2
	mov	r0, r3
	bl	print_char
.L17:
	.loc 1 57 0
	ldr	r3, [r7, #4]
	ldrb	r3, [r3]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L18
	.loc 1 59 0
	adds	r7, r7, #8
	.cfi_def_cfa_offset 8
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	pop	{r7, pc}
	.cfi_endproc
.LFE112:
	.size	print_string, .-print_string
	.align	2
	.global	print_terminal
	.thumb
	.thumb_func
	.type	print_terminal, %function
print_terminal:
.LFB113:
	.loc 1 63 0
	.cfi_startproc
	@ args = 4, pretend = 16, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 1
	push	{r0, r1, r2, r3}
	.cfi_def_cfa_offset 16
	.cfi_offset 0, -16
	.cfi_offset 1, -12
	.cfi_offset 2, -8
	.cfi_offset 3, -4
	push	{r7, lr}
	.cfi_def_cfa_offset 24
	.cfi_offset 7, -24
	.cfi_offset 14, -20
	sub	sp, sp, #8
	.cfi_def_cfa_offset 32
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	.loc 1 66 0
	add	r3, r7, #20
	str	r3, [r7]
	.loc 1 68 0
	movs	r3, #0
	strb	r3, [r7, #7]
.L32:
	.loc 1 72 0
	ldr	r3, [r7, #16]
	ldrb	r3, [r3]	@ zero_extendqisi2
	cmp	r3, #37
	beq	.L21
	cmp	r3, #37
	bgt	.L22
	cmp	r3, #0
	beq	.L34
	b	.L20
.L22:
	cmp	r3, #100
	beq	.L24
	cmp	r3, #120
	beq	.L25
	b	.L20
.L21:
	.loc 1 79 0
	movs	r3, #1
	strb	r3, [r7, #7]
	.loc 1 80 0
	b	.L27
.L24:
	.loc 1 83 0
	ldrb	r3, [r7, #7]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L28
	.loc 1 85 0
	ldr	r3, [r7]
	adds	r2, r3, #4
	str	r2, [r7]
	ldr	r3, [r3]
	mov	r0, r3
	bl	print_integer
	.loc 1 86 0
	movs	r3, #0
	strb	r3, [r7, #7]
	.loc 1 91 0
	b	.L27
.L28:
	.loc 1 89 0
	movs	r0, #100
	bl	print_char
	.loc 1 91 0
	b	.L27
.L25:
	.loc 1 94 0
	ldrb	r3, [r7, #7]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L30
	.loc 1 96 0
	ldr	r3, [r7]
	adds	r2, r3, #4
	str	r2, [r7]
	ldr	r3, [r3]
	mov	r0, r3
	bl	print_hex
	.loc 1 97 0
	movs	r3, #0
	strb	r3, [r7, #7]
	.loc 1 101 0
	b	.L27
.L30:
	.loc 1 100 0
	movs	r0, #120
	bl	print_char
	.loc 1 101 0
	b	.L27
.L20:
	.loc 1 104 0
	movs	r3, #0
	strb	r3, [r7, #7]
	.loc 1 105 0
	ldr	r3, [r7, #16]
	ldrb	r3, [r3]	@ zero_extendqisi2
	mov	r0, r3
	bl	print_char
.L27:
	.loc 1 108 0
	ldr	r3, [r7, #16]
	adds	r3, r3, #1
	str	r3, [r7, #16]
	.loc 1 109 0
	b	.L32
.L34:
	.loc 1 76 0
	nop
	.loc 1 110 0
	adds	r7, r7, #8
	.cfi_def_cfa_offset 24
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	pop	{r7, lr}
	.cfi_restore 14
	.cfi_restore 7
	.cfi_def_cfa_offset 16
	add	sp, sp, #16
	.cfi_restore 3
	.cfi_restore 2
	.cfi_restore 1
	.cfi_restore 0
	.cfi_def_cfa_offset 0
	bx	lr
	.cfi_endproc
.LFE113:
	.size	print_terminal, .-print_terminal
	.align	2
	.global	init_UART4
	.thumb
	.thumb_func
	.type	init_UART4, %function
init_UART4:
.LFB114:
	.loc 1 118 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	push	{r7}
	.cfi_def_cfa_offset 4
	.cfi_offset 7, -4
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	.loc 1 121 0
	ldr	r2, .L36
	ldr	r3, .L36
	ldr	r3, [r3, #48]
	orr	r3, r3, #4
	str	r3, [r2, #48]
	.loc 1 124 0
	ldr	r2, .L36
	ldr	r3, .L36
	ldr	r3, [r3, #64]
	orr	r3, r3, #524288
	str	r3, [r2, #64]
	.loc 1 127 0
	ldr	r2, .L36+4
	ldr	r3, .L36+4
	ldr	r3, [r3]
	bic	r3, r3, #15728640
	str	r3, [r2]
	.loc 1 128 0
	ldr	r2, .L36+4
	ldr	r3, .L36+4
	ldr	r3, [r3]
	orr	r3, r3, #10485760
	str	r3, [r2]
	.loc 1 130 0
	ldr	r1, .L36+4
	ldr	r3, .L36+4
	ldr	r2, [r3, #4]
	movw	r3, #62463
	ands	r3, r3, r2
	str	r3, [r1, #4]
	.loc 1 132 0
	ldr	r2, .L36+4
	ldr	r3, .L36+4
	ldr	r3, [r3, #8]
	bic	r3, r3, #15728640
	str	r3, [r2, #8]
	.loc 1 133 0
	ldr	r2, .L36+4
	ldr	r3, .L36+4
	ldr	r3, [r3, #8]
	orr	r3, r3, #10485760
	str	r3, [r2, #8]
	.loc 1 135 0
	ldr	r2, .L36+4
	ldr	r3, .L36+4
	ldr	r3, [r3, #12]
	bic	r3, r3, #15728640
	str	r3, [r2, #12]
	.loc 1 136 0
	ldr	r2, .L36+4
	ldr	r3, .L36+4
	ldr	r3, [r3, #12]
	orr	r3, r3, #5242880
	str	r3, [r2, #12]
	.loc 1 138 0
	ldr	r2, .L36+4
	ldr	r3, .L36+4
	ldr	r3, [r3, #36]
	bic	r3, r3, #65280
	str	r3, [r2, #36]
	.loc 1 139 0
	ldr	r2, .L36+4
	ldr	r3, .L36+4
	ldr	r3, [r3, #36]
	orr	r3, r3, #34816
	str	r3, [r2, #36]
	.loc 1 143 0
	ldr	r2, .L36+8
	ldr	r3, .L36+8
	ldrh	r3, [r3, #12]	@ movhi
	uxth	r3, r3
	orr	r3, r3, #8192
	orr	r3, r3, #8
	uxth	r3, r3
	strh	r3, [r2, #12]	@ movhi
	.loc 1 148 0
	ldr	r3, .L36+8
	mov	r2, #364
	strh	r2, [r3, #8]	@ movhi
	.loc 1 150 0
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	ldr	r7, [sp], #4
	.cfi_restore 7
	.cfi_def_cfa_offset 0
	bx	lr
.L37:
	.align	2
.L36:
	.word	1073887232
	.word	1073874944
	.word	1073761280
	.cfi_endproc
.LFE114:
	.size	init_UART4, .-init_UART4
	.align	2
	.global	print_char
	.thumb
	.thumb_func
	.type	print_char, %function
print_char:
.LFB115:
	.loc 1 153 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	push	{r7}
	.cfi_def_cfa_offset 4
	.cfi_offset 7, -4
	sub	sp, sp, #12
	.cfi_def_cfa_offset 16
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	mov	r3, r0
	strb	r3, [r7, #7]
	.loc 1 155 0
	nop
.L39:
	.loc 1 155 0 is_stmt 0 discriminator 1
	ldr	r3, .L40
	ldrh	r3, [r3]	@ movhi
	uxth	r3, r3
	and	r3, r3, #64
	cmp	r3, #0
	beq	.L39
	.loc 1 156 0 is_stmt 1
	ldr	r3, .L40
	ldrb	r2, [r7, #7]	@ zero_extendqisi2
	uxth	r2, r2
	strh	r2, [r3, #4]	@ movhi
	.loc 1 157 0
	adds	r7, r7, #12
	.cfi_def_cfa_offset 4
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	ldr	r7, [sp], #4
	.cfi_restore 7
	.cfi_def_cfa_offset 0
	bx	lr
.L41:
	.align	2
.L40:
	.word	1073761280
	.cfi_endproc
.LFE115:
	.size	print_char, .-print_char
	.section	.rodata
	.align	2
	.type	hex_map.5401, %object
	.size	hex_map.5401, 17
hex_map.5401:
	.ascii	"0123456789ABCDEF\000"
	.text
.Letext0:
	.file 2 "/home/sharpovici-ostrakovici/stm_dev/STM32F407/gcc-arm-none-eabi/arm-none-eabi/include/machine/_default_types.h"
	.file 3 "/home/sharpovici-ostrakovici/stm_dev/STM32F407/gcc-arm-none-eabi/arm-none-eabi/include/stdint.h"
	.file 4 "stm32f4xx.h"
	.file 5 "/home/sharpovici-ostrakovici/stm_dev/STM32F407/gcc-arm-none-eabi/lib/gcc/arm-none-eabi/4.9.3/include/stdarg.h"
	.file 6 "<built-in>"
	.file 7 "../../../STM32F407/Libraries/CMSIS/Include/core_cm4.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x56c
	.2byte	0x4
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF65
	.byte	0x1
	.4byte	.LASF66
	.4byte	.LASF67
	.4byte	.Ltext0
	.4byte	.Letext0-.Ltext0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF2
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x2
	.byte	0x2b
	.4byte	0x45
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x3f
	.4byte	0x57
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF6
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x2
	.byte	0x41
	.4byte	0x69
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF9
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF11
	.uleb128 0x3
	.4byte	.LASF12
	.byte	0x3
	.byte	0x21
	.4byte	0x3a
	.uleb128 0x3
	.4byte	.LASF13
	.byte	0x3
	.byte	0x2c
	.4byte	0x4c
	.uleb128 0x3
	.4byte	.LASF14
	.byte	0x3
	.byte	0x2d
	.4byte	0x5e
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF15
	.uleb128 0x5
	.4byte	0xa2
	.uleb128 0x6
	.4byte	0xa2
	.4byte	0xc9
	.uleb128 0x7
	.4byte	0xad
	.byte	0x1
	.byte	0
	.uleb128 0x5
	.4byte	0x8c
	.uleb128 0x5
	.4byte	0x97
	.uleb128 0x8
	.byte	0x28
	.byte	0x4
	.2byte	0x28e
	.4byte	0x15f
	.uleb128 0x9
	.4byte	.LASF16
	.byte	0x4
	.2byte	0x290
	.4byte	0xb4
	.byte	0
	.uleb128 0x9
	.4byte	.LASF17
	.byte	0x4
	.2byte	0x291
	.4byte	0xb4
	.byte	0x4
	.uleb128 0x9
	.4byte	.LASF18
	.byte	0x4
	.2byte	0x292
	.4byte	0xb4
	.byte	0x8
	.uleb128 0x9
	.4byte	.LASF19
	.byte	0x4
	.2byte	0x293
	.4byte	0xb4
	.byte	0xc
	.uleb128 0xa
	.ascii	"IDR\000"
	.byte	0x4
	.2byte	0x294
	.4byte	0xb4
	.byte	0x10
	.uleb128 0xa
	.ascii	"ODR\000"
	.byte	0x4
	.2byte	0x295
	.4byte	0xb4
	.byte	0x14
	.uleb128 0x9
	.4byte	.LASF20
	.byte	0x4
	.2byte	0x296
	.4byte	0xc9
	.byte	0x18
	.uleb128 0x9
	.4byte	.LASF21
	.byte	0x4
	.2byte	0x297
	.4byte	0xc9
	.byte	0x1a
	.uleb128 0x9
	.4byte	.LASF22
	.byte	0x4
	.2byte	0x298
	.4byte	0xb4
	.byte	0x1c
	.uleb128 0xa
	.ascii	"AFR\000"
	.byte	0x4
	.2byte	0x299
	.4byte	0x15f
	.byte	0x20
	.byte	0
	.uleb128 0x5
	.4byte	0xb9
	.uleb128 0xb
	.4byte	.LASF23
	.byte	0x4
	.2byte	0x29a
	.4byte	0xd3
	.uleb128 0x8
	.byte	0x88
	.byte	0x4
	.2byte	0x2dd
	.4byte	0x2ff
	.uleb128 0xa
	.ascii	"CR\000"
	.byte	0x4
	.2byte	0x2df
	.4byte	0xb4
	.byte	0
	.uleb128 0x9
	.4byte	.LASF24
	.byte	0x4
	.2byte	0x2e0
	.4byte	0xb4
	.byte	0x4
	.uleb128 0x9
	.4byte	.LASF25
	.byte	0x4
	.2byte	0x2e1
	.4byte	0xb4
	.byte	0x8
	.uleb128 0xa
	.ascii	"CIR\000"
	.byte	0x4
	.2byte	0x2e2
	.4byte	0xb4
	.byte	0xc
	.uleb128 0x9
	.4byte	.LASF26
	.byte	0x4
	.2byte	0x2e3
	.4byte	0xb4
	.byte	0x10
	.uleb128 0x9
	.4byte	.LASF27
	.byte	0x4
	.2byte	0x2e4
	.4byte	0xb4
	.byte	0x14
	.uleb128 0x9
	.4byte	.LASF28
	.byte	0x4
	.2byte	0x2e5
	.4byte	0xb4
	.byte	0x18
	.uleb128 0x9
	.4byte	.LASF29
	.byte	0x4
	.2byte	0x2e6
	.4byte	0xa2
	.byte	0x1c
	.uleb128 0x9
	.4byte	.LASF30
	.byte	0x4
	.2byte	0x2e7
	.4byte	0xb4
	.byte	0x20
	.uleb128 0x9
	.4byte	.LASF31
	.byte	0x4
	.2byte	0x2e8
	.4byte	0xb4
	.byte	0x24
	.uleb128 0x9
	.4byte	.LASF32
	.byte	0x4
	.2byte	0x2e9
	.4byte	0xb9
	.byte	0x28
	.uleb128 0x9
	.4byte	.LASF33
	.byte	0x4
	.2byte	0x2ea
	.4byte	0xb4
	.byte	0x30
	.uleb128 0x9
	.4byte	.LASF34
	.byte	0x4
	.2byte	0x2eb
	.4byte	0xb4
	.byte	0x34
	.uleb128 0x9
	.4byte	.LASF35
	.byte	0x4
	.2byte	0x2ec
	.4byte	0xb4
	.byte	0x38
	.uleb128 0x9
	.4byte	.LASF36
	.byte	0x4
	.2byte	0x2ed
	.4byte	0xa2
	.byte	0x3c
	.uleb128 0x9
	.4byte	.LASF37
	.byte	0x4
	.2byte	0x2ee
	.4byte	0xb4
	.byte	0x40
	.uleb128 0x9
	.4byte	.LASF38
	.byte	0x4
	.2byte	0x2ef
	.4byte	0xb4
	.byte	0x44
	.uleb128 0x9
	.4byte	.LASF39
	.byte	0x4
	.2byte	0x2f0
	.4byte	0xb9
	.byte	0x48
	.uleb128 0x9
	.4byte	.LASF40
	.byte	0x4
	.2byte	0x2f1
	.4byte	0xb4
	.byte	0x50
	.uleb128 0x9
	.4byte	.LASF41
	.byte	0x4
	.2byte	0x2f2
	.4byte	0xb4
	.byte	0x54
	.uleb128 0x9
	.4byte	.LASF42
	.byte	0x4
	.2byte	0x2f3
	.4byte	0xb4
	.byte	0x58
	.uleb128 0x9
	.4byte	.LASF43
	.byte	0x4
	.2byte	0x2f4
	.4byte	0xa2
	.byte	0x5c
	.uleb128 0x9
	.4byte	.LASF44
	.byte	0x4
	.2byte	0x2f5
	.4byte	0xb4
	.byte	0x60
	.uleb128 0x9
	.4byte	.LASF45
	.byte	0x4
	.2byte	0x2f6
	.4byte	0xb4
	.byte	0x64
	.uleb128 0x9
	.4byte	.LASF46
	.byte	0x4
	.2byte	0x2f7
	.4byte	0xb9
	.byte	0x68
	.uleb128 0x9
	.4byte	.LASF47
	.byte	0x4
	.2byte	0x2f8
	.4byte	0xb4
	.byte	0x70
	.uleb128 0xa
	.ascii	"CSR\000"
	.byte	0x4
	.2byte	0x2f9
	.4byte	0xb4
	.byte	0x74
	.uleb128 0x9
	.4byte	.LASF48
	.byte	0x4
	.2byte	0x2fa
	.4byte	0xb9
	.byte	0x78
	.uleb128 0x9
	.4byte	.LASF49
	.byte	0x4
	.2byte	0x2fb
	.4byte	0xb4
	.byte	0x80
	.uleb128 0x9
	.4byte	.LASF50
	.byte	0x4
	.2byte	0x2fc
	.4byte	0xb4
	.byte	0x84
	.byte	0
	.uleb128 0xb
	.4byte	.LASF51
	.byte	0x4
	.2byte	0x2fd
	.4byte	0x170
	.uleb128 0x8
	.byte	0x1c
	.byte	0x4
	.2byte	0x395
	.4byte	0x3c9
	.uleb128 0xa
	.ascii	"SR\000"
	.byte	0x4
	.2byte	0x397
	.4byte	0xc9
	.byte	0
	.uleb128 0x9
	.4byte	.LASF29
	.byte	0x4
	.2byte	0x398
	.4byte	0x8c
	.byte	0x2
	.uleb128 0xa
	.ascii	"DR\000"
	.byte	0x4
	.2byte	0x399
	.4byte	0xc9
	.byte	0x4
	.uleb128 0x9
	.4byte	.LASF32
	.byte	0x4
	.2byte	0x39a
	.4byte	0x8c
	.byte	0x6
	.uleb128 0xa
	.ascii	"BRR\000"
	.byte	0x4
	.2byte	0x39b
	.4byte	0xc9
	.byte	0x8
	.uleb128 0x9
	.4byte	.LASF36
	.byte	0x4
	.2byte	0x39c
	.4byte	0x8c
	.byte	0xa
	.uleb128 0xa
	.ascii	"CR1\000"
	.byte	0x4
	.2byte	0x39d
	.4byte	0xc9
	.byte	0xc
	.uleb128 0x9
	.4byte	.LASF39
	.byte	0x4
	.2byte	0x39e
	.4byte	0x8c
	.byte	0xe
	.uleb128 0xa
	.ascii	"CR2\000"
	.byte	0x4
	.2byte	0x39f
	.4byte	0xc9
	.byte	0x10
	.uleb128 0x9
	.4byte	.LASF43
	.byte	0x4
	.2byte	0x3a0
	.4byte	0x8c
	.byte	0x12
	.uleb128 0xa
	.ascii	"CR3\000"
	.byte	0x4
	.2byte	0x3a1
	.4byte	0xc9
	.byte	0x14
	.uleb128 0x9
	.4byte	.LASF46
	.byte	0x4
	.2byte	0x3a2
	.4byte	0x8c
	.byte	0x16
	.uleb128 0x9
	.4byte	.LASF52
	.byte	0x4
	.2byte	0x3a3
	.4byte	0xc9
	.byte	0x18
	.uleb128 0x9
	.4byte	.LASF48
	.byte	0x4
	.2byte	0x3a4
	.4byte	0x8c
	.byte	0x1a
	.byte	0
	.uleb128 0xb
	.4byte	.LASF53
	.byte	0x4
	.2byte	0x3a5
	.4byte	0x30b
	.uleb128 0x3
	.4byte	.LASF54
	.byte	0x5
	.byte	0x28
	.4byte	0x3e0
	.uleb128 0xc
	.4byte	.LASF68
	.byte	0x4
	.byte	0x6
	.byte	0
	.4byte	0x3f7
	.uleb128 0xd
	.4byte	.LASF69
	.4byte	0x3f7
	.byte	0
	.byte	0
	.uleb128 0xe
	.byte	0x4
	.uleb128 0x3
	.4byte	.LASF55
	.byte	0x5
	.byte	0x62
	.4byte	0x3d5
	.uleb128 0xf
	.4byte	.LASF58
	.byte	0x1
	.byte	0x3
	.4byte	.LFB110
	.4byte	.LFE110-.LFB110
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x452
	.uleb128 0x10
	.ascii	"val\000"
	.byte	0x1
	.byte	0x3
	.4byte	0x7e
	.uleb128 0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x11
	.4byte	.LASF56
	.byte	0x1
	.byte	0x5
	.4byte	0x452
	.uleb128 0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x12
	.ascii	"idx\000"
	.byte	0x1
	.byte	0x6
	.4byte	0x7e
	.uleb128 0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.ascii	"tmp\000"
	.byte	0x1
	.byte	0x7
	.4byte	0x7e
	.uleb128 0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x6
	.4byte	0x462
	.4byte	0x462
	.uleb128 0x7
	.4byte	0xad
	.byte	0x1f
	.byte	0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF57
	.uleb128 0xf
	.4byte	.LASF59
	.byte	0x1
	.byte	0x29
	.4byte	.LFB111
	.4byte	.LFE111-.LFB111
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x4ac
	.uleb128 0x10
	.ascii	"val\000"
	.byte	0x1
	.byte	0x29
	.4byte	0x7e
	.uleb128 0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x11
	.4byte	.LASF60
	.byte	0x1
	.byte	0x2b
	.4byte	0x4bc
	.uleb128 0x5
	.byte	0x3
	.4byte	hex_map.5401
	.uleb128 0x12
	.ascii	"idx\000"
	.byte	0x1
	.byte	0x2c
	.4byte	0x7e
	.uleb128 0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x6
	.4byte	0x462
	.4byte	0x4bc
	.uleb128 0x7
	.4byte	0xad
	.byte	0x10
	.byte	0
	.uleb128 0x13
	.4byte	0x4ac
	.uleb128 0xf
	.4byte	.LASF61
	.byte	0x1
	.byte	0x37
	.4byte	.LFB112
	.4byte	.LFE112-.LFB112
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x4e5
	.uleb128 0x10
	.ascii	"str\000"
	.byte	0x1
	.byte	0x37
	.4byte	0x4e5
	.uleb128 0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x14
	.byte	0x4
	.4byte	0x4eb
	.uleb128 0x13
	.4byte	0x462
	.uleb128 0xf
	.4byte	.LASF62
	.byte	0x1
	.byte	0x3e
	.4byte	.LFB113
	.4byte	.LFE113-.LFB113
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x530
	.uleb128 0x10
	.ascii	"fmt\000"
	.byte	0x1
	.byte	0x3e
	.4byte	0x4e5
	.uleb128 0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x15
	.uleb128 0x12
	.ascii	"ap\000"
	.byte	0x1
	.byte	0x41
	.4byte	0x3f9
	.uleb128 0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x11
	.4byte	.LASF63
	.byte	0x1
	.byte	0x44
	.4byte	0x462
	.uleb128 0x2
	.byte	0x91
	.sleb128 -25
	.byte	0
	.uleb128 0x16
	.4byte	.LASF70
	.byte	0x1
	.byte	0x75
	.4byte	.LFB114
	.4byte	.LFE114-.LFB114
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x17
	.4byte	.LASF64
	.byte	0x1
	.byte	0x98
	.4byte	.LFB115
	.4byte	.LFE115-.LFB115
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x563
	.uleb128 0x10
	.ascii	"c\000"
	.byte	0x1
	.byte	0x98
	.4byte	0x462
	.uleb128 0x2
	.byte	0x91
	.sleb128 -9
	.byte	0
	.uleb128 0x18
	.4byte	.LASF71
	.byte	0x7
	.2byte	0x51b
	.4byte	0xce
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.uleb128 0x34
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2116
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x1c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.Ltext0
	.4byte	.Letext0-.Ltext0
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF40:
	.ascii	"AHB1LPENR\000"
.LASF70:
	.ascii	"init_UART4\000"
.LASF52:
	.ascii	"GTPR\000"
.LASF30:
	.ascii	"APB1RSTR\000"
.LASF34:
	.ascii	"AHB2ENR\000"
.LASF64:
	.ascii	"print_char\000"
.LASF66:
	.ascii	"print.c\000"
.LASF2:
	.ascii	"short int\000"
.LASF15:
	.ascii	"sizetype\000"
.LASF47:
	.ascii	"BDCR\000"
.LASF50:
	.ascii	"PLLI2SCFGR\000"
.LASF7:
	.ascii	"__uint32_t\000"
.LASF67:
	.ascii	"/home/sharpovici-ostrakovici/stm_dev/STM32F407/exer"
	.ascii	"cise/project\000"
.LASF4:
	.ascii	"__uint16_t\000"
.LASF49:
	.ascii	"SSCGR\000"
.LASF42:
	.ascii	"AHB3LPENR\000"
.LASF25:
	.ascii	"CFGR\000"
.LASF36:
	.ascii	"RESERVED2\000"
.LASF55:
	.ascii	"va_list\000"
.LASF39:
	.ascii	"RESERVED3\000"
.LASF37:
	.ascii	"APB1ENR\000"
.LASF53:
	.ascii	"USART_TypeDef\000"
.LASF17:
	.ascii	"OTYPER\000"
.LASF0:
	.ascii	"signed char\000"
.LASF35:
	.ascii	"AHB3ENR\000"
.LASF9:
	.ascii	"long long int\000"
.LASF19:
	.ascii	"PUPDR\000"
.LASF68:
	.ascii	"__va_list\000"
.LASF6:
	.ascii	"long int\000"
.LASF51:
	.ascii	"RCC_TypeDef\000"
.LASF21:
	.ascii	"BSRRH\000"
.LASF16:
	.ascii	"MODER\000"
.LASF31:
	.ascii	"APB2RSTR\000"
.LASF20:
	.ascii	"BSRRL\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF38:
	.ascii	"APB2ENR\000"
.LASF10:
	.ascii	"long long unsigned int\000"
.LASF14:
	.ascii	"uint32_t\000"
.LASF11:
	.ascii	"unsigned int\000"
.LASF27:
	.ascii	"AHB2RSTR\000"
.LASF12:
	.ascii	"uint16_t\000"
.LASF8:
	.ascii	"long unsigned int\000"
.LASF62:
	.ascii	"print_terminal\000"
.LASF24:
	.ascii	"PLLCFGR\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF44:
	.ascii	"APB1LPENR\000"
.LASF69:
	.ascii	"__ap\000"
.LASF57:
	.ascii	"char\000"
.LASF13:
	.ascii	"int32_t\000"
.LASF58:
	.ascii	"print_integer\000"
.LASF60:
	.ascii	"hex_map\000"
.LASF65:
	.ascii	"GNU C 4.9.3 20141119 (release) [ARM/embedded-4_9-br"
	.ascii	"anch revision 218278] -mlittle-endian -mthumb -mcpu"
	.ascii	"=cortex-m4 -mthumb-interwork -mfloat-abi=hard -mfpu"
	.ascii	"=fpv4-sp-d16 -g -O0 -fsingle-precision-constant\000"
.LASF61:
	.ascii	"print_string\000"
.LASF41:
	.ascii	"AHB2LPENR\000"
.LASF56:
	.ascii	"buffer\000"
.LASF29:
	.ascii	"RESERVED0\000"
.LASF32:
	.ascii	"RESERVED1\000"
.LASF59:
	.ascii	"print_hex\000"
.LASF18:
	.ascii	"OSPEEDR\000"
.LASF43:
	.ascii	"RESERVED4\000"
.LASF46:
	.ascii	"RESERVED5\000"
.LASF48:
	.ascii	"RESERVED6\000"
.LASF33:
	.ascii	"AHB1ENR\000"
.LASF5:
	.ascii	"__int32_t\000"
.LASF54:
	.ascii	"__gnuc_va_list\000"
.LASF26:
	.ascii	"AHB1RSTR\000"
.LASF23:
	.ascii	"GPIO_TypeDef\000"
.LASF28:
	.ascii	"AHB3RSTR\000"
.LASF63:
	.ascii	"flag\000"
.LASF71:
	.ascii	"ITM_RxBuffer\000"
.LASF22:
	.ascii	"LCKR\000"
.LASF45:
	.ascii	"APB2LPENR\000"
	.ident	"GCC: (GNU Tools for ARM Embedded Processors) 4.9.3 20141119 (release) [ARM/embedded-4_9-branch revision 218278]"
