	.syntax unified
	.cpu cortex-m4
	.eabi_attribute 27, 3
	.eabi_attribute 28, 1
	.fpu fpv4-sp-d16
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 1
	.eabi_attribute 30, 6
	.eabi_attribute 34, 1
	.eabi_attribute 18, 4
	.thumb
	.file	"ili9341_driver.c"
	.text
.Ltext0:
	.cfi_sections	.debug_frame
	.align	2
	.global	init_ILI9341_GPIO
	.thumb
	.thumb_func
	.type	init_ILI9341_GPIO, %function
init_ILI9341_GPIO:
.LFB110:
	.file 1 "ili9341_driver.c"
	.loc 1 4 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	push	{r7}
	.cfi_def_cfa_offset 4
	.cfi_offset 7, -4
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	.loc 1 6 0
	ldr	r2, .L2
	ldr	r3, .L2
	ldr	r3, [r3, #48]
	orr	r3, r3, #1
	str	r3, [r2, #48]
	.loc 1 10 0
	ldr	r2, .L2+4
	ldr	r3, .L2+4
	ldr	r3, [r3]
	bic	r3, r3, #4128768
	str	r3, [r2]
	.loc 1 15 0
	ldr	r2, .L2+4
	ldr	r3, .L2+4
	ldr	r3, [r3]
	orr	r3, r3, #1376256
	str	r3, [r2]
	.loc 1 22 0
	ldr	r2, .L2+4
	ldr	r3, .L2+4
	ldr	r3, [r3, #4]
	bic	r3, r3, #1792
	str	r3, [r2, #4]
	.loc 1 29 0
	ldr	r2, .L2+4
	ldr	r3, .L2+4
	ldr	r3, [r3, #8]
	orr	r3, r3, #4128768
	str	r3, [r2, #8]
	.loc 1 37 0
	ldr	r2, .L2+4
	ldr	r3, .L2+4
	ldr	r3, [r3, #12]
	bic	r3, r3, #4128768
	str	r3, [r2, #12]
	.loc 1 41 0
	ldr	r2, .L2+4
	ldr	r3, .L2+4
	ldr	r3, [r3, #12]
	orr	r3, r3, #1048576
	str	r3, [r2, #12]
	.loc 1 42 0
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	ldr	r7, [sp], #4
	.cfi_restore 7
	.cfi_def_cfa_offset 0
	bx	lr
.L3:
	.align	2
.L2:
	.word	1073887232
	.word	1073872896
	.cfi_endproc
.LFE110:
	.size	init_ILI9341_GPIO, .-init_ILI9341_GPIO
	.align	2
	.global	ILI9341_write_cmd
	.thumb
	.thumb_func
	.type	ILI9341_write_cmd, %function
ILI9341_write_cmd:
.LFB111:
	.loc 1 47 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r7, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	sub	sp, sp, #8
	.cfi_def_cfa_offset 16
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	str	r0, [r7, #4]
	.loc 1 48 0
	ldr	r2, .L5
	ldr	r3, .L5
	ldr	r3, [r3, #20]
	bic	r3, r3, #256
	str	r3, [r2, #20]
	.loc 1 49 0
	ldr	r3, [r7, #4]
	uxtb	r3, r3
	mov	r0, r3
	bl	SPI1_write
	.loc 1 50 0
	adds	r7, r7, #8
	.cfi_def_cfa_offset 8
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	pop	{r7, pc}
.L6:
	.align	2
.L5:
	.word	1073872896
	.cfi_endproc
.LFE111:
	.size	ILI9341_write_cmd, .-ILI9341_write_cmd
	.align	2
	.global	ILI9341_write_data
	.thumb
	.thumb_func
	.type	ILI9341_write_data, %function
ILI9341_write_data:
.LFB112:
	.loc 1 56 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r7, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	sub	sp, sp, #8
	.cfi_def_cfa_offset 16
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	str	r0, [r7, #4]
	.loc 1 57 0
	ldr	r2, .L8
	ldr	r3, .L8
	ldr	r3, [r3, #20]
	orr	r3, r3, #256
	str	r3, [r2, #20]
	.loc 1 58 0
	ldr	r3, [r7, #4]
	uxtb	r3, r3
	mov	r0, r3
	bl	SPI1_write
	.loc 1 59 0
	adds	r7, r7, #8
	.cfi_def_cfa_offset 8
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	pop	{r7, pc}
.L9:
	.align	2
.L8:
	.word	1073872896
	.cfi_endproc
.LFE112:
	.size	ILI9341_write_data, .-ILI9341_write_data
	.align	2
	.global	ILI9341_write
	.thumb
	.thumb_func
	.type	ILI9341_write, %function
ILI9341_write:
.LFB113:
	.loc 1 62 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r7, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	sub	sp, sp, #8
	.cfi_def_cfa_offset 16
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	str	r0, [r7, #4]
	.loc 1 63 0
	ldr	r3, [r7, #4]
	uxtb	r3, r3
	mov	r0, r3
	bl	SPI1_write
	.loc 1 64 0
	adds	r7, r7, #8
	.cfi_def_cfa_offset 8
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	pop	{r7, pc}
	.cfi_endproc
.LFE113:
	.size	ILI9341_write, .-ILI9341_write
	.align	2
	.global	write_cmd
	.thumb
	.thumb_func
	.type	write_cmd, %function
write_cmd:
.LFB114:
	.loc 1 106 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r7, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	sub	sp, sp, #8
	.cfi_def_cfa_offset 16
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	mov	r3, r0
	strb	r3, [r7, #7]
	.loc 1 107 0
	ldrb	r3, [r7, #7]	@ zero_extendqisi2
	mov	r0, r3
	bl	ILI9341_write_cmd
	.loc 1 108 0
	adds	r7, r7, #8
	.cfi_def_cfa_offset 8
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	pop	{r7, pc}
	.cfi_endproc
.LFE114:
	.size	write_cmd, .-write_cmd
	.align	2
	.global	write_data
	.thumb
	.thumb_func
	.type	write_data, %function
write_data:
.LFB115:
	.loc 1 111 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r7, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	sub	sp, sp, #8
	.cfi_def_cfa_offset 16
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	mov	r3, r0
	strb	r3, [r7, #7]
	.loc 1 112 0
	ldrb	r3, [r7, #7]	@ zero_extendqisi2
	mov	r0, r3
	bl	ILI9341_write_data
	.loc 1 113 0
	adds	r7, r7, #8
	.cfi_def_cfa_offset 8
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	pop	{r7, pc}
	.cfi_endproc
.LFE115:
	.size	write_data, .-write_data
	.section	.rodata
	.align	2
	.type	ILI9341_init_cmd, %object
	.size	ILI9341_init_cmd, 64
ILI9341_init_cmd:
	.byte	58
	.byte	1
	.byte	85
	.byte	0
	.byte	-79
	.byte	2
	.byte	0
	.byte	31
	.byte	0
	.byte	54
	.byte	1
	.byte	-24
	.byte	0
	.byte	-14
	.byte	1
	.byte	8
	.byte	0
	.byte	38
	.byte	1
	.byte	1
	.byte	0
	.byte	-32
	.byte	15
	.byte	31
	.byte	26
	.byte	24
	.byte	10
	.byte	15
	.byte	6
	.byte	69
	.byte	-121
	.byte	50
	.byte	10
	.byte	7
	.byte	2
	.byte	7
	.byte	5
	.byte	0
	.byte	0
	.byte	-31
	.byte	15
	.byte	0
	.byte	37
	.byte	39
	.byte	5
	.byte	16
	.byte	9
	.byte	58
	.byte	120
	.byte	77
	.byte	5
	.byte	24
	.byte	13
	.byte	56
	.byte	58
	.byte	31
	.byte	0
	.byte	17
	.byte	0
	.byte	5
	.byte	41
	.byte	0
	.byte	0
	.byte	0
	.text
	.align	2
	.global	ILI9341_init
	.thumb
	.thumb_func
	.type	ILI9341_init, %function
ILI9341_init:
.LFB116:
	.loc 1 177 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r7, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	sub	sp, sp, #8
	.cfi_def_cfa_offset 16
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	.loc 1 178 0
	ldr	r3, .L19
	str	r3, [r7, #4]
	.loc 1 181 0
	bl	init_SPI1
	.loc 1 182 0
	bl	init_ILI9341_GPIO
	.loc 1 194 0
	ldr	r2, .L19+4
	ldr	r3, .L19+4
	ldr	r3, [r3, #20]
	orr	r3, r3, #1024
	str	r3, [r2, #20]
	.loc 1 195 0
	movs	r0, #200
	bl	wait_ms
	.loc 1 197 0
	ldr	r2, .L19+4
	ldr	r3, .L19+4
	ldr	r3, [r3, #20]
	bic	r3, r3, #1024
	str	r3, [r2, #20]
	.loc 1 198 0
	movs	r0, #10
	bl	wait_us
	.loc 1 200 0
	ldr	r2, .L19+4
	ldr	r3, .L19+4
	ldr	r3, [r3, #20]
	orr	r3, r3, #1024
	str	r3, [r2, #20]
	.loc 1 201 0
	movs	r0, #120
	bl	wait_ms
	.loc 1 203 0
	ldr	r2, .L19+4
	ldr	r3, .L19+4
	ldr	r3, [r3, #20]
	bic	r3, r3, #512
	str	r3, [r2, #20]
	.loc 1 204 0
	movs	r0, #10
	bl	wait_ms
	.loc 1 206 0
	b	.L14
.L18:
	.loc 1 208 0
	ldr	r3, [r7, #4]
	adds	r2, r3, #1
	str	r2, [r7, #4]
	ldrb	r3, [r3]	@ zero_extendqisi2
	mov	r0, r3
	bl	ILI9341_write_cmd
	.loc 1 210 0
	ldr	r3, [r7, #4]
	adds	r2, r3, #1
	str	r2, [r7, #4]
	ldrb	r3, [r3]
	strb	r3, [r7, #3]
	ldrb	r3, [r7, #3]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L15
	.loc 1 211 0
	b	.L16
.L17:
	.loc 1 211 0 is_stmt 0 discriminator 2
	ldr	r3, [r7, #4]
	adds	r2, r3, #1
	str	r2, [r7, #4]
	ldrb	r3, [r3]	@ zero_extendqisi2
	mov	r0, r3
	bl	ILI9341_write_data
.L16:
	.loc 1 211 0 discriminator 1
	ldrb	r3, [r7, #3]	@ zero_extendqisi2
	subs	r2, r3, #1
	strb	r2, [r7, #3]
	cmp	r3, #0
	bne	.L17
.L15:
	.loc 1 213 0 is_stmt 1
	ldr	r3, [r7, #4]
	adds	r2, r3, #1
	str	r2, [r7, #4]
	ldrb	r3, [r3]	@ zero_extendqisi2
	uxth	r3, r3
	mov	r0, r3
	bl	wait_ms
.L14:
	.loc 1 206 0
	ldr	r3, [r7, #4]
	ldrb	r3, [r3]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L18
	.loc 1 216 0
	adds	r7, r7, #8
	.cfi_def_cfa_offset 8
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	pop	{r7, pc}
.L20:
	.align	2
.L19:
	.word	ILI9341_init_cmd
	.word	1073872896
	.cfi_endproc
.LFE116:
	.size	ILI9341_init, .-ILI9341_init
	.align	2
	.global	ILI9341_set_window
	.thumb
	.thumb_func
	.type	ILI9341_set_window, %function
ILI9341_set_window:
.LFB117:
	.loc 1 220 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r4, r7, lr}
	.cfi_def_cfa_offset 12
	.cfi_offset 4, -12
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	sub	sp, sp, #12
	.cfi_def_cfa_offset 24
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	mov	r4, r0
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	mov	r3, r4	@ movhi
	strh	r3, [r7, #6]	@ movhi
	mov	r3, r0	@ movhi
	strh	r3, [r7, #4]	@ movhi
	mov	r3, r1	@ movhi
	strh	r3, [r7, #2]	@ movhi
	mov	r3, r2	@ movhi
	strh	r3, [r7]	@ movhi
	.loc 1 233 0
	movs	r0, #42
	bl	ILI9341_write_cmd
	.loc 1 234 0
	ldrh	r3, [r7, #6]
	lsrs	r3, r3, #8
	uxth	r3, r3
	mov	r0, r3
	bl	ILI9341_write_data
	.loc 1 235 0
	ldrh	r3, [r7, #6]
	mov	r0, r3
	bl	ILI9341_write_data
	.loc 1 236 0
	ldrh	r3, [r7, #2]
	lsrs	r3, r3, #8
	uxth	r3, r3
	mov	r0, r3
	bl	ILI9341_write_data
	.loc 1 237 0
	ldrh	r3, [r7, #2]
	mov	r0, r3
	bl	ILI9341_write_data
	.loc 1 252 0
	movs	r0, #43
	bl	ILI9341_write_cmd
	.loc 1 253 0
	ldrh	r3, [r7, #4]
	lsrs	r3, r3, #8
	uxth	r3, r3
	mov	r0, r3
	bl	ILI9341_write_data
	.loc 1 254 0
	ldrh	r3, [r7, #4]
	mov	r0, r3
	bl	ILI9341_write_data
	.loc 1 255 0
	ldrh	r3, [r7]
	lsrs	r3, r3, #8
	uxth	r3, r3
	mov	r0, r3
	bl	ILI9341_write_data
	.loc 1 256 0
	ldrh	r3, [r7]
	mov	r0, r3
	bl	ILI9341_write_data
	.loc 1 258 0
	adds	r7, r7, #12
	.cfi_def_cfa_offset 12
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	pop	{r4, r7, pc}
	.cfi_endproc
.LFE117:
	.size	ILI9341_set_window, .-ILI9341_set_window
	.align	2
	.global	ILI9341_rectangle
	.thumb
	.thumb_func
	.type	ILI9341_rectangle, %function
ILI9341_rectangle:
.LFB118:
	.loc 1 261 0
	.cfi_startproc
	@ args = 4, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r4, r7, lr}
	.cfi_def_cfa_offset 12
	.cfi_offset 4, -12
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	sub	sp, sp, #20
	.cfi_def_cfa_offset 32
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	mov	r4, r0
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	mov	r3, r4	@ movhi
	strh	r3, [r7, #6]	@ movhi
	mov	r3, r0	@ movhi
	strh	r3, [r7, #4]	@ movhi
	mov	r3, r1	@ movhi
	strh	r3, [r7, #2]	@ movhi
	mov	r3, r2	@ movhi
	strh	r3, [r7]	@ movhi
	.loc 1 263 0
	ldrh	r2, [r7]
	ldrh	r3, [r7, #4]
	subs	r3, r2, r3
	ldrh	r1, [r7, #32]
	ldrh	r2, [r7, #2]
	subs	r2, r1, r2
	mul	r3, r2, r3
	str	r3, [r7, #8]
	.loc 1 265 0
	ldrh	r0, [r7, #4]
	ldrh	r1, [r7, #2]
	ldrh	r2, [r7]
	ldrh	r3, [r7, #32]
	bl	ILI9341_set_window
	.loc 1 272 0
	movs	r0, #44
	bl	ILI9341_write_cmd
	.loc 1 275 0
	movs	r0, #20
	bl	wait_us
	.loc 1 276 0
	ldr	r2, .L25
	ldr	r3, .L25
	ldr	r3, [r3, #20]
	orr	r3, r3, #256
	str	r3, [r2, #20]
	.loc 1 279 0
	movs	r3, #0
	str	r3, [r7, #12]
	b	.L23
.L24:
	.loc 1 281 0 discriminator 3
	ldrh	r3, [r7, #6]
	uxtb	r3, r3
	mov	r0, r3
	bl	ILI9341_write
	.loc 1 282 0 discriminator 3
	ldrh	r3, [r7, #6]
	lsrs	r3, r3, #8
	uxth	r3, r3
	mov	r0, r3
	bl	ILI9341_write
	.loc 1 279 0 discriminator 3
	ldr	r3, [r7, #12]
	adds	r3, r3, #1
	str	r3, [r7, #12]
.L23:
	.loc 1 279 0 is_stmt 0 discriminator 1
	ldr	r2, [r7, #12]
	ldr	r3, [r7, #8]
	cmp	r2, r3
	bcc	.L24
	.loc 1 284 0 is_stmt 1
	adds	r7, r7, #20
	.cfi_def_cfa_offset 12
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	pop	{r4, r7, pc}
.L26:
	.align	2
.L25:
	.word	1073872896
	.cfi_endproc
.LFE118:
	.size	ILI9341_rectangle, .-ILI9341_rectangle
	.align	2
	.global	ILI9341_set_window2
	.thumb
	.thumb_func
	.type	ILI9341_set_window2, %function
ILI9341_set_window2:
.LFB119:
	.loc 1 297 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r4, r7, lr}
	.cfi_def_cfa_offset 12
	.cfi_offset 4, -12
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	sub	sp, sp, #12
	.cfi_def_cfa_offset 24
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	mov	r4, r0
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	mov	r3, r4	@ movhi
	strh	r3, [r7, #6]	@ movhi
	mov	r3, r0	@ movhi
	strh	r3, [r7, #4]	@ movhi
	mov	r3, r1	@ movhi
	strh	r3, [r7, #2]	@ movhi
	mov	r3, r2	@ movhi
	strh	r3, [r7]	@ movhi
	.loc 1 310 0
	movs	r0, #43
	bl	ILI9341_write_cmd
	.loc 1 311 0
	ldrh	r3, [r7, #6]
	lsrs	r3, r3, #8
	uxth	r3, r3
	mov	r0, r3
	bl	ILI9341_write_data
	.loc 1 312 0
	ldrh	r3, [r7, #6]
	mov	r0, r3
	bl	ILI9341_write_data
	.loc 1 313 0
	ldrh	r3, [r7, #2]
	lsrs	r3, r3, #8
	uxth	r3, r3
	mov	r0, r3
	bl	ILI9341_write_data
	.loc 1 314 0
	ldrh	r3, [r7, #2]
	mov	r0, r3
	bl	ILI9341_write_data
	.loc 1 329 0
	movs	r0, #42
	bl	ILI9341_write_cmd
	.loc 1 330 0
	ldrh	r3, [r7, #4]
	lsrs	r3, r3, #8
	uxth	r3, r3
	mov	r0, r3
	bl	ILI9341_write_data
	.loc 1 331 0
	ldrh	r3, [r7, #4]
	mov	r0, r3
	bl	ILI9341_write_data
	.loc 1 332 0
	ldrh	r3, [r7]
	lsrs	r3, r3, #8
	uxth	r3, r3
	mov	r0, r3
	bl	ILI9341_write_data
	.loc 1 333 0
	ldrh	r3, [r7]
	mov	r0, r3
	bl	ILI9341_write_data
	.loc 1 335 0
	adds	r7, r7, #12
	.cfi_def_cfa_offset 12
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	pop	{r4, r7, pc}
	.cfi_endproc
.LFE119:
	.size	ILI9341_set_window2, .-ILI9341_set_window2
	.align	2
	.global	ILI9341_image
	.thumb
	.thumb_func
	.type	ILI9341_image, %function
ILI9341_image:
.LFB120:
	.loc 1 341 0
	.cfi_startproc
	@ args = 4, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r7, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	sub	sp, sp, #24
	.cfi_def_cfa_offset 32
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	str	r0, [r7, #12]
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	mov	r3, r0	@ movhi
	strh	r3, [r7, #10]	@ movhi
	mov	r3, r1	@ movhi
	strh	r3, [r7, #8]	@ movhi
	mov	r3, r2	@ movhi
	strh	r3, [r7, #6]	@ movhi
	.loc 1 343 0
	ldrh	r2, [r7, #6]
	ldrh	r3, [r7, #10]
	subs	r3, r2, r3
	ldrh	r1, [r7, #32]
	ldrh	r2, [r7, #8]
	subs	r2, r1, r2
	mul	r3, r2, r3
	str	r3, [r7, #16]
	.loc 1 345 0
	ldrh	r3, [r7, #6]	@ movhi
	subs	r3, r3, #1
	uxth	r2, r3
	ldrh	r3, [r7, #32]	@ movhi
	subs	r3, r3, #1
	uxth	r3, r3
	ldrh	r0, [r7, #10]
	ldrh	r1, [r7, #8]
	bl	ILI9341_set_window
	.loc 1 353 0
	movs	r0, #44
	bl	ILI9341_write_cmd
	.loc 1 356 0
	movs	r0, #20
	bl	wait_us
	.loc 1 357 0
	ldr	r2, .L31
	ldr	r3, .L31
	ldr	r3, [r3, #20]
	orr	r3, r3, #256
	str	r3, [r2, #20]
	.loc 1 361 0
	movs	r3, #0
	str	r3, [r7, #20]
	b	.L29
.L30:
	.loc 1 363 0 discriminator 3
	ldr	r3, [r7, #20]
	lsls	r3, r3, #1
	ldr	r2, [r7, #12]
	add	r3, r3, r2
	ldrb	r3, [r3]	@ zero_extendqisi2
	mov	r0, r3
	bl	ILI9341_write
	.loc 1 364 0 discriminator 3
	ldr	r3, [r7, #20]
	lsls	r3, r3, #1
	adds	r3, r3, #1
	ldr	r2, [r7, #12]
	add	r3, r3, r2
	ldrb	r3, [r3]	@ zero_extendqisi2
	mov	r0, r3
	bl	ILI9341_write
	.loc 1 361 0 discriminator 3
	ldr	r3, [r7, #20]
	adds	r3, r3, #1
	str	r3, [r7, #20]
.L29:
	.loc 1 361 0 is_stmt 0 discriminator 1
	ldr	r2, [r7, #20]
	ldr	r3, [r7, #16]
	cmp	r2, r3
	bcc	.L30
	.loc 1 367 0 is_stmt 1
	adds	r7, r7, #24
	.cfi_def_cfa_offset 8
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	pop	{r7, pc}
.L32:
	.align	2
.L31:
	.word	1073872896
	.cfi_endproc
.LFE120:
	.size	ILI9341_image, .-ILI9341_image
.Letext0:
	.file 2 "/home/sharpovici-ostrakovici/stm_dev/STM32F407/gcc-arm-none-eabi/arm-none-eabi/include/machine/_default_types.h"
	.file 3 "/home/sharpovici-ostrakovici/stm_dev/STM32F407/gcc-arm-none-eabi/arm-none-eabi/include/stdint.h"
	.file 4 "stm32f4xx.h"
	.file 5 "../../../STM32F407/Libraries/CMSIS/Include/core_cm4.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x5f7
	.2byte	0x4
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF69
	.byte	0x1
	.4byte	.LASF70
	.4byte	.LASF71
	.4byte	.Ltext0
	.4byte	.Letext0-.Ltext0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF0
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x2
	.byte	0x1d
	.4byte	0x37
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF2
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x2
	.byte	0x2b
	.4byte	0x50
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF5
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x3f
	.4byte	0x62
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF7
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x2
	.byte	0x41
	.4byte	0x74
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF9
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF10
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF11
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF12
	.uleb128 0x3
	.4byte	.LASF13
	.byte	0x3
	.byte	0x15
	.4byte	0x2c
	.uleb128 0x3
	.4byte	.LASF14
	.byte	0x3
	.byte	0x21
	.4byte	0x45
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x3
	.byte	0x2c
	.4byte	0x57
	.uleb128 0x3
	.4byte	.LASF16
	.byte	0x3
	.byte	0x2d
	.4byte	0x69
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF17
	.uleb128 0x5
	.4byte	0xb8
	.uleb128 0x6
	.4byte	0xb8
	.4byte	0xdf
	.uleb128 0x7
	.4byte	0xc3
	.byte	0x1
	.byte	0
	.uleb128 0x5
	.4byte	0xa2
	.uleb128 0x5
	.4byte	0xad
	.uleb128 0x8
	.4byte	0x97
	.uleb128 0x9
	.byte	0x28
	.byte	0x4
	.2byte	0x28e
	.4byte	0x17a
	.uleb128 0xa
	.4byte	.LASF18
	.byte	0x4
	.2byte	0x290
	.4byte	0xca
	.byte	0
	.uleb128 0xa
	.4byte	.LASF19
	.byte	0x4
	.2byte	0x291
	.4byte	0xca
	.byte	0x4
	.uleb128 0xa
	.4byte	.LASF20
	.byte	0x4
	.2byte	0x292
	.4byte	0xca
	.byte	0x8
	.uleb128 0xa
	.4byte	.LASF21
	.byte	0x4
	.2byte	0x293
	.4byte	0xca
	.byte	0xc
	.uleb128 0xb
	.ascii	"IDR\000"
	.byte	0x4
	.2byte	0x294
	.4byte	0xca
	.byte	0x10
	.uleb128 0xb
	.ascii	"ODR\000"
	.byte	0x4
	.2byte	0x295
	.4byte	0xca
	.byte	0x14
	.uleb128 0xa
	.4byte	.LASF22
	.byte	0x4
	.2byte	0x296
	.4byte	0xdf
	.byte	0x18
	.uleb128 0xa
	.4byte	.LASF23
	.byte	0x4
	.2byte	0x297
	.4byte	0xdf
	.byte	0x1a
	.uleb128 0xa
	.4byte	.LASF24
	.byte	0x4
	.2byte	0x298
	.4byte	0xca
	.byte	0x1c
	.uleb128 0xb
	.ascii	"AFR\000"
	.byte	0x4
	.2byte	0x299
	.4byte	0x17a
	.byte	0x20
	.byte	0
	.uleb128 0x5
	.4byte	0xcf
	.uleb128 0xc
	.4byte	.LASF25
	.byte	0x4
	.2byte	0x29a
	.4byte	0xee
	.uleb128 0x9
	.byte	0x88
	.byte	0x4
	.2byte	0x2dd
	.4byte	0x31a
	.uleb128 0xb
	.ascii	"CR\000"
	.byte	0x4
	.2byte	0x2df
	.4byte	0xca
	.byte	0
	.uleb128 0xa
	.4byte	.LASF26
	.byte	0x4
	.2byte	0x2e0
	.4byte	0xca
	.byte	0x4
	.uleb128 0xa
	.4byte	.LASF27
	.byte	0x4
	.2byte	0x2e1
	.4byte	0xca
	.byte	0x8
	.uleb128 0xb
	.ascii	"CIR\000"
	.byte	0x4
	.2byte	0x2e2
	.4byte	0xca
	.byte	0xc
	.uleb128 0xa
	.4byte	.LASF28
	.byte	0x4
	.2byte	0x2e3
	.4byte	0xca
	.byte	0x10
	.uleb128 0xa
	.4byte	.LASF29
	.byte	0x4
	.2byte	0x2e4
	.4byte	0xca
	.byte	0x14
	.uleb128 0xa
	.4byte	.LASF30
	.byte	0x4
	.2byte	0x2e5
	.4byte	0xca
	.byte	0x18
	.uleb128 0xa
	.4byte	.LASF31
	.byte	0x4
	.2byte	0x2e6
	.4byte	0xb8
	.byte	0x1c
	.uleb128 0xa
	.4byte	.LASF32
	.byte	0x4
	.2byte	0x2e7
	.4byte	0xca
	.byte	0x20
	.uleb128 0xa
	.4byte	.LASF33
	.byte	0x4
	.2byte	0x2e8
	.4byte	0xca
	.byte	0x24
	.uleb128 0xa
	.4byte	.LASF34
	.byte	0x4
	.2byte	0x2e9
	.4byte	0xcf
	.byte	0x28
	.uleb128 0xa
	.4byte	.LASF35
	.byte	0x4
	.2byte	0x2ea
	.4byte	0xca
	.byte	0x30
	.uleb128 0xa
	.4byte	.LASF36
	.byte	0x4
	.2byte	0x2eb
	.4byte	0xca
	.byte	0x34
	.uleb128 0xa
	.4byte	.LASF37
	.byte	0x4
	.2byte	0x2ec
	.4byte	0xca
	.byte	0x38
	.uleb128 0xa
	.4byte	.LASF38
	.byte	0x4
	.2byte	0x2ed
	.4byte	0xb8
	.byte	0x3c
	.uleb128 0xa
	.4byte	.LASF39
	.byte	0x4
	.2byte	0x2ee
	.4byte	0xca
	.byte	0x40
	.uleb128 0xa
	.4byte	.LASF40
	.byte	0x4
	.2byte	0x2ef
	.4byte	0xca
	.byte	0x44
	.uleb128 0xa
	.4byte	.LASF41
	.byte	0x4
	.2byte	0x2f0
	.4byte	0xcf
	.byte	0x48
	.uleb128 0xa
	.4byte	.LASF42
	.byte	0x4
	.2byte	0x2f1
	.4byte	0xca
	.byte	0x50
	.uleb128 0xa
	.4byte	.LASF43
	.byte	0x4
	.2byte	0x2f2
	.4byte	0xca
	.byte	0x54
	.uleb128 0xa
	.4byte	.LASF44
	.byte	0x4
	.2byte	0x2f3
	.4byte	0xca
	.byte	0x58
	.uleb128 0xa
	.4byte	.LASF45
	.byte	0x4
	.2byte	0x2f4
	.4byte	0xb8
	.byte	0x5c
	.uleb128 0xa
	.4byte	.LASF46
	.byte	0x4
	.2byte	0x2f5
	.4byte	0xca
	.byte	0x60
	.uleb128 0xa
	.4byte	.LASF47
	.byte	0x4
	.2byte	0x2f6
	.4byte	0xca
	.byte	0x64
	.uleb128 0xa
	.4byte	.LASF48
	.byte	0x4
	.2byte	0x2f7
	.4byte	0xcf
	.byte	0x68
	.uleb128 0xa
	.4byte	.LASF49
	.byte	0x4
	.2byte	0x2f8
	.4byte	0xca
	.byte	0x70
	.uleb128 0xb
	.ascii	"CSR\000"
	.byte	0x4
	.2byte	0x2f9
	.4byte	0xca
	.byte	0x74
	.uleb128 0xa
	.4byte	.LASF50
	.byte	0x4
	.2byte	0x2fa
	.4byte	0xcf
	.byte	0x78
	.uleb128 0xa
	.4byte	.LASF51
	.byte	0x4
	.2byte	0x2fb
	.4byte	0xca
	.byte	0x80
	.uleb128 0xa
	.4byte	.LASF52
	.byte	0x4
	.2byte	0x2fc
	.4byte	0xca
	.byte	0x84
	.byte	0
	.uleb128 0xc
	.4byte	.LASF53
	.byte	0x4
	.2byte	0x2fd
	.4byte	0x18b
	.uleb128 0xd
	.4byte	.LASF72
	.byte	0x1
	.byte	0x3
	.4byte	.LFB110
	.4byte	.LFE110-.LFB110
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0xe
	.4byte	.LASF54
	.byte	0x1
	.byte	0x2e
	.4byte	.LFB111
	.4byte	.LFE111-.LFB111
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x35b
	.uleb128 0xf
	.ascii	"cmd\000"
	.byte	0x1
	.byte	0x2e
	.4byte	0xb8
	.uleb128 0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xe
	.4byte	.LASF55
	.byte	0x1
	.byte	0x37
	.4byte	.LFB112
	.4byte	.LFE112-.LFB112
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x37f
	.uleb128 0x10
	.4byte	.LASF56
	.byte	0x1
	.byte	0x37
	.4byte	0xb8
	.uleb128 0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xe
	.4byte	.LASF57
	.byte	0x1
	.byte	0x3d
	.4byte	.LFB113
	.4byte	.LFE113-.LFB113
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x3a3
	.uleb128 0x10
	.4byte	.LASF56
	.byte	0x1
	.byte	0x3d
	.4byte	0xb8
	.uleb128 0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0xe
	.4byte	.LASF58
	.byte	0x1
	.byte	0x69
	.4byte	.LFB114
	.4byte	.LFE114-.LFB114
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x3c7
	.uleb128 0xf
	.ascii	"cmd\000"
	.byte	0x1
	.byte	0x69
	.4byte	0x97
	.uleb128 0x2
	.byte	0x91
	.sleb128 -9
	.byte	0
	.uleb128 0xe
	.4byte	.LASF59
	.byte	0x1
	.byte	0x6e
	.4byte	.LFB115
	.4byte	.LFE115-.LFB115
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x3eb
	.uleb128 0x10
	.4byte	.LASF56
	.byte	0x1
	.byte	0x6e
	.4byte	0x97
	.uleb128 0x2
	.byte	0x91
	.sleb128 -9
	.byte	0
	.uleb128 0x11
	.4byte	.LASF73
	.byte	0x1
	.byte	0xb0
	.4byte	.LFB116
	.4byte	.LFE116-.LFB116
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x42a
	.uleb128 0x12
	.4byte	.LASF60
	.byte	0x1
	.byte	0xb2
	.4byte	0x42a
	.uleb128 0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x12
	.4byte	.LASF61
	.byte	0x1
	.byte	0xb3
	.4byte	0x97
	.uleb128 0x2
	.byte	0x91
	.sleb128 -13
	.uleb128 0x13
	.4byte	.LASF74
	.byte	0x1
	.byte	0xb5
	.4byte	0x89
	.uleb128 0x14
	.byte	0
	.byte	0
	.uleb128 0x15
	.byte	0x4
	.4byte	0xe9
	.uleb128 0xe
	.4byte	.LASF62
	.byte	0x1
	.byte	0xdb
	.4byte	.LFB117
	.4byte	.LFE117-.LFB117
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x47a
	.uleb128 0xf
	.ascii	"x0\000"
	.byte	0x1
	.byte	0xdb
	.4byte	0xa2
	.uleb128 0x2
	.byte	0x91
	.sleb128 -18
	.uleb128 0xf
	.ascii	"y0\000"
	.byte	0x1
	.byte	0xdb
	.4byte	0xa2
	.uleb128 0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0xf
	.ascii	"x1\000"
	.byte	0x1
	.byte	0xdb
	.4byte	0xa2
	.uleb128 0x2
	.byte	0x91
	.sleb128 -22
	.uleb128 0xf
	.ascii	"y1\000"
	.byte	0x1
	.byte	0xdb
	.4byte	0xa2
	.uleb128 0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x16
	.4byte	.LASF63
	.byte	0x1
	.2byte	0x104
	.4byte	.LFB118
	.4byte	.LFE118-.LFB118
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x4f3
	.uleb128 0x17
	.4byte	.LASF64
	.byte	0x1
	.2byte	0x104
	.4byte	0xa2
	.uleb128 0x2
	.byte	0x91
	.sleb128 -26
	.uleb128 0x18
	.ascii	"x0\000"
	.byte	0x1
	.2byte	0x104
	.4byte	0xa2
	.uleb128 0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x18
	.ascii	"y0\000"
	.byte	0x1
	.2byte	0x104
	.4byte	0xa2
	.uleb128 0x2
	.byte	0x91
	.sleb128 -30
	.uleb128 0x18
	.ascii	"x1\000"
	.byte	0x1
	.2byte	0x104
	.4byte	0xa2
	.uleb128 0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x18
	.ascii	"y1\000"
	.byte	0x1
	.2byte	0x104
	.4byte	0xa2
	.uleb128 0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x19
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x106
	.4byte	0xb8
	.uleb128 0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x19
	.ascii	"sz\000"
	.byte	0x1
	.2byte	0x107
	.4byte	0xb8
	.uleb128 0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x16
	.4byte	.LASF65
	.byte	0x1
	.2byte	0x128
	.4byte	.LFB119
	.4byte	.LFE119-.LFB119
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x542
	.uleb128 0x18
	.ascii	"x0\000"
	.byte	0x1
	.2byte	0x128
	.4byte	0xa2
	.uleb128 0x2
	.byte	0x91
	.sleb128 -18
	.uleb128 0x18
	.ascii	"y0\000"
	.byte	0x1
	.2byte	0x128
	.4byte	0xa2
	.uleb128 0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x18
	.ascii	"x1\000"
	.byte	0x1
	.2byte	0x128
	.4byte	0xa2
	.uleb128 0x2
	.byte	0x91
	.sleb128 -22
	.uleb128 0x18
	.ascii	"y1\000"
	.byte	0x1
	.2byte	0x128
	.4byte	0xa2
	.uleb128 0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x16
	.4byte	.LASF66
	.byte	0x1
	.2byte	0x154
	.4byte	.LFB120
	.4byte	.LFE120-.LFB120
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x5bb
	.uleb128 0x18
	.ascii	"img\000"
	.byte	0x1
	.2byte	0x154
	.4byte	0x5bb
	.uleb128 0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x18
	.ascii	"x0\000"
	.byte	0x1
	.2byte	0x154
	.4byte	0xa2
	.uleb128 0x2
	.byte	0x91
	.sleb128 -22
	.uleb128 0x18
	.ascii	"y0\000"
	.byte	0x1
	.2byte	0x154
	.4byte	0xa2
	.uleb128 0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x18
	.ascii	"x1\000"
	.byte	0x1
	.2byte	0x154
	.4byte	0xa2
	.uleb128 0x2
	.byte	0x91
	.sleb128 -26
	.uleb128 0x18
	.ascii	"y1\000"
	.byte	0x1
	.2byte	0x154
	.4byte	0xa2
	.uleb128 0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x19
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x156
	.4byte	0xb8
	.uleb128 0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x19
	.ascii	"sz\000"
	.byte	0x1
	.2byte	0x157
	.4byte	0xb8
	.uleb128 0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x15
	.byte	0x4
	.4byte	0x5c1
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF67
	.uleb128 0x6
	.4byte	0x97
	.4byte	0x5d8
	.uleb128 0x7
	.4byte	0xc3
	.byte	0x3f
	.byte	0
	.uleb128 0x12
	.4byte	.LASF68
	.byte	0x1
	.byte	0x78
	.4byte	0x5e9
	.uleb128 0x5
	.byte	0x3
	.4byte	ILI9341_init_cmd
	.uleb128 0x8
	.4byte	0x5c8
	.uleb128 0x1a
	.4byte	.LASF75
	.byte	0x5
	.2byte	0x51b
	.4byte	0xe4
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2116
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2116
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2116
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x1c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.Ltext0
	.4byte	.Letext0-.Ltext0
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF42:
	.ascii	"AHB1LPENR\000"
.LASF73:
	.ascii	"ILI9341_init\000"
.LASF70:
	.ascii	"ili9341_driver.c\000"
.LASF32:
	.ascii	"APB1RSTR\000"
.LASF36:
	.ascii	"AHB2ENR\000"
.LASF2:
	.ascii	"short int\000"
.LASF64:
	.ascii	"color\000"
.LASF49:
	.ascii	"BDCR\000"
.LASF52:
	.ascii	"PLLI2SCFGR\000"
.LASF8:
	.ascii	"__uint32_t\000"
.LASF71:
	.ascii	"/home/sharpovici-ostrakovici/stm_dev/STM32F407/exer"
	.ascii	"cise/project\000"
.LASF4:
	.ascii	"__uint16_t\000"
.LASF51:
	.ascii	"SSCGR\000"
.LASF44:
	.ascii	"AHB3LPENR\000"
.LASF27:
	.ascii	"CFGR\000"
.LASF41:
	.ascii	"RESERVED3\000"
.LASF13:
	.ascii	"uint8_t\000"
.LASF39:
	.ascii	"APB1ENR\000"
.LASF60:
	.ascii	"commands\000"
.LASF24:
	.ascii	"LCKR\000"
.LASF19:
	.ascii	"OTYPER\000"
.LASF61:
	.ascii	"argc\000"
.LASF59:
	.ascii	"write_data\000"
.LASF37:
	.ascii	"AHB3ENR\000"
.LASF58:
	.ascii	"write_cmd\000"
.LASF10:
	.ascii	"long long int\000"
.LASF62:
	.ascii	"ILI9341_set_window\000"
.LASF21:
	.ascii	"PUPDR\000"
.LASF7:
	.ascii	"long int\000"
.LASF63:
	.ascii	"ILI9341_rectangle\000"
.LASF53:
	.ascii	"RCC_TypeDef\000"
.LASF9:
	.ascii	"long unsigned int\000"
.LASF18:
	.ascii	"MODER\000"
.LASF33:
	.ascii	"APB2RSTR\000"
.LASF3:
	.ascii	"__uint8_t\000"
.LASF22:
	.ascii	"BSRRL\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF40:
	.ascii	"APB2ENR\000"
.LASF65:
	.ascii	"ILI9341_set_window2\000"
.LASF0:
	.ascii	"signed char\000"
.LASF54:
	.ascii	"ILI9341_write_cmd\000"
.LASF11:
	.ascii	"long long unsigned int\000"
.LASF16:
	.ascii	"uint32_t\000"
.LASF12:
	.ascii	"unsigned int\000"
.LASF29:
	.ascii	"AHB2RSTR\000"
.LASF14:
	.ascii	"uint16_t\000"
.LASF72:
	.ascii	"init_ILI9341_GPIO\000"
.LASF26:
	.ascii	"PLLCFGR\000"
.LASF46:
	.ascii	"APB1LPENR\000"
.LASF74:
	.ascii	"init_SPI1\000"
.LASF67:
	.ascii	"char\000"
.LASF55:
	.ascii	"ILI9341_write_data\000"
.LASF15:
	.ascii	"int32_t\000"
.LASF5:
	.ascii	"short unsigned int\000"
.LASF69:
	.ascii	"GNU C 4.9.3 20141119 (release) [ARM/embedded-4_9-br"
	.ascii	"anch revision 218278] -mlittle-endian -mthumb -mcpu"
	.ascii	"=cortex-m4 -mthumb-interwork -mfloat-abi=hard -mfpu"
	.ascii	"=fpv4-sp-d16 -g -O0 -fsingle-precision-constant\000"
.LASF57:
	.ascii	"ILI9341_write\000"
.LASF56:
	.ascii	"data\000"
.LASF43:
	.ascii	"AHB2LPENR\000"
.LASF31:
	.ascii	"RESERVED0\000"
.LASF34:
	.ascii	"RESERVED1\000"
.LASF38:
	.ascii	"RESERVED2\000"
.LASF20:
	.ascii	"OSPEEDR\000"
.LASF45:
	.ascii	"RESERVED4\000"
.LASF48:
	.ascii	"RESERVED5\000"
.LASF50:
	.ascii	"RESERVED6\000"
.LASF35:
	.ascii	"AHB1ENR\000"
.LASF6:
	.ascii	"__int32_t\000"
.LASF68:
	.ascii	"ILI9341_init_cmd\000"
.LASF17:
	.ascii	"sizetype\000"
.LASF28:
	.ascii	"AHB1RSTR\000"
.LASF25:
	.ascii	"GPIO_TypeDef\000"
.LASF23:
	.ascii	"BSRRH\000"
.LASF30:
	.ascii	"AHB3RSTR\000"
.LASF75:
	.ascii	"ITM_RxBuffer\000"
.LASF66:
	.ascii	"ILI9341_image\000"
.LASF47:
	.ascii	"APB2LPENR\000"
	.ident	"GCC: (GNU Tools for ARM Embedded Processors) 4.9.3 20141119 (release) [ARM/embedded-4_9-branch revision 218278]"
