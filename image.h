#ifndef IMAGE_H
#define IMAGE_H

struct image {
  unsigned int 	 width;
  unsigned int 	 height;
  unsigned int 	 bytes_per_pixel; /* 2:RGB16, 3:RGB, 4:RGBA */ 
  unsigned char	 pixel_data[240 * 240 * 2 + 1];
}; 

#endif
