#include "stm32f4xx.h" 
#include "spi.h"
#include "ili9341_driver.h"
#include "image.h"
#include "print.h"
#include "line_draw.h"

#define max(A, B) (A > B ? A : B)

typedef struct {
	char r;
	char g;
	char b;
} COLOR;





COLOR line_color;

char image_buffer[240*240*2];
char image_buffer_tmp[60*60*2];

char color_lo, color_hi;


COLOR get_pixel(int x, int y)
{
	COLOR c;
	int idx = 2 * (240 * x + y);

	c.r = image_buffer[idx] & 0xF8; 
	c.g = image_buffer[idx] << 5 | ((image_buffer[idx+1] >> 3) & 0x1C);
	c.b = image_buffer[idx+1] << 3;

	return c;
}


void set_pixel(int x, int y, COLOR color) 
{
	int idx = 2 * (240 * x + y);
	
	image_buffer[idx] = (color.r & 0xF8) | (color.g >> 5);
	image_buffer[idx+1] = ((color.g << 3) & 0xE0) | ((color.b >> 3) & 0x1F);
}


void setPixelAA(int x, int y, int i) 
{
	static int count = 0;

	float a;	
	COLOR color, backcolor;

	if (x < 0 || x >= 240 || y < 0 || y >= 240) 
		return;
      
	a = 1 - i / 255.0;
  backcolor = get_pixel(x, y);

	color.r = (1 - a) * backcolor.r + a * line_color.r;
	color.g = (1 - a) * backcolor.g + a * line_color.g;
	color.b = (1 - a) * backcolor.b + a * line_color.b;


	if(count > 100 && count < 200)
	{	
		print_terminal("255(1-a) = %d\r\n", i);	
		print_terminal("BACKGROUND COLOR: %d %d %d\r\n", backcolor.r, backcolor.g, backcolor.b);
		print_terminal("LINE COLOR: %d %d %d\r\n", line_color.r, line_color.g, line_color.b);
		print_terminal("RESULT COLOR: %d %d %d\r\n", color.r, color.g, color.b);
		print_terminal("-----------------------------------------\r\n");	
	}
	++count;

	set_pixel(x, y, color);
}


void plotLineAA(int x0, int y0, int x1, int y1)
{
   int dx = abs(x1-x0), sx = x0<x1 ? 1 : -1;
   int dy = abs(y1-y0), sy = y0<y1 ? 1 : -1; 
   int err = dx-dy, e2, x2;                       /* error value e_xy */
   int ed = dx+dy == 0 ? 1 : sqrt((float)dx*dx+(float)dy*dy);

   for ( ; ; ){                                         /* pixel loop */
      setPixelAA(x0,y0, 255*abs(err-dx+dy)/ed);
      e2 = err; x2 = x0;
      if (2*e2 >= -dx) {                                    /* x step */
         if (x0 == x1) break;
         if (e2+dy < ed) setPixelAA(x0,y0+sy, 255*(e2+dy)/ed);
         err -= dy; x0 += sx; 
      } 
      if (2*e2 <= dy) {                                     /* y step */
         if (y0 == y1) break;
         if (dx-e2 < ed) setPixelAA(x2+sx,y0, 255*(dx-e2)/ed);
         err += dx; y0 += sy; 
    }
  }
}
  



void plotLineWidth(int x0, int y0, int x1, int y1, float wd)
{ 
   int dx = abs(x1-x0), sx = x0 < x1 ? 1 : -1; 
   int dy = abs(y1-y0), sy = y0 < y1 ? 1 : -1; 
   int err = dx-dy, e2, x2, y2;                          /* error value e_xy */
   float ed = dx+dy == 0 ? 1 : sqrt((float)dx*dx+(float)dy*dy);
   
   for (wd = (wd+1)/2; ; ) {                                   /* pixel loop */
      setPixelAA(x0,y0,max(0,255*(abs(err-dx+dy)/ed-wd+1)));
      e2 = err; x2 = x0;
      if (2*e2 >= -dx) {                                           /* x step */
         for (e2 += dy, y2 = y0; e2 < ed*wd && (y1 != y2 || dx > dy); e2 += dx)
            setPixelAA(x0, y2 += sy, max(0,255*(abs(e2)/ed-wd+1)));
         if (x0 == x1) break;
         e2 = err; err -= dy; x0 += sx; 
      } 
      if (2*e2 <= dy) {                                            /* y step */
         for (e2 = dx-e2; e2 < ed*wd && (x1 != x2 || dx < dy); e2 += dy)
            setPixelAA(x2 += sx, y0, max(0,255*(abs(e2)/ed-wd+1)));
         if (y0 == y1) break;
         err += dx; y0 += sy; 
      }
   }
}


void plotCircleAA(int xm, int ym, int r)
{                     /* draw a black anti-aliased circle on white background */
   int x = -r, y = 0;           /* II. quadrant from bottom left to top right */
   int i, x2, e2, err = 2-2*r;                             /* error of 1.step */
   r = 1-err;
   do {
      i = 255*abs(err-2*(x+y)-2)/r;               /* get blend value of pixel */
      setPixelAA(xm-x, ym+y, i);                             /*   I. Quadrant */
      setPixelAA(xm-y, ym-x, i);                             /*  II. Quadrant */
      setPixelAA(xm+x, ym-y, i);                             /* III. Quadrant */
      setPixelAA(xm+y, ym+x, i);                             /*  IV. Quadrant */
      e2 = err; x2 = x;                                    /* remember values */
      if (err+y > 0) {                                              /* x step */
         i = 255*(err-2*x-1)/r;                              /* outward pixel */
         if (i < 256) {
            setPixelAA(xm-x, ym+y+1, i);
            setPixelAA(xm-y-1, ym-x, i);
            setPixelAA(xm+x, ym-y-1, i);
            setPixelAA(xm+y+1, ym+x, i);
         }
         err += ++x*2+1;
      }
      if (e2+x2 <= 0) {                                             /* y step */
         i = 255*(2*y+3-e2)/r;                                /* inward pixel */
         if (i < 256) {
            setPixelAA(xm-x2-1, ym+y, i);
            setPixelAA(xm-y, ym-x2-1, i);
            setPixelAA(xm+x2+1, ym-y, i);
            setPixelAA(xm+y, ym+x2+1, i);
         }
         err += ++y*2+1;
      }
   } while (x < 0);
}





extern const char clock_img[]; 

const char gamma_correction_LUT[256] = {
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,3,3,3,3,3,4,4,4,4,5,5,5,5,6,6,6,6,7,7,7,8,8,8,9,9,9,10,10,10,11,11,12,12,12,13,13,14,14,15,15,16,16,17,17,18,18,19,19,20,20,21,21,22,23,23,24,24,25,26,26,27,27,28,29,29,30,31,32,32,33,34,34,35,36,37,37,38,39,40,41,41,42,43,44,45,46,46,47,48,49,50,51,52,53,54,55,55,56,57,58,59,60,61,62,63,64,65,67,68,69,70,71,72,73,74,75,76,78,79,80,81,82,83,85,86,87,88,89,91,92,93,94,96,97,98,100,101,102,104,105,106,108,109,110,112,113,115,116,117,119,120,122,123,125,126,128,129,131,132,134,135,137,139,140,142,143,145,147,148,150,151,153,155,156,158,160,161,163,165,167,168,170,172,174,175,177,179,181,183,185,186,188,190,192,194,196,198,200,201,203,205,207,209,211,213,215,217,219,221,223,225,227,229,232,234,236,238,240,242,244,246,248,251,253,255
};

const char gamma_correction_LUT2[256] = {
0,21,28,34,39,43,46,50,53,56,58,61,63,66,68,70,72,74,76,78,80,82,84,85,87,89,90,92,93,95,96,98,99,100,102,103,105,106,107,108,110,111,112,113,115,116,117,118,119,120,121,122,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,141,142,143,144,145,146,147,148,149,149,150,151,152,153,154,154,155,156,157,158,159,159,160,161,162,162,163,164,165,166,166,167,168,169,169,170,171,172,172,173,174,174,175,176,177,177,178,179,179,180,181,181,182,183,183,184,185,185,186,187,187,188,189,189,190,191,191,192,193,193,194,194,195,196,196,197,198,198,199,199,200,201,201,202,202,203,204,204,205,205,206,207,207,208,208,209,209,210,211,211,212,212,213,213,214,215,215,216,216,217,217,218,218,219,219,220,221,221,222,222,223,223,224,224,225,225,226,226,227,227,228,228,229,229,230,231,231,232,232,233,233,234,234,235,235,236,236,237,237,238,238,239,239,239,240,240,241,241,242,242,243,243,244,244,245,245,246,246,247,247,248,248,249,249,249,250,250,251,251,252,252,253,253,254,254,255,255
};

struct pixel{
	char x;
	char y;
};

const struct pixel clock_addon[] = {
	{ 30, 120 },	
	{ 31, 120 },
	{ 32, 120 },
	{ 33, 120 },
	{ 34, 120 },
	{ 35, 120 },
	{ 36, 120 },
	{ 37, 120 },
	{ 38, 120 },
	{ 39, 120 }
};



char DMA2_Stream7_compleated = 1;


void memory_copy(char dst[], const char src[], uint16_t n)
{
	// (technical paper pg.17)
	// DMA2 is located on the APB1 bus
	RCC->AHB1ENR |= RCC_AHB1ENR_DMA2EN;

	// (reference manual pg.305)
	// Stream7, Channel6 => free

	// (reference manual pg.325)
	// Select channel 6 => '110'b
	DMA2_Stream7->CR =  DMA_SxCR_CHSEL_2 | DMA_SxCR_CHSEL_1;


	//Memory burst transfer configuration
	//burst of 16 beats
	DMA2_Stream7->CR |= DMA_SxCR_MBURST;


	//Peripheral burst transfer configuration
	//burst of 16 beats
	DMA2_Stream7->CR |= DMA_SxCR_PBURST;


	// Priority level Very High => '11'b
	DMA2_Stream7->CR |= DMA_SxCR_PL;

	// Memory data size is byte(8-bit) => '00'b
	DMA2_Stream7->CR &= ~DMA_SxCR_MSIZE;

	// Peripheral data size is byte(8-bit) => '00'b
	DMA2_Stream7->CR &=  ~DMA_SxCR_PSIZE;

	// Memory and peripheral increment mode
	DMA2_Stream7->CR |= DMA_SxCR_MINC | DMA_SxCR_PINC;

	// Data transfer direction memory to memory => '10'b
	DMA2_Stream7->CR |= DMA_SxCR_DIR_1;

	// Enable transfer complete interrupt
	DMA2_Stream7->CR |= DMA_SxCR_TCIE;

	// Clear event flag for Stream7
	DMA2->HIFCR |= DMA_HIFCR_CTCIF7 | DMA_HIFCR_CHTIF7 | DMA_HIFCR_CTEIF7 |                  
								 DMA_HIFCR_CDMEIF7 | DMA_HIFCR_CFEIF7;                     


	// Number of data items to transfer on Stream1 
	DMA2_Stream7->NDTR = n;

	// Peripheral address register
	DMA2_Stream7->PAR = src;

	// Memory0 address register
	DMA2_Stream7->M0AR = dst;

	// FIFO treshold set to full FIFO
	// Direct mode disabled
	DMA2_Stream7->FCR = DMA_SxFCR_DMDIS | DMA_SxFCR_FTH;

	//Enable interrupt for DMA2 Stream7
	NVIC_EnableIRQ(DMA2_Stream7_IRQn);

	DMA2_Stream7_compleated = 0; 

	// Enable DMA2 Stream7
	DMA2_Stream7->CR |= DMA_SxCR_EN;
}



void DMA2_Stream7_IRQHandler()
{

	DMA2->HIFCR |= DMA_HIFCR_CTCIF7 | DMA_HIFCR_CHTIF7 | DMA_HIFCR_CTEIF7 |                  
								 DMA_HIFCR_CDMEIF7 | DMA_HIFCR_CFEIF7;                     

	DMA2_Stream7_compleated = 1; 
}



int img_buff_n, img_buff_tot;
int img_buff_src;
int img_buff_dst;
char img_buffer_copy_compleated = 1;


void init_img_buffer_copy()
{
	img_buff_n = 0;
	img_buff_tot = 240 * 240 * 2;

	img_buff_src = (int)clock_img;
	img_buff_dst = (int)image_buffer;
	
	img_buffer_copy_compleated = 0;
}

void process_img_buffer_copy()
{
	int inc;

	if(img_buff_n < img_buff_tot)
	{
		if(DMA2_Stream7_compleated)
		{
			inc = img_buff_tot - img_buff_n;
			if(inc > 64000)
				inc = 64000;

			memory_copy(img_buff_dst + img_buff_n, 
									img_buff_src + img_buff_n, 
									inc);

			img_buff_n += inc;
		}
	}
	else
		img_buffer_copy_compleated = 1;
}





void init_clock(){
	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;

	// -Clock division == 1
	// -Edge-aligned mode, counter count uo od down depending on the DIR
	// -DIR is set to '0', counter used as upcounter
	TIM2->CR1 = 0x0;

	// -ARR register is buffered.
	// -Only counter overflow/underflow generates and update interrupt.
	TIM2->CR1 |= TIM_CR1_ARPE | TIM_CR1_URS;


	// Slave mode disabled, prescaler is clocked directly by the internal clock
	TIM2->SMCR &= ~TIM_SMCR_SMS;

	TIM2->PSC = 42000 - 1; // 84MHz / 42000 = 2kHz; 
	TIM2->ARR = 2000 ; 

	// Update generation
	TIM2->EGR |= TIM_EGR_UG;

	// Enable update event interrupt generation
	TIM2->DIER |= TIM_DIER_UIE;

	// Enable TIM2 interrupt
	NVIC_EnableIRQ(TIM2_IRQn);

	// Counter enabled
	TIM2->CR1 = TIM_CR1_CEN;
}



char show_next_frame = 1;

void TIM2_IRQHandler()
{
	TIM2->SR &= ~TIM_SR_UIF;	
	show_next_frame = 1; 
}





	
/*
void init_img_buffer()
{
	int i;

	for(i = 0; i < 240 * 240 * 2; ++i)
		image_buffer[i] = clock_img[i];
}
*/

void draw_pixelmap(struct pixel pixels[], uint32_t pixels_sz, uint16_t color)
{
	int i, idx;

	for(i = 0; i < pixels_sz; ++i)
	{
		idx = 2 * (240 * pixels[i].x + pixels[i].y);
		image_buffer[idx] = color & 0xff;
		image_buffer[idx + 1] = color >> 8;
	}
}

const float sin_value[] = {  
	1,
	0.99452,
	0.97815,
	0.95106,
	0.91355,
	0.86603,
	0.80902,
	0.74314,
	0.66913,
	0.58779,
	0.5,
	0.40674,
	0.30902,
	0.20791,
	0.10453,
	0,
	-0.10453,
	-0.20791,
	-0.30902,
	-0.40674,
	-0.5,
	-0.58779,
	-0.66913,
	-0.74314,
	-0.80902,
	-0.86603,
	-0.91355,
	-0.95106,
	-0.97815,
	-0.99452,
	-1,
	-0.99452,
	-0.97815,
	-0.95106,
	-0.91355,
	-0.86603,
	-0.80902,
	-0.74314,
	-0.66913,
	-0.58779,
	-0.5,
	-0.40674,
	-0.30902,
	-0.20791,
	-0.10453,
	0,
	0.10453,
	0.20791,
	0.30902,
	0.40674,
	0.5,
	0.58779,
	0.66913,
	0.74314,
	0.80902,
	0.86603,
	0.91355,
	0.95106,
	0.97815,
	0.99452,

};

const float cos_value[] = {
	0,
	-0.10453,
	-0.20791,
	-0.30902,
	-0.40674,
	-0.5,
	-0.58779,
	-0.66913,
	-0.74314,
	-0.80902,
	-0.86603,
	-0.91355,
	-0.95106,
	-0.97815,
	-0.99452,
	-1,
	-0.99452,
	-0.97815,
	-0.95106,
	-0.91355,
	-0.86603,
	-0.80902,
	-0.74314,
	-0.66913,
	-0.58779,
	-0.5,
	-0.40674,
	-0.30902,
	-0.20791,
	-0.10453,
	0,
	0.10453,
	0.20791,
	0.30902,
	0.40674,
	0.5,
	0.58779,
	0.66913,
	0.74314,
	0.80902,
	0.86603,
	0.91355,
	0.95106,
	0.97815,
	0.99452,
	1,
	0.99452,
	0.97815,
	0.95106,
	0.91355,
	0.86603,
	0.80902,
	0.74314,
	0.66913,
	0.58779,
	0.5,
	0.40674,
	0.30902,
	0.20791,
	0.10453
};


float abs_f(float x)
{
	return x > 0 ? x : -x;
}


void draw_line(int x_start, int y_start, int r, int angle, COLOR c, int w)
{
	int x_end, y_end;
	
	c.r = gamma_correction_LUT[c.r];
	c.g = gamma_correction_LUT[c.g];
  c.b = gamma_correction_LUT[c.b];

	line_color = c;

	x_end = x_start - r * sin_value[(angle % 360) / 6];
	y_end = y_start - r * cos_value[(angle % 360) / 6];


	//plotLineAA(x_start, y_start, x_end, y_end);
	
	plotLineWidth(x_start, y_start, x_end, y_end, w);
	
	//if(x_start == x_end || y_start == y_end)
  //	plotLineWidth(x_start, y_start, x_end, y_end, w+2);
	//else	
  //	plotLineWidth(x_start, y_start, x_end, y_end, w);
	
	//myLine(image_buffer, x_start, y_start, x_end, y_end);
}

void draw_circle(int x_center, int y_center, int r, COLOR c)
{
	int i;

	c.r = gamma_correction_LUT[c.r];
	c.g = gamma_correction_LUT[c.g];
  c.b = gamma_correction_LUT[c.b];
	
	line_color = c;

	for(i = 1; i <= r; ++i)
		plotCircleAA(x_center, y_center, i);
}


int main(void)
{
	char init_image_buffer_state = 3;	

	COLOR sec_color = {255, 0, 0};		
	COLOR min_color = {255, 255, 255};	
	COLOR hour_color = {255, 255, 255};	

	int sec = 0;	
	int min = 40;
	int hour = 7;

	init_UART4();
	ILI9341_init();
	init_clock();

	print_terminal("INIT COMPLEATED");
	
	//init_img_buffer();
	//init_img_buffer3();
	

	while(1)
	{
		switch(init_image_buffer_state)
		{
			case 0:
				init_img_buffer_copy();
				init_image_buffer_state = 1;
				break;

			case 1:
				if(img_buffer_copy_compleated != 1)
					process_img_buffer_copy();
				else
					init_image_buffer_state = 2;
				break;

			case 2:	
				draw_line(120, 120, 100, 6 * min, min_color, 2);
				draw_line(120, 120, 50, 30 * hour, hour_color, 3);	
				draw_line(120, 120, 100, 6 * sec, sec_color, 2);
				init_image_buffer_state = 3;
				break;

			case 3:
				if(show_next_frame)
				{	
					ILI9341_image(image_buffer, 40, 0, 280, 240);     	
					init_image_buffer_state = 0;
					show_next_frame = 0;
				
					++sec;
		
					if(sec == 60)
					{
						sec = 0;
						++min;
					}	

					if(min == 60)
					{
						min = 0;
						++hour;
					}

					if(hour == 12)
						hour = 0;
				}
				break;
		}
	}
	
	return 0;
}
