#include "spi.h"


void init_SPI1()
{
	// (technical paper pg.17)
	// SPI1 is connected to APB2 bus
	RCC->APB2ENR |=	RCC_APB2ENR_SPI1EN;

	// (technical paper pg.56)
	// SPI1_NSS  -> PA4
	// SPI1_SCK  -> PA5
	// SPI1_MISO -> PA6
	// SPI1_MOSI -> PA7

	// All GPIO ports are connected to AHB1 bus
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;

	// (reference manual pg.278) 
	// Set GPIOs mode as alternate function mode
	GPIOA->MODER &= ~( GPIO_MODER_MODER4 |
										 GPIO_MODER_MODER5 |
										 GPIO_MODER_MODER6 |
										 GPIO_MODER_MODER7 );


	GPIOA->MODER |= 	GPIO_MODER_MODER4_1 
									| GPIO_MODER_MODER5_1
								  | GPIO_MODER_MODER6_1
									| GPIO_MODER_MODER7_1; 

	// (reference manual pg.279)
	// Set GPIOs output type to push-pull configuration
	GPIOA->OTYPER &= ~( GPIO_OTYPER_OT_4 |
											GPIO_OTYPER_OT_5 |
											GPIO_OTYPER_OT_6 |
											GPIO_OTYPER_OT_7 );
	
	// (reference manual pg.279)
	// Set GPIOs output speed to 'High speed'
	GPIOA->OSPEEDR |=   GPIO_OSPEEDER_OSPEEDR4
										| GPIO_OSPEEDER_OSPEEDR5
										| GPIO_OSPEEDER_OSPEEDR6
										| GPIO_OSPEEDER_OSPEEDR7;			


	// (reference manual pg.280)
	// Set GPIOs configuration to no pull-up, pull-down
	GPIOA->PUPDR &= ~( GPIO_PUPDR_PUPDR4 |
										 GPIO_PUPDR_PUPDR5 |
										 GPIO_PUPDR_PUPDR6 |	
										 GPIO_PUPDR_PUPDR7 );

	// (reference manual pg.283)
	// (technical paper pg.56)
	// Set GPIOs alternate function to AF5 aka. SPI1
	GPIOA->AFR[0] &= ~0xFFFF0000; // reset value of 16 MS bits

	GPIOA->AFR[0] |= 0x55550000; 

	// (reference manual pg.901)
	// Bits description:
	// BIDIMODE[15], value '0'b => 2-line unidirectional data mode selected
	// BIDIOE[14], not used if BIDIMODE value is '0'b
	// CRCEN[13], value '0'b => CRC calculation disabled
	// CRCNEXT[12], value '0'b => Data phase(no CRC phase)
	// DFF[11], value '0'b => 8-bit data frame format is selected for T/R
	// RXONLY[10], value '0'b => Full duplex, used when BIDIMODE value is '0'b
	// SSM[9], value '0'b => software slave managment disabled
	// SSI[8], has only effect when SSM bit value is '1'b
	// LSBFIRST[7], value '0'b => MSB transmitted first
	// SPE[6], value '0'b => peripheral disabled, value '1'b => peripheral enabled
	// BR[5:3], value '000'b => fpclk/2
	// MSTR[2], value '1'b => Master configuration
	// CPOL[1], value '0'b => CK to 0 when idle
	// CPHA[0], value '0'b => first clock transition is the first data capture edge
	
	SPI1->CR1 = SPI_CR1_MSTR;



	// (reference manual pg.903)
	// TXEIE[7], value '0'b => TXE interrupt masked
	// RXNEIE[6], value '0'b => RXNE interrupt masked
	// ERRIE[5], value '0'b => Error interrupt is masked
	// FRF[4], value '0'b => SPI Motorola mode
	// SSOE[2], value '1'b => SS output is enabled in master mode
	// TXDMAEN[1], value '0'b => Tx buffer DMA disabled
	// RXDMAEN[0], value '0'b => Rx buffer DMA disabled

	SPI1->CR2 = SPI_CR2_SSOE;



	// (reference manual pg.909)
	// I2SMODE[11], value '0'b => SPI mode is selected
	// I2SE[10], value '0'b => I2S peripheral is disabled

	SPI1->I2SCFGR &= ~( SPI_I2SCFGR_I2SE   |
										  SPI_I2SCFGR_I2SMOD );             



	SPI1->CR1 |= SPI_CR1_SPE;
}	


uint8_t SPI1_write(uint8_t data)
{
	//(reference manual pg.906)
	SPI1->DR = data;
	
	//(reference manual pg.905)
	while(!(SPI1->SR & SPI_SR_RXNE));
	while(!(SPI1->SR & SPI_SR_TXE));
	while((SPI1->SR & SPI_SR_BSY));

	return SPI1->DR;
}



