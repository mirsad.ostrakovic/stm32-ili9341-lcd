#ifndef LINE_DRAW_H
#define LINE_DRAW_H

typedef char SURFACE;
typedef unsigned char bool;

#define true 1u
#define false 0u

extern char image_buffer[240*240*2];

extern char color_hi;
extern char color_lo;


void myPixel(SURFACE* surface, int x,int y);
void myLine(SURFACE* surface, int x, int y, int x2, int y2);
void mySquare(SURFACE* surface,int x, int y, int x2, int y2);
void myRect(SURFACE* surface, int x, int y, int x2, int y2);

#endif
