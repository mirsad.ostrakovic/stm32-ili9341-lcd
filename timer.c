#include "timer.h"

void wait_ms(uint16_t ms)
{
    if(ms == 0)
        return;

    if(ms > 5000u)
        ms = 5000u;

    RCC->APB2ENR |= RCC_APB2ENR_TIM10EN;        
    TIM10->PSC = 16800 - 1;
    TIM10->ARR = 10 * ms;
    TIM10->CR1 |= TIM_CR1_URS | TIM_CR1_ARPE;
    TIM10->EGR |= TIM_EGR_UG;
    TIM10->CR1 |= TIM_CR1_CEN;

    while((TIM10->SR & 0x1) != 0x1);
    
    TIM10->SR &= ~0x1;
    TIM10->CR1 &= ~TIM_CR1_CEN;
}

void wait_us(uint16_t us)
{
    if(us == 0)
        return;

    if(us > 50000u)
        us = 50000u;

    RCC->APB2ENR |= RCC_APB2ENR_TIM10EN;        
    TIM10->PSC = 168 - 1;
    TIM10->ARR = us;
    TIM10->CR1 |= TIM_CR1_URS | TIM_CR1_ARPE;
    TIM10->EGR |= TIM_EGR_UG;
    TIM10->CR1 |= TIM_CR1_CEN;

    while((TIM10->SR & 0x1) != 0x1);
    
    TIM10->SR &= ~0x1;
    TIM10->CR1 &= ~TIM_CR1_CEN;
}


