#include "line_draw.h"
// Extremely Fast Line Algorithm Var E (Addition Fixed Point PreCalc ModeX)
// Copyright 2001-2, By Po-Han Lin


// Freely useable in non-commercial applications as long as credits 
// to Po-Han Lin and link to http://www.edepot.com is provided in 
// source code and can been seen in compiled executable.  
// Commercial applications please inquire about licensing the algorithms.
//
// Lastest version at http://www.edepot.com/phl.html
// Note: This version is for small displays like on cell phones.
// with 256x256 max resolution.  For displays up to 65536x65536
// please visit http://www.edepot.com/linee.html

// used by myLine
void myPixel(SURFACE* surface, int x,int y) {
	// PLOT x,y point on surface
	int idx = 2 * (240 * x + y);
	
	surface[idx] = color_hi;
	surface[idx+1] = color_lo;
}


// THE EXTREMELY FAST LINE ALGORITHM Variation E (Addition Fixed Point PreCalc Small Display)
// Small Display (256x256) resolution.
void myLine(SURFACE* surface, int x, int y, int x2, int y2) {
	int j; 
	bool yLonger = false;
	
	int shortLen=y2-y;
	int longLen=x2-x;
	if (abs(shortLen)>abs(longLen)) {
		int swap=shortLen;
		shortLen=longLen;
		longLen=swap;				
		yLonger=true;
	}
	int decInc;
	if (longLen==0) decInc=0;
	else decInc = (shortLen << 8) / longLen;

	if (yLonger) {
		if (longLen>0) {
			longLen+=y;
			for (j=0x80+(x<<8);y<=longLen;++y) {
				myPixel(surface,j >> 8,y);	
				j+=decInc;
			}
			return;
		}
		longLen+=y;
		for (j=0x80+(x<<8);y>=longLen;--y) {
			myPixel(surface,j >> 8,y);	
			j-=decInc;
		}
		return;	
	}

	if (longLen>0) {
		longLen+=x;
		for (j=0x80+(y<<8);x<=longLen;++x) {
			myPixel(surface,x,j >> 8);
			j+=decInc;
		}
		return;
	}
	longLen+=x;
	for (j=0x80+(y<<8);x>=longLen;--x) {
		myPixel(surface,x,j >> 8);
		j-=decInc;
	}

}

void mySquare(SURFACE* surface,int x, int y, int x2, int y2) {
	myLine(surface,x,y,x2,y2);
	myLine(surface,x2,y2,x2+(y-y2),y2+(x2-x));
	myLine(surface,x,y,x+(y-y2),y+(x2-x));
	myLine(surface,x+(y-y2),y+(x2-x),x2+(y-y2),y2+(x2-x));
}


void myRect(SURFACE* surface, int x, int y, int x2, int y2) {
	myLine(surface,x,y,x2,y);
	myLine(surface,x2,y,x2,y2);
	myLine(surface,x2,y2,x,y2);
	myLine(surface,x,y2,x,y);
}

