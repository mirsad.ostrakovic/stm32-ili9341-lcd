#ifndef TIMER_H
#define TIMER_H

#include "stm32f4xx.h"

void wait_ms(uint16_t ms);
void wait_us(uint16_t us);


#endif
