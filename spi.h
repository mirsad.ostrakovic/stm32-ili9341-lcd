#ifndef SPI_H
#define SPI_H

#include "stm32f4xx.h"

void init_spi1();
uint8_t SPI1_write(uint8_t data);

#endif
