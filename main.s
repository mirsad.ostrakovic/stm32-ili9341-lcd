	.syntax unified
	.cpu cortex-m4
	.eabi_attribute 27, 3
	.eabi_attribute 28, 1
	.fpu fpv4-sp-d16
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 1
	.eabi_attribute 30, 6
	.eabi_attribute 34, 1
	.eabi_attribute 18, 4
	.thumb
	.file	"main.c"
	.text
.Ltext0:
	.cfi_sections	.debug_frame
	.align	2
	.thumb
	.thumb_func
	.type	NVIC_EnableIRQ, %function
NVIC_EnableIRQ:
.LFB95:
	.file 1 "../../../STM32F407/Libraries/CMSIS/Include/core_cm4.h"
	.loc 1 1073 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	push	{r7}
	.cfi_def_cfa_offset 4
	.cfi_offset 7, -4
	sub	sp, sp, #12
	.cfi_def_cfa_offset 16
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	mov	r3, r0
	strb	r3, [r7, #7]
	.loc 1 1075 0
	ldr	r1, .L2
	ldrsb	r3, [r7, #7]
	lsrs	r3, r3, #5
	ldrb	r2, [r7, #7]	@ zero_extendqisi2
	and	r2, r2, #31
	movs	r0, #1
	lsl	r2, r0, r2
	str	r2, [r1, r3, lsl #2]
	.loc 1 1076 0
	adds	r7, r7, #12
	.cfi_def_cfa_offset 4
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	ldr	r7, [sp], #4
	.cfi_restore 7
	.cfi_def_cfa_offset 0
	bx	lr
.L3:
	.align	2
.L2:
	.word	-536813312
	.cfi_endproc
.LFE95:
	.size	NVIC_EnableIRQ, .-NVIC_EnableIRQ
	.comm	line_color,3,4
	.comm	image_buffer,115200,4
	.comm	image_buffer_tmp,7200,4
	.comm	color_lo,1,1
	.comm	color_hi,1,1
	.align	2
	.global	get_pixel
	.thumb
	.thumb_func
	.type	get_pixel, %function
get_pixel:
.LFB110:
	.file 2 "main.c"
	.loc 2 29 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	push	{r7}
	.cfi_def_cfa_offset 4
	.cfi_offset 7, -4
	sub	sp, sp, #28
	.cfi_def_cfa_offset 32
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	str	r0, [r7, #4]
	str	r1, [r7]
	.loc 2 31 0
	ldr	r2, [r7, #4]
	mov	r3, r2
	lsls	r3, r3, #4
	subs	r3, r3, r2
	lsls	r3, r3, #4
	mov	r2, r3
	ldr	r3, [r7]
	add	r3, r3, r2
	lsls	r3, r3, #1
	str	r3, [r7, #20]
	.loc 2 33 0
	ldr	r2, .L6
	ldr	r3, [r7, #20]
	add	r3, r3, r2
	ldrb	r3, [r3]	@ zero_extendqisi2
	bic	r3, r3, #7
	uxtb	r3, r3
	strb	r3, [r7, #12]
	.loc 2 34 0
	ldr	r2, .L6
	ldr	r3, [r7, #20]
	add	r3, r3, r2
	ldrb	r3, [r3]	@ zero_extendqisi2
	lsls	r3, r3, #5
	uxtb	r1, r3
	ldr	r3, [r7, #20]
	adds	r3, r3, #1
	ldr	r2, .L6
	ldrb	r3, [r2, r3]	@ zero_extendqisi2
	lsrs	r3, r3, #3
	uxtb	r3, r3
	uxtb	r3, r3
	and	r3, r3, #28
	uxtb	r3, r3
	mov	r2, r1
	orrs	r3, r3, r2
	uxtb	r3, r3
	uxtb	r3, r3
	strb	r3, [r7, #13]
	.loc 2 35 0
	ldr	r3, [r7, #20]
	adds	r3, r3, #1
	ldr	r2, .L6
	ldrb	r3, [r2, r3]	@ zero_extendqisi2
	lsls	r3, r3, #3
	uxtb	r3, r3
	strb	r3, [r7, #14]
	.loc 2 37 0
	add	r3, r7, #16
	add	r2, r7, #12
	ldr	r2, [r2]
	mov	r1, r2	@ movhi
	strh	r1, [r3]	@ movhi
	adds	r3, r3, #2
	lsrs	r2, r2, #16
	strb	r2, [r3]
	movs	r3, #0
	ldrb	r2, [r7, #16]	@ zero_extendqisi2
	bfi	r3, r2, #0, #8
	ldrb	r2, [r7, #17]	@ zero_extendqisi2
	bfi	r3, r2, #8, #8
	ldrb	r2, [r7, #18]	@ zero_extendqisi2
	bfi	r3, r2, #16, #8
	.loc 2 38 0
	mov	r0, r3
	adds	r7, r7, #28
	.cfi_def_cfa_offset 4
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	ldr	r7, [sp], #4
	.cfi_restore 7
	.cfi_def_cfa_offset 0
	bx	lr
.L7:
	.align	2
.L6:
	.word	image_buffer
	.cfi_endproc
.LFE110:
	.size	get_pixel, .-get_pixel
	.align	2
	.global	set_pixel
	.thumb
	.thumb_func
	.type	set_pixel, %function
set_pixel:
.LFB111:
	.loc 2 42 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	push	{r7}
	.cfi_def_cfa_offset 4
	.cfi_offset 7, -4
	sub	sp, sp, #28
	.cfi_def_cfa_offset 32
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	str	r0, [r7, #12]
	str	r1, [r7, #8]
	str	r2, [r7, #4]
	.loc 2 43 0
	ldr	r2, [r7, #12]
	mov	r3, r2
	lsls	r3, r3, #4
	subs	r3, r3, r2
	lsls	r3, r3, #4
	mov	r2, r3
	ldr	r3, [r7, #8]
	add	r3, r3, r2
	lsls	r3, r3, #1
	str	r3, [r7, #20]
	.loc 2 45 0
	ldrb	r3, [r7, #4]	@ zero_extendqisi2
	uxtb	r3, r3
	bic	r3, r3, #7
	uxtb	r2, r3
	ldrb	r3, [r7, #5]	@ zero_extendqisi2
	lsrs	r3, r3, #5
	uxtb	r3, r3
	uxtb	r3, r3
	orrs	r3, r3, r2
	uxtb	r3, r3
	uxtb	r1, r3
	ldr	r2, .L9
	ldr	r3, [r7, #20]
	add	r3, r3, r2
	mov	r2, r1
	strb	r2, [r3]
	.loc 2 46 0
	ldr	r3, [r7, #20]
	adds	r3, r3, #1
	ldrb	r2, [r7, #5]	@ zero_extendqisi2
	lsls	r2, r2, #3
	uxtb	r2, r2
	bic	r2, r2, #31
	uxtb	r1, r2
	ldrb	r2, [r7, #6]	@ zero_extendqisi2
	lsrs	r2, r2, #3
	uxtb	r2, r2
	uxtb	r2, r2
	orrs	r2, r2, r1
	uxtb	r2, r2
	uxtb	r1, r2
	ldr	r2, .L9
	strb	r1, [r2, r3]
	.loc 2 47 0
	adds	r7, r7, #28
	.cfi_def_cfa_offset 4
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	ldr	r7, [sp], #4
	.cfi_restore 7
	.cfi_def_cfa_offset 0
	bx	lr
.L10:
	.align	2
.L9:
	.word	image_buffer
	.cfi_endproc
.LFE111:
	.size	set_pixel, .-set_pixel
	.section	.rodata
	.align	2
.LC1:
	.ascii	"255(1-a) = %d\015\012\000"
	.align	2
.LC2:
	.ascii	"BACKGROUND COLOR: %d %d %d\015\012\000"
	.align	2
.LC3:
	.ascii	"LINE COLOR: %d %d %d\015\012\000"
	.align	2
.LC4:
	.ascii	"RESULT COLOR: %d %d %d\015\012\000"
	.align	2
.LC5:
	.ascii	"-----------------------------------------\015\012\000"
	.text
	.align	2
	.global	setPixelAA
	.thumb
	.thumb_func
	.type	setPixelAA, %function
setPixelAA:
.LFB112:
	.loc 2 51 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r7, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	sub	sp, sp, #32
	.cfi_def_cfa_offset 40
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	str	r0, [r7, #12]
	str	r1, [r7, #8]
	str	r2, [r7, #4]
	.loc 2 57 0
	ldr	r3, [r7, #12]
	cmp	r3, #0
	blt	.L11
	.loc 2 57 0 is_stmt 0 discriminator 1
	ldr	r3, [r7, #12]
	cmp	r3, #239
	bgt	.L11
	.loc 2 57 0 discriminator 2
	ldr	r3, [r7, #8]
	cmp	r3, #0
	blt	.L11
	.loc 2 57 0 discriminator 3
	ldr	r3, [r7, #8]
	cmp	r3, #239
	bgt	.L11
	.loc 2 60 0 is_stmt 1
	ldr	r3, [r7, #4]
	fmsr	s15, r3	@ int
	fsitos	s15, s15
	flds	s14, .L16
	fdivs	s15, s15, s14
	fconsts	s14, #112
	fsubs	s15, s14, s15
	fsts	s15, [r7, #28]
	.loc 2 61 0
	ldr	r0, [r7, #12]
	ldr	r1, [r7, #8]
	bl	get_pixel
	mov	r3, r0
	mov	r2, r3
	strb	r2, [r7, #20]
	ubfx	r2, r3, #8, #8
	strb	r2, [r7, #21]
	ubfx	r3, r3, #16, #8
	strb	r3, [r7, #22]
	.loc 2 63 0
	fconsts	s14, #112
	flds	s15, [r7, #28]
	fsubs	s14, s14, s15
	ldrb	r3, [r7, #20]	@ zero_extendqisi2
	fmsr	s15, r3	@ int
	fsitos	s15, s15
	fmuls	s14, s14, s15
	ldr	r3, .L16+4
	ldrb	r3, [r3]	@ zero_extendqisi2
	fmsr	s15, r3	@ int
	fsitos	s13, s15
	flds	s15, [r7, #28]
	fmuls	s15, s13, s15
	fadds	s15, s14, s15
	ftouizs	s15, s15
	fsts	s15, [r7]	@ int
	ldrb	r3, [r7]
	uxtb	r3, r3
	strb	r3, [r7, #24]
	.loc 2 64 0
	fconsts	s14, #112
	flds	s15, [r7, #28]
	fsubs	s14, s14, s15
	ldrb	r3, [r7, #21]	@ zero_extendqisi2
	fmsr	s15, r3	@ int
	fsitos	s15, s15
	fmuls	s14, s14, s15
	ldr	r3, .L16+4
	ldrb	r3, [r3, #1]	@ zero_extendqisi2
	fmsr	s15, r3	@ int
	fsitos	s13, s15
	flds	s15, [r7, #28]
	fmuls	s15, s13, s15
	fadds	s15, s14, s15
	ftouizs	s15, s15
	fsts	s15, [r7]	@ int
	ldrb	r3, [r7]
	uxtb	r3, r3
	strb	r3, [r7, #25]
	.loc 2 65 0
	fconsts	s14, #112
	flds	s15, [r7, #28]
	fsubs	s14, s14, s15
	ldrb	r3, [r7, #22]	@ zero_extendqisi2
	fmsr	s15, r3	@ int
	fsitos	s15, s15
	fmuls	s14, s14, s15
	ldr	r3, .L16+4
	ldrb	r3, [r3, #2]	@ zero_extendqisi2
	fmsr	s15, r3	@ int
	fsitos	s13, s15
	flds	s15, [r7, #28]
	fmuls	s15, s13, s15
	fadds	s15, s14, s15
	ftouizs	s15, s15
	fsts	s15, [r7]	@ int
	ldrb	r3, [r7]
	uxtb	r3, r3
	strb	r3, [r7, #26]
	.loc 2 68 0
	ldr	r3, .L16+8
	ldr	r3, [r3]
	cmp	r3, #100
	ble	.L15
	.loc 2 68 0 is_stmt 0 discriminator 1
	ldr	r3, .L16+8
	ldr	r3, [r3]
	cmp	r3, #199
	bgt	.L15
	.loc 2 70 0 is_stmt 1
	ldr	r0, .L16+12
	ldr	r1, [r7, #4]
	bl	print_terminal
	.loc 2 71 0
	ldrb	r3, [r7, #20]	@ zero_extendqisi2
	mov	r1, r3
	ldrb	r3, [r7, #21]	@ zero_extendqisi2
	mov	r2, r3
	ldrb	r3, [r7, #22]	@ zero_extendqisi2
	ldr	r0, .L16+16
	bl	print_terminal
	.loc 2 72 0
	ldr	r3, .L16+4
	ldrb	r3, [r3]	@ zero_extendqisi2
	mov	r1, r3
	ldr	r3, .L16+4
	ldrb	r3, [r3, #1]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L16+4
	ldrb	r3, [r3, #2]	@ zero_extendqisi2
	ldr	r0, .L16+20
	bl	print_terminal
	.loc 2 73 0
	ldrb	r3, [r7, #24]	@ zero_extendqisi2
	mov	r1, r3
	ldrb	r3, [r7, #25]	@ zero_extendqisi2
	mov	r2, r3
	ldrb	r3, [r7, #26]	@ zero_extendqisi2
	ldr	r0, .L16+24
	bl	print_terminal
	.loc 2 74 0
	ldr	r0, .L16+28
	bl	print_terminal
.L15:
	.loc 2 76 0
	ldr	r3, .L16+8
	ldr	r3, [r3]
	adds	r3, r3, #1
	ldr	r2, .L16+8
	str	r3, [r2]
	.loc 2 78 0
	ldr	r0, [r7, #12]
	ldr	r1, [r7, #8]
	ldr	r2, [r7, #24]
	bl	set_pixel
.L11:
	.loc 2 79 0
	adds	r7, r7, #32
	.cfi_def_cfa_offset 8
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	pop	{r7, pc}
.L17:
	.align	2
.L16:
	.word	1132396544
	.word	line_color
	.word	count.5473
	.word	.LC1
	.word	.LC2
	.word	.LC3
	.word	.LC4
	.word	.LC5
	.cfi_endproc
.LFE112:
	.size	setPixelAA, .-setPixelAA
	.global	__aeabi_f2d
	.global	__aeabi_d2iz
	.align	2
	.global	plotLineAA
	.thumb
	.thumb_func
	.type	plotLineAA, %function
plotLineAA:
.LFB113:
	.loc 2 83 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r7, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	sub	sp, sp, #48
	.cfi_def_cfa_offset 56
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	str	r0, [r7, #12]
	str	r1, [r7, #8]
	str	r2, [r7, #4]
	str	r3, [r7]
	.loc 2 84 0
	ldr	r2, [r7, #4]
	ldr	r3, [r7, #12]
	subs	r3, r2, r3
	cmp	r3, #0
	it	lt
	rsblt	r3, r3, #0
	str	r3, [r7, #40]
	ldr	r2, [r7, #12]
	ldr	r3, [r7, #4]
	cmp	r2, r3
	bge	.L19
	.loc 2 84 0 is_stmt 0 discriminator 1
	movs	r3, #1
	b	.L20
.L19:
	.loc 2 84 0 discriminator 2
	mov	r3, #-1
.L20:
	.loc 2 84 0 discriminator 4
	str	r3, [r7, #36]
	.loc 2 85 0 is_stmt 1 discriminator 4
	ldr	r2, [r7]
	ldr	r3, [r7, #8]
	subs	r3, r2, r3
	cmp	r3, #0
	it	lt
	rsblt	r3, r3, #0
	str	r3, [r7, #32]
	ldr	r2, [r7, #8]
	ldr	r3, [r7]
	cmp	r2, r3
	bge	.L21
	.loc 2 85 0 is_stmt 0 discriminator 1
	movs	r3, #1
	b	.L22
.L21:
	.loc 2 85 0 discriminator 2
	mov	r3, #-1
.L22:
	.loc 2 85 0 discriminator 4
	str	r3, [r7, #28]
	.loc 2 86 0 is_stmt 1 discriminator 4
	ldr	r2, [r7, #40]
	ldr	r3, [r7, #32]
	subs	r3, r2, r3
	str	r3, [r7, #44]
	.loc 2 87 0 discriminator 4
	ldr	r2, [r7, #40]
	ldr	r3, [r7, #32]
	add	r3, r3, r2
	cmp	r3, #0
	beq	.L23
	.loc 2 87 0 is_stmt 0 discriminator 1
	ldr	r3, [r7, #40]
	fmsr	s15, r3	@ int
	fsitos	s14, s15
	ldr	r3, [r7, #40]
	fmsr	s15, r3	@ int
	fsitos	s15, s15
	fmuls	s14, s14, s15
	ldr	r3, [r7, #32]
	fmsr	s15, r3	@ int
	fsitos	s13, s15
	ldr	r3, [r7, #32]
	fmsr	s15, r3	@ int
	fsitos	s15, s15
	fmuls	s15, s13, s15
	fadds	s15, s14, s15
	fmrs	r0, s15
	bl	__aeabi_f2d
	mov	r2, r0
	mov	r3, r1
	fmdrr	d0, r2, r3
	bl	sqrt
	fmrrd	r2, r3, d0
	mov	r0, r2
	mov	r1, r3
	bl	__aeabi_d2iz
	mov	r3, r0
	b	.L24
.L23:
	.loc 2 87 0 discriminator 2
	movs	r3, #1
.L24:
	.loc 2 87 0 discriminator 4
	str	r3, [r7, #24]
.L32:
	.loc 2 90 0 is_stmt 1
	ldr	r2, [r7, #44]
	ldr	r3, [r7, #40]
	subs	r2, r2, r3
	ldr	r3, [r7, #32]
	add	r3, r3, r2
	eor	r2, r3, r3, asr #31
	sub	r2, r2, r3, asr #31
	mov	r3, r2
	lsls	r3, r3, #8
	subs	r2, r3, r2
	ldr	r3, [r7, #24]
	sdiv	r3, r2, r3
	ldr	r0, [r7, #12]
	ldr	r1, [r7, #8]
	mov	r2, r3
	bl	setPixelAA
	.loc 2 91 0
	ldr	r3, [r7, #44]
	str	r3, [r7, #20]
	ldr	r3, [r7, #12]
	str	r3, [r7, #16]
	.loc 2 92 0
	ldr	r3, [r7, #20]
	lsls	r2, r3, #1
	ldr	r3, [r7, #40]
	negs	r3, r3
	cmp	r2, r3
	blt	.L25
	.loc 2 93 0
	ldr	r2, [r7, #12]
	ldr	r3, [r7, #4]
	cmp	r2, r3
	bne	.L26
	b	.L18
.L26:
	.loc 2 94 0
	ldr	r2, [r7, #20]
	ldr	r3, [r7, #32]
	add	r2, r2, r3
	ldr	r3, [r7, #24]
	cmp	r2, r3
	bge	.L28
	.loc 2 94 0 is_stmt 0 discriminator 1
	ldr	r2, [r7, #8]
	ldr	r3, [r7, #28]
	adds	r1, r2, r3
	ldr	r2, [r7, #20]
	ldr	r3, [r7, #32]
	add	r2, r2, r3
	mov	r3, r2
	lsls	r3, r3, #8
	subs	r2, r3, r2
	ldr	r3, [r7, #24]
	sdiv	r3, r2, r3
	ldr	r0, [r7, #12]
	mov	r2, r3
	bl	setPixelAA
.L28:
	.loc 2 95 0 is_stmt 1
	ldr	r2, [r7, #44]
	ldr	r3, [r7, #32]
	subs	r3, r2, r3
	str	r3, [r7, #44]
	ldr	r2, [r7, #12]
	ldr	r3, [r7, #36]
	add	r3, r3, r2
	str	r3, [r7, #12]
.L25:
	.loc 2 97 0
	ldr	r3, [r7, #20]
	lsls	r2, r3, #1
	ldr	r3, [r7, #32]
	cmp	r2, r3
	bgt	.L29
	.loc 2 98 0
	ldr	r2, [r7, #8]
	ldr	r3, [r7]
	cmp	r2, r3
	bne	.L30
	b	.L18
.L30:
	.loc 2 99 0
	ldr	r2, [r7, #40]
	ldr	r3, [r7, #20]
	subs	r2, r2, r3
	ldr	r3, [r7, #24]
	cmp	r2, r3
	bge	.L31
	.loc 2 99 0 is_stmt 0 discriminator 1
	ldr	r2, [r7, #16]
	ldr	r3, [r7, #36]
	adds	r1, r2, r3
	ldr	r2, [r7, #40]
	ldr	r3, [r7, #20]
	subs	r2, r2, r3
	mov	r3, r2
	lsls	r3, r3, #8
	subs	r2, r3, r2
	ldr	r3, [r7, #24]
	sdiv	r3, r2, r3
	mov	r0, r1
	ldr	r1, [r7, #8]
	mov	r2, r3
	bl	setPixelAA
.L31:
	.loc 2 100 0 is_stmt 1
	ldr	r2, [r7, #44]
	ldr	r3, [r7, #40]
	add	r3, r3, r2
	str	r3, [r7, #44]
	ldr	r2, [r7, #8]
	ldr	r3, [r7, #28]
	add	r3, r3, r2
	str	r3, [r7, #8]
.L29:
	.loc 2 102 0
	b	.L32
.L18:
	.loc 2 103 0
	adds	r7, r7, #48
	.cfi_def_cfa_offset 8
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	pop	{r7, pc}
	.cfi_endproc
.LFE113:
	.size	plotLineAA, .-plotLineAA
	.global	__aeabi_d2f
	.align	2
	.global	plotLineWidth
	.thumb
	.thumb_func
	.type	plotLineWidth, %function
plotLineWidth:
.LFB114:
	.loc 2 109 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 64
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r7, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	sub	sp, sp, #64
	.cfi_def_cfa_offset 72
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	str	r0, [r7, #20]
	str	r1, [r7, #16]
	str	r2, [r7, #12]
	str	r3, [r7, #8]
	fsts	s0, [r7, #4]
	.loc 2 110 0
	ldr	r2, [r7, #12]
	ldr	r3, [r7, #20]
	subs	r3, r2, r3
	cmp	r3, #0
	it	lt
	rsblt	r3, r3, #0
	str	r3, [r7, #44]
	ldr	r2, [r7, #20]
	ldr	r3, [r7, #12]
	cmp	r2, r3
	bge	.L34
	.loc 2 110 0 is_stmt 0 discriminator 1
	movs	r3, #1
	b	.L35
.L34:
	.loc 2 110 0 discriminator 2
	mov	r3, #-1
.L35:
	.loc 2 110 0 discriminator 4
	str	r3, [r7, #40]
	.loc 2 111 0 is_stmt 1 discriminator 4
	ldr	r2, [r7, #8]
	ldr	r3, [r7, #16]
	subs	r3, r2, r3
	cmp	r3, #0
	it	lt
	rsblt	r3, r3, #0
	str	r3, [r7, #36]
	ldr	r2, [r7, #16]
	ldr	r3, [r7, #8]
	cmp	r2, r3
	bge	.L36
	.loc 2 111 0 is_stmt 0 discriminator 1
	movs	r3, #1
	b	.L37
.L36:
	.loc 2 111 0 discriminator 2
	mov	r3, #-1
.L37:
	.loc 2 111 0 discriminator 4
	str	r3, [r7, #32]
	.loc 2 112 0 is_stmt 1 discriminator 4
	ldr	r2, [r7, #44]
	ldr	r3, [r7, #36]
	subs	r3, r2, r3
	str	r3, [r7, #60]
	.loc 2 113 0 discriminator 4
	ldr	r2, [r7, #44]
	ldr	r3, [r7, #36]
	add	r3, r3, r2
	cmp	r3, #0
	beq	.L38
	.loc 2 113 0 is_stmt 0 discriminator 1
	ldr	r3, [r7, #44]
	fmsr	s15, r3	@ int
	fsitos	s14, s15
	ldr	r3, [r7, #44]
	fmsr	s15, r3	@ int
	fsitos	s15, s15
	fmuls	s14, s14, s15
	ldr	r3, [r7, #36]
	fmsr	s15, r3	@ int
	fsitos	s13, s15
	ldr	r3, [r7, #36]
	fmsr	s15, r3	@ int
	fsitos	s15, s15
	fmuls	s15, s13, s15
	fadds	s15, s14, s15
	fmrs	r0, s15
	bl	__aeabi_f2d
	mov	r2, r0
	mov	r3, r1
	fmdrr	d0, r2, r3
	bl	sqrt
	fmrrd	r2, r3, d0
	mov	r0, r2
	mov	r1, r3
	bl	__aeabi_d2f
	mov	r3, r0	@ float
	b	.L39
.L38:
	.loc 2 113 0 discriminator 2
	mov	r3, #1065353216
.L39:
	.loc 2 113 0 discriminator 4
	str	r3, [r7, #28]	@ float
	.loc 2 115 0 is_stmt 1 discriminator 4
	flds	s15, [r7, #4]
	fconsts	s14, #112
	fadds	s15, s15, s14
	fconsts	s14, #0
	fdivs	s15, s15, s14
	fsts	s15, [r7, #4]
.L62:
	.loc 2 116 0
	ldr	r2, [r7, #60]
	ldr	r3, [r7, #44]
	subs	r2, r2, r3
	ldr	r3, [r7, #36]
	add	r3, r3, r2
	cmp	r3, #0
	it	lt
	rsblt	r3, r3, #0
	fmsr	s15, r3	@ int
	fsitos	s14, s15
	flds	s15, [r7, #28]
	fdivs	s14, s14, s15
	flds	s15, [r7, #4]
	fsubs	s15, s14, s15
	fconsts	s14, #112
	fadds	s15, s15, s14
	flds	s14, .L71
	fmuls	s15, s15, s14
	fcmpezs	s15
	fmstat
	bpl	.L68
	.loc 2 116 0 is_stmt 0 discriminator 1
	movs	r3, #0
	b	.L42
.L68:
	.loc 2 116 0 discriminator 2
	ldr	r2, [r7, #60]
	ldr	r3, [r7, #44]
	subs	r2, r2, r3
	ldr	r3, [r7, #36]
	add	r3, r3, r2
	cmp	r3, #0
	it	lt
	rsblt	r3, r3, #0
	fmsr	s15, r3	@ int
	fsitos	s14, s15
	flds	s15, [r7, #28]
	fdivs	s14, s14, s15
	flds	s15, [r7, #4]
	fsubs	s15, s14, s15
	fconsts	s14, #112
	fadds	s15, s15, s14
	flds	s14, .L71
	fmuls	s15, s15, s14
	ftosizs	s15, s15
	fmrs	r3, s15	@ int
.L42:
	.loc 2 116 0 discriminator 4
	ldr	r0, [r7, #20]
	ldr	r1, [r7, #16]
	mov	r2, r3
	bl	setPixelAA
	.loc 2 117 0 is_stmt 1 discriminator 4
	ldr	r3, [r7, #60]
	str	r3, [r7, #56]
	ldr	r3, [r7, #20]
	str	r3, [r7, #52]
	.loc 2 118 0 discriminator 4
	ldr	r3, [r7, #56]
	lsls	r2, r3, #1
	ldr	r3, [r7, #44]
	negs	r3, r3
	cmp	r2, r3
	blt	.L43
	.loc 2 119 0
	ldr	r2, [r7, #56]
	ldr	r3, [r7, #36]
	add	r3, r3, r2
	str	r3, [r7, #56]
	ldr	r3, [r7, #16]
	str	r3, [r7, #48]
	b	.L44
.L50:
	.loc 2 120 0
	ldr	r2, [r7, #48]
	ldr	r3, [r7, #32]
	add	r3, r3, r2
	str	r3, [r7, #48]
	ldr	r3, [r7, #56]
	cmp	r3, #0
	it	lt
	rsblt	r3, r3, #0
	fmsr	s15, r3	@ int
	fsitos	s14, s15
	flds	s15, [r7, #28]
	fdivs	s14, s14, s15
	flds	s15, [r7, #4]
	fsubs	s15, s14, s15
	fconsts	s14, #112
	fadds	s15, s15, s14
	flds	s14, .L71
	fmuls	s15, s15, s14
	fcmpezs	s15
	fmstat
	bpl	.L69
	.loc 2 120 0 is_stmt 0 discriminator 1
	movs	r3, #0
	b	.L47
.L69:
	.loc 2 120 0 discriminator 2
	ldr	r3, [r7, #56]
	cmp	r3, #0
	it	lt
	rsblt	r3, r3, #0
	fmsr	s15, r3	@ int
	fsitos	s14, s15
	flds	s15, [r7, #28]
	fdivs	s14, s14, s15
	flds	s15, [r7, #4]
	fsubs	s15, s14, s15
	fconsts	s14, #112
	fadds	s15, s15, s14
	flds	s14, .L71
	fmuls	s15, s15, s14
	ftosizs	s15, s15
	fmrs	r3, s15	@ int
.L47:
	.loc 2 120 0 discriminator 4
	ldr	r0, [r7, #20]
	ldr	r1, [r7, #48]
	mov	r2, r3
	bl	setPixelAA
	.loc 2 119 0 is_stmt 1 discriminator 4
	ldr	r2, [r7, #56]
	ldr	r3, [r7, #44]
	add	r3, r3, r2
	str	r3, [r7, #56]
.L44:
	.loc 2 119 0 is_stmt 0 discriminator 2
	ldr	r3, [r7, #56]
	fmsr	s15, r3	@ int
	fsitos	s14, s15
	flds	s13, [r7, #28]
	flds	s15, [r7, #4]
	fmuls	s15, s13, s15
	fcmpes	s14, s15
	fmstat
	bpl	.L48
	.loc 2 119 0 discriminator 3
	ldr	r2, [r7, #8]
	ldr	r3, [r7, #48]
	cmp	r2, r3
	bne	.L50
	.loc 2 119 0 discriminator 4
	ldr	r2, [r7, #44]
	ldr	r3, [r7, #36]
	cmp	r2, r3
	bgt	.L50
.L48:
	.loc 2 121 0 is_stmt 1
	ldr	r2, [r7, #20]
	ldr	r3, [r7, #12]
	cmp	r2, r3
	bne	.L51
	b	.L33
.L51:
	.loc 2 122 0
	ldr	r3, [r7, #60]
	str	r3, [r7, #56]
	ldr	r2, [r7, #60]
	ldr	r3, [r7, #36]
	subs	r3, r2, r3
	str	r3, [r7, #60]
	ldr	r2, [r7, #20]
	ldr	r3, [r7, #40]
	add	r3, r3, r2
	str	r3, [r7, #20]
.L43:
	.loc 2 124 0
	ldr	r3, [r7, #56]
	lsls	r2, r3, #1
	ldr	r3, [r7, #36]
	cmp	r2, r3
	bgt	.L53
	.loc 2 125 0
	ldr	r2, [r7, #44]
	ldr	r3, [r7, #56]
	subs	r3, r2, r3
	str	r3, [r7, #56]
	b	.L54
.L60:
	.loc 2 126 0
	ldr	r2, [r7, #52]
	ldr	r3, [r7, #40]
	add	r3, r3, r2
	str	r3, [r7, #52]
	ldr	r3, [r7, #56]
	cmp	r3, #0
	it	lt
	rsblt	r3, r3, #0
	fmsr	s15, r3	@ int
	fsitos	s14, s15
	flds	s15, [r7, #28]
	fdivs	s14, s14, s15
	flds	s15, [r7, #4]
	fsubs	s15, s14, s15
	fconsts	s14, #112
	fadds	s15, s15, s14
	flds	s14, .L71
	fmuls	s15, s15, s14
	fcmpezs	s15
	fmstat
	bpl	.L70
	.loc 2 126 0 is_stmt 0 discriminator 1
	movs	r3, #0
	b	.L57
.L70:
	.loc 2 126 0 discriminator 2
	ldr	r3, [r7, #56]
	cmp	r3, #0
	it	lt
	rsblt	r3, r3, #0
	fmsr	s15, r3	@ int
	fsitos	s14, s15
	flds	s15, [r7, #28]
	fdivs	s14, s14, s15
	flds	s15, [r7, #4]
	fsubs	s15, s14, s15
	fconsts	s14, #112
	fadds	s15, s15, s14
	flds	s14, .L71
	fmuls	s15, s15, s14
	ftosizs	s15, s15
	fmrs	r3, s15	@ int
.L57:
	.loc 2 126 0 discriminator 4
	ldr	r0, [r7, #52]
	ldr	r1, [r7, #16]
	mov	r2, r3
	bl	setPixelAA
	.loc 2 125 0 is_stmt 1 discriminator 4
	ldr	r2, [r7, #56]
	ldr	r3, [r7, #36]
	add	r3, r3, r2
	str	r3, [r7, #56]
.L54:
	.loc 2 125 0 is_stmt 0 discriminator 2
	ldr	r3, [r7, #56]
	fmsr	s15, r3	@ int
	fsitos	s14, s15
	flds	s13, [r7, #28]
	flds	s15, [r7, #4]
	fmuls	s15, s13, s15
	fcmpes	s14, s15
	fmstat
	bpl	.L58
	.loc 2 125 0 discriminator 3
	ldr	r2, [r7, #12]
	ldr	r3, [r7, #52]
	cmp	r2, r3
	bne	.L60
	.loc 2 125 0 discriminator 4
	ldr	r2, [r7, #44]
	ldr	r3, [r7, #36]
	cmp	r2, r3
	blt	.L60
.L58:
	.loc 2 127 0 is_stmt 1
	ldr	r2, [r7, #16]
	ldr	r3, [r7, #8]
	cmp	r2, r3
	bne	.L61
	b	.L33
.L61:
	.loc 2 128 0
	ldr	r2, [r7, #60]
	ldr	r3, [r7, #44]
	add	r3, r3, r2
	str	r3, [r7, #60]
	ldr	r2, [r7, #16]
	ldr	r3, [r7, #32]
	add	r3, r3, r2
	str	r3, [r7, #16]
.L53:
	.loc 2 130 0
	b	.L62
.L33:
	.loc 2 131 0
	adds	r7, r7, #64
	.cfi_def_cfa_offset 8
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	pop	{r7, pc}
.L72:
	.align	2
.L71:
	.word	1132396544
	.cfi_endproc
.LFE114:
	.size	plotLineWidth, .-plotLineWidth
	.align	2
	.global	plotCircleAA
	.thumb
	.thumb_func
	.type	plotCircleAA, %function
plotCircleAA:
.LFB115:
	.loc 2 135 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r7, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	sub	sp, sp, #40
	.cfi_def_cfa_offset 48
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	str	r0, [r7, #12]
	str	r1, [r7, #8]
	str	r2, [r7, #4]
	.loc 2 136 0
	ldr	r3, [r7, #4]
	negs	r3, r3
	str	r3, [r7, #36]
	movs	r3, #0
	str	r3, [r7, #32]
	.loc 2 137 0
	ldr	r2, [r7, #4]
	mov	r3, r2
	lsls	r3, r3, #31
	subs	r3, r3, r2
	lsls	r3, r3, #1
	adds	r3, r3, #2
	str	r3, [r7, #28]
	.loc 2 138 0
	ldr	r3, [r7, #28]
	rsb	r3, r3, #1
	str	r3, [r7, #4]
.L78:
.LBB2:
	.loc 2 140 0
	ldr	r2, [r7, #36]
	ldr	r3, [r7, #32]
	add	r2, r2, r3
	mov	r3, r2
	lsls	r3, r3, #31
	subs	r3, r3, r2
	lsls	r3, r3, #1
	mov	r2, r3
	ldr	r3, [r7, #28]
	add	r3, r3, r2
	subs	r3, r3, #2
	eor	r2, r3, r3, asr #31
	sub	r2, r2, r3, asr #31
	mov	r3, r2
	lsls	r3, r3, #8
	subs	r2, r3, r2
	ldr	r3, [r7, #4]
	sdiv	r3, r2, r3
	str	r3, [r7, #24]
	.loc 2 141 0
	ldr	r2, [r7, #12]
	ldr	r3, [r7, #36]
	subs	r1, r2, r3
	ldr	r2, [r7, #8]
	ldr	r3, [r7, #32]
	add	r3, r3, r2
	mov	r0, r1
	mov	r1, r3
	ldr	r2, [r7, #24]
	bl	setPixelAA
	.loc 2 142 0
	ldr	r2, [r7, #12]
	ldr	r3, [r7, #32]
	subs	r1, r2, r3
	ldr	r2, [r7, #8]
	ldr	r3, [r7, #36]
	subs	r3, r2, r3
	mov	r0, r1
	mov	r1, r3
	ldr	r2, [r7, #24]
	bl	setPixelAA
	.loc 2 143 0
	ldr	r2, [r7, #12]
	ldr	r3, [r7, #36]
	adds	r1, r2, r3
	ldr	r2, [r7, #8]
	ldr	r3, [r7, #32]
	subs	r3, r2, r3
	mov	r0, r1
	mov	r1, r3
	ldr	r2, [r7, #24]
	bl	setPixelAA
	.loc 2 144 0
	ldr	r2, [r7, #12]
	ldr	r3, [r7, #32]
	adds	r1, r2, r3
	ldr	r2, [r7, #8]
	ldr	r3, [r7, #36]
	add	r3, r3, r2
	mov	r0, r1
	mov	r1, r3
	ldr	r2, [r7, #24]
	bl	setPixelAA
	.loc 2 145 0
	ldr	r3, [r7, #28]
	str	r3, [r7, #20]
	ldr	r3, [r7, #36]
	str	r3, [r7, #16]
	.loc 2 146 0
	ldr	r2, [r7, #28]
	ldr	r3, [r7, #32]
	add	r3, r3, r2
	cmp	r3, #0
	ble	.L74
	.loc 2 147 0
	ldr	r2, [r7, #36]
	mov	r3, r2
	lsls	r3, r3, #31
	subs	r3, r3, r2
	lsls	r3, r3, #1
	mov	r2, r3
	ldr	r3, [r7, #28]
	add	r3, r3, r2
	subs	r2, r3, #1
	mov	r3, r2
	lsls	r3, r3, #8
	subs	r2, r3, r2
	ldr	r3, [r7, #4]
	sdiv	r3, r2, r3
	str	r3, [r7, #24]
	.loc 2 148 0
	ldr	r3, [r7, #24]
	cmp	r3, #255
	bgt	.L75
	.loc 2 149 0
	ldr	r2, [r7, #12]
	ldr	r3, [r7, #36]
	subs	r1, r2, r3
	ldr	r2, [r7, #8]
	ldr	r3, [r7, #32]
	add	r3, r3, r2
	adds	r3, r3, #1
	mov	r0, r1
	mov	r1, r3
	ldr	r2, [r7, #24]
	bl	setPixelAA
	.loc 2 150 0
	ldr	r2, [r7, #12]
	ldr	r3, [r7, #32]
	subs	r3, r2, r3
	subs	r1, r3, #1
	ldr	r2, [r7, #8]
	ldr	r3, [r7, #36]
	subs	r3, r2, r3
	mov	r0, r1
	mov	r1, r3
	ldr	r2, [r7, #24]
	bl	setPixelAA
	.loc 2 151 0
	ldr	r2, [r7, #12]
	ldr	r3, [r7, #36]
	adds	r1, r2, r3
	ldr	r2, [r7, #8]
	ldr	r3, [r7, #32]
	subs	r3, r2, r3
	subs	r3, r3, #1
	mov	r0, r1
	mov	r1, r3
	ldr	r2, [r7, #24]
	bl	setPixelAA
	.loc 2 152 0
	ldr	r2, [r7, #12]
	ldr	r3, [r7, #32]
	add	r3, r3, r2
	adds	r1, r3, #1
	ldr	r2, [r7, #8]
	ldr	r3, [r7, #36]
	add	r3, r3, r2
	mov	r0, r1
	mov	r1, r3
	ldr	r2, [r7, #24]
	bl	setPixelAA
.L75:
	.loc 2 154 0
	ldr	r3, [r7, #36]
	adds	r3, r3, #1
	str	r3, [r7, #36]
	ldr	r3, [r7, #36]
	lsls	r3, r3, #1
	adds	r3, r3, #1
	ldr	r2, [r7, #28]
	add	r3, r3, r2
	str	r3, [r7, #28]
.L74:
	.loc 2 156 0
	ldr	r2, [r7, #20]
	ldr	r3, [r7, #16]
	add	r3, r3, r2
	cmp	r3, #0
	bgt	.L76
	.loc 2 157 0
	ldr	r3, [r7, #32]
	lsls	r3, r3, #1
	adds	r2, r3, #3
	ldr	r3, [r7, #20]
	subs	r2, r2, r3
	mov	r3, r2
	lsls	r3, r3, #8
	subs	r2, r3, r2
	ldr	r3, [r7, #4]
	sdiv	r3, r2, r3
	str	r3, [r7, #24]
	.loc 2 158 0
	ldr	r3, [r7, #24]
	cmp	r3, #255
	bgt	.L77
	.loc 2 159 0
	ldr	r2, [r7, #12]
	ldr	r3, [r7, #16]
	subs	r3, r2, r3
	subs	r1, r3, #1
	ldr	r2, [r7, #8]
	ldr	r3, [r7, #32]
	add	r3, r3, r2
	mov	r0, r1
	mov	r1, r3
	ldr	r2, [r7, #24]
	bl	setPixelAA
	.loc 2 160 0
	ldr	r2, [r7, #12]
	ldr	r3, [r7, #32]
	subs	r1, r2, r3
	ldr	r2, [r7, #8]
	ldr	r3, [r7, #16]
	subs	r3, r2, r3
	subs	r3, r3, #1
	mov	r0, r1
	mov	r1, r3
	ldr	r2, [r7, #24]
	bl	setPixelAA
	.loc 2 161 0
	ldr	r2, [r7, #12]
	ldr	r3, [r7, #16]
	add	r3, r3, r2
	adds	r1, r3, #1
	ldr	r2, [r7, #8]
	ldr	r3, [r7, #32]
	subs	r3, r2, r3
	mov	r0, r1
	mov	r1, r3
	ldr	r2, [r7, #24]
	bl	setPixelAA
	.loc 2 162 0
	ldr	r2, [r7, #12]
	ldr	r3, [r7, #32]
	adds	r1, r2, r3
	ldr	r2, [r7, #8]
	ldr	r3, [r7, #16]
	add	r3, r3, r2
	adds	r3, r3, #1
	mov	r0, r1
	mov	r1, r3
	ldr	r2, [r7, #24]
	bl	setPixelAA
.L77:
	.loc 2 164 0
	ldr	r3, [r7, #32]
	adds	r3, r3, #1
	str	r3, [r7, #32]
	ldr	r3, [r7, #32]
	lsls	r3, r3, #1
	adds	r3, r3, #1
	ldr	r2, [r7, #28]
	add	r3, r3, r2
	str	r3, [r7, #28]
.L76:
.LBE2:
	.loc 2 166 0
	ldr	r3, [r7, #36]
	cmp	r3, #0
	blt	.L78
	.loc 2 167 0
	adds	r7, r7, #40
	.cfi_def_cfa_offset 8
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	pop	{r7, pc}
	.cfi_endproc
.LFE115:
	.size	plotCircleAA, .-plotCircleAA
	.global	gamma_correction_LUT
	.section	.rodata
	.align	2
	.type	gamma_correction_LUT, %object
	.size	gamma_correction_LUT, 256
gamma_correction_LUT:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	7
	.byte	7
	.byte	7
	.byte	8
	.byte	8
	.byte	8
	.byte	9
	.byte	9
	.byte	9
	.byte	10
	.byte	10
	.byte	10
	.byte	11
	.byte	11
	.byte	12
	.byte	12
	.byte	12
	.byte	13
	.byte	13
	.byte	14
	.byte	14
	.byte	15
	.byte	15
	.byte	16
	.byte	16
	.byte	17
	.byte	17
	.byte	18
	.byte	18
	.byte	19
	.byte	19
	.byte	20
	.byte	20
	.byte	21
	.byte	21
	.byte	22
	.byte	23
	.byte	23
	.byte	24
	.byte	24
	.byte	25
	.byte	26
	.byte	26
	.byte	27
	.byte	27
	.byte	28
	.byte	29
	.byte	29
	.byte	30
	.byte	31
	.byte	32
	.byte	32
	.byte	33
	.byte	34
	.byte	34
	.byte	35
	.byte	36
	.byte	37
	.byte	37
	.byte	38
	.byte	39
	.byte	40
	.byte	41
	.byte	41
	.byte	42
	.byte	43
	.byte	44
	.byte	45
	.byte	46
	.byte	46
	.byte	47
	.byte	48
	.byte	49
	.byte	50
	.byte	51
	.byte	52
	.byte	53
	.byte	54
	.byte	55
	.byte	55
	.byte	56
	.byte	57
	.byte	58
	.byte	59
	.byte	60
	.byte	61
	.byte	62
	.byte	63
	.byte	64
	.byte	65
	.byte	67
	.byte	68
	.byte	69
	.byte	70
	.byte	71
	.byte	72
	.byte	73
	.byte	74
	.byte	75
	.byte	76
	.byte	78
	.byte	79
	.byte	80
	.byte	81
	.byte	82
	.byte	83
	.byte	85
	.byte	86
	.byte	87
	.byte	88
	.byte	89
	.byte	91
	.byte	92
	.byte	93
	.byte	94
	.byte	96
	.byte	97
	.byte	98
	.byte	100
	.byte	101
	.byte	102
	.byte	104
	.byte	105
	.byte	106
	.byte	108
	.byte	109
	.byte	110
	.byte	112
	.byte	113
	.byte	115
	.byte	116
	.byte	117
	.byte	119
	.byte	120
	.byte	122
	.byte	123
	.byte	125
	.byte	126
	.byte	-128
	.byte	-127
	.byte	-125
	.byte	-124
	.byte	-122
	.byte	-121
	.byte	-119
	.byte	-117
	.byte	-116
	.byte	-114
	.byte	-113
	.byte	-111
	.byte	-109
	.byte	-108
	.byte	-106
	.byte	-105
	.byte	-103
	.byte	-101
	.byte	-100
	.byte	-98
	.byte	-96
	.byte	-95
	.byte	-93
	.byte	-91
	.byte	-89
	.byte	-88
	.byte	-86
	.byte	-84
	.byte	-82
	.byte	-81
	.byte	-79
	.byte	-77
	.byte	-75
	.byte	-73
	.byte	-71
	.byte	-70
	.byte	-68
	.byte	-66
	.byte	-64
	.byte	-62
	.byte	-60
	.byte	-58
	.byte	-56
	.byte	-55
	.byte	-53
	.byte	-51
	.byte	-49
	.byte	-47
	.byte	-45
	.byte	-43
	.byte	-41
	.byte	-39
	.byte	-37
	.byte	-35
	.byte	-33
	.byte	-31
	.byte	-29
	.byte	-27
	.byte	-24
	.byte	-22
	.byte	-20
	.byte	-18
	.byte	-16
	.byte	-14
	.byte	-12
	.byte	-10
	.byte	-8
	.byte	-5
	.byte	-3
	.global	gamma_correction_LUT2
	.align	2
	.type	gamma_correction_LUT2, %object
	.size	gamma_correction_LUT2, 256
gamma_correction_LUT2:
	.byte	0
	.byte	21
	.byte	28
	.byte	34
	.byte	39
	.byte	43
	.byte	46
	.byte	50
	.byte	53
	.byte	56
	.byte	58
	.byte	61
	.byte	63
	.byte	66
	.byte	68
	.byte	70
	.byte	72
	.byte	74
	.byte	76
	.byte	78
	.byte	80
	.byte	82
	.byte	84
	.byte	85
	.byte	87
	.byte	89
	.byte	90
	.byte	92
	.byte	93
	.byte	95
	.byte	96
	.byte	98
	.byte	99
	.byte	100
	.byte	102
	.byte	103
	.byte	105
	.byte	106
	.byte	107
	.byte	108
	.byte	110
	.byte	111
	.byte	112
	.byte	113
	.byte	115
	.byte	116
	.byte	117
	.byte	118
	.byte	119
	.byte	120
	.byte	121
	.byte	122
	.byte	124
	.byte	125
	.byte	126
	.byte	127
	.byte	-128
	.byte	-127
	.byte	-126
	.byte	-125
	.byte	-124
	.byte	-123
	.byte	-122
	.byte	-121
	.byte	-120
	.byte	-119
	.byte	-118
	.byte	-117
	.byte	-116
	.byte	-115
	.byte	-115
	.byte	-114
	.byte	-113
	.byte	-112
	.byte	-111
	.byte	-110
	.byte	-109
	.byte	-108
	.byte	-107
	.byte	-107
	.byte	-106
	.byte	-105
	.byte	-104
	.byte	-103
	.byte	-102
	.byte	-102
	.byte	-101
	.byte	-100
	.byte	-99
	.byte	-98
	.byte	-97
	.byte	-97
	.byte	-96
	.byte	-95
	.byte	-94
	.byte	-94
	.byte	-93
	.byte	-92
	.byte	-91
	.byte	-90
	.byte	-90
	.byte	-89
	.byte	-88
	.byte	-87
	.byte	-87
	.byte	-86
	.byte	-85
	.byte	-84
	.byte	-84
	.byte	-83
	.byte	-82
	.byte	-82
	.byte	-81
	.byte	-80
	.byte	-79
	.byte	-79
	.byte	-78
	.byte	-77
	.byte	-77
	.byte	-76
	.byte	-75
	.byte	-75
	.byte	-74
	.byte	-73
	.byte	-73
	.byte	-72
	.byte	-71
	.byte	-71
	.byte	-70
	.byte	-69
	.byte	-69
	.byte	-68
	.byte	-67
	.byte	-67
	.byte	-66
	.byte	-65
	.byte	-65
	.byte	-64
	.byte	-63
	.byte	-63
	.byte	-62
	.byte	-62
	.byte	-61
	.byte	-60
	.byte	-60
	.byte	-59
	.byte	-58
	.byte	-58
	.byte	-57
	.byte	-57
	.byte	-56
	.byte	-55
	.byte	-55
	.byte	-54
	.byte	-54
	.byte	-53
	.byte	-52
	.byte	-52
	.byte	-51
	.byte	-51
	.byte	-50
	.byte	-49
	.byte	-49
	.byte	-48
	.byte	-48
	.byte	-47
	.byte	-47
	.byte	-46
	.byte	-45
	.byte	-45
	.byte	-44
	.byte	-44
	.byte	-43
	.byte	-43
	.byte	-42
	.byte	-41
	.byte	-41
	.byte	-40
	.byte	-40
	.byte	-39
	.byte	-39
	.byte	-38
	.byte	-38
	.byte	-37
	.byte	-37
	.byte	-36
	.byte	-35
	.byte	-35
	.byte	-34
	.byte	-34
	.byte	-33
	.byte	-33
	.byte	-32
	.byte	-32
	.byte	-31
	.byte	-31
	.byte	-30
	.byte	-30
	.byte	-29
	.byte	-29
	.byte	-28
	.byte	-28
	.byte	-27
	.byte	-27
	.byte	-26
	.byte	-25
	.byte	-25
	.byte	-24
	.byte	-24
	.byte	-23
	.byte	-23
	.byte	-22
	.byte	-22
	.byte	-21
	.byte	-21
	.byte	-20
	.byte	-20
	.byte	-19
	.byte	-19
	.byte	-18
	.byte	-18
	.byte	-17
	.byte	-17
	.byte	-17
	.byte	-16
	.byte	-16
	.byte	-15
	.byte	-15
	.byte	-14
	.byte	-14
	.byte	-13
	.byte	-13
	.byte	-12
	.byte	-12
	.byte	-11
	.byte	-11
	.byte	-10
	.byte	-10
	.byte	-9
	.byte	-9
	.byte	-8
	.byte	-8
	.byte	-7
	.byte	-7
	.byte	-7
	.byte	-6
	.byte	-6
	.byte	-5
	.byte	-5
	.byte	-4
	.byte	-4
	.byte	-3
	.byte	-3
	.byte	-2
	.byte	-2
	.byte	-1
	.global	clock_addon
	.align	2
	.type	clock_addon, %object
	.size	clock_addon, 20
clock_addon:
	.byte	30
	.byte	120
	.byte	31
	.byte	120
	.byte	32
	.byte	120
	.byte	33
	.byte	120
	.byte	34
	.byte	120
	.byte	35
	.byte	120
	.byte	36
	.byte	120
	.byte	37
	.byte	120
	.byte	38
	.byte	120
	.byte	39
	.byte	120
	.global	DMA2_Stream7_compleated
	.data
	.type	DMA2_Stream7_compleated, %object
	.size	DMA2_Stream7_compleated, 1
DMA2_Stream7_compleated:
	.byte	1
	.text
	.align	2
	.global	memory_copy
	.thumb
	.thumb_func
	.type	memory_copy, %function
memory_copy:
.LFB116:
	.loc 2 207 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r7, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	sub	sp, sp, #16
	.cfi_def_cfa_offset 24
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	str	r0, [r7, #12]
	str	r1, [r7, #8]
	mov	r3, r2
	strh	r3, [r7, #6]	@ movhi
	.loc 2 210 0
	ldr	r2, .L80
	ldr	r3, .L80
	ldr	r3, [r3, #48]
	orr	r3, r3, #4194304
	str	r3, [r2, #48]
	.loc 2 217 0
	ldr	r3, .L80+4
	mov	r2, #201326592
	str	r2, [r3]
	.loc 2 222 0
	ldr	r2, .L80+4
	ldr	r3, .L80+4
	ldr	r3, [r3]
	orr	r3, r3, #25165824
	str	r3, [r2]
	.loc 2 227 0
	ldr	r2, .L80+4
	ldr	r3, .L80+4
	ldr	r3, [r3]
	orr	r3, r3, #6291456
	str	r3, [r2]
	.loc 2 231 0
	ldr	r2, .L80+4
	ldr	r3, .L80+4
	ldr	r3, [r3]
	orr	r3, r3, #196608
	str	r3, [r2]
	.loc 2 234 0
	ldr	r2, .L80+4
	ldr	r3, .L80+4
	ldr	r3, [r3]
	bic	r3, r3, #24576
	str	r3, [r2]
	.loc 2 237 0
	ldr	r2, .L80+4
	ldr	r3, .L80+4
	ldr	r3, [r3]
	bic	r3, r3, #6144
	str	r3, [r2]
	.loc 2 240 0
	ldr	r2, .L80+4
	ldr	r3, .L80+4
	ldr	r3, [r3]
	orr	r3, r3, #1536
	str	r3, [r2]
	.loc 2 243 0
	ldr	r2, .L80+4
	ldr	r3, .L80+4
	ldr	r3, [r3]
	orr	r3, r3, #128
	str	r3, [r2]
	.loc 2 246 0
	ldr	r2, .L80+4
	ldr	r3, .L80+4
	ldr	r3, [r3]
	orr	r3, r3, #16
	str	r3, [r2]
	.loc 2 249 0
	ldr	r2, .L80+8
	ldr	r3, .L80+8
	ldr	r3, [r3, #12]
	orr	r3, r3, #255852544
	str	r3, [r2, #12]
	.loc 2 254 0
	ldr	r2, .L80+4
	ldrh	r3, [r7, #6]
	str	r3, [r2, #4]
	.loc 2 257 0
	ldr	r2, .L80+4
	ldr	r3, [r7, #8]
	str	r3, [r2, #8]
	.loc 2 260 0
	ldr	r2, .L80+4
	ldr	r3, [r7, #12]
	str	r3, [r2, #12]
	.loc 2 264 0
	ldr	r3, .L80+4
	movs	r2, #7
	str	r2, [r3, #20]
	.loc 2 267 0
	movs	r0, #70
	bl	NVIC_EnableIRQ
	.loc 2 269 0
	ldr	r3, .L80+12
	movs	r2, #0
	strb	r2, [r3]
	.loc 2 272 0
	ldr	r2, .L80+4
	ldr	r3, .L80+4
	ldr	r3, [r3]
	orr	r3, r3, #1
	str	r3, [r2]
	.loc 2 273 0
	adds	r7, r7, #16
	.cfi_def_cfa_offset 8
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	pop	{r7, pc}
.L81:
	.align	2
.L80:
	.word	1073887232
	.word	1073898680
	.word	1073898496
	.word	DMA2_Stream7_compleated
	.cfi_endproc
.LFE116:
	.size	memory_copy, .-memory_copy
	.align	2
	.global	DMA2_Stream7_IRQHandler
	.thumb
	.thumb_func
	.type	DMA2_Stream7_IRQHandler, %function
DMA2_Stream7_IRQHandler:
.LFB117:
	.loc 2 278 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	push	{r7}
	.cfi_def_cfa_offset 4
	.cfi_offset 7, -4
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	.loc 2 280 0
	ldr	r2, .L83
	ldr	r3, .L83
	ldr	r3, [r3, #12]
	orr	r3, r3, #255852544
	str	r3, [r2, #12]
	.loc 2 283 0
	ldr	r3, .L83+4
	movs	r2, #1
	strb	r2, [r3]
	.loc 2 284 0
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	ldr	r7, [sp], #4
	.cfi_restore 7
	.cfi_def_cfa_offset 0
	bx	lr
.L84:
	.align	2
.L83:
	.word	1073898496
	.word	DMA2_Stream7_compleated
	.cfi_endproc
.LFE117:
	.size	DMA2_Stream7_IRQHandler, .-DMA2_Stream7_IRQHandler
	.comm	img_buff_n,4,4
	.comm	img_buff_tot,4,4
	.comm	img_buff_src,4,4
	.comm	img_buff_dst,4,4
	.global	img_buffer_copy_compleated
	.data
	.type	img_buffer_copy_compleated, %object
	.size	img_buffer_copy_compleated, 1
img_buffer_copy_compleated:
	.byte	1
	.text
	.align	2
	.global	init_img_buffer_copy
	.thumb
	.thumb_func
	.type	init_img_buffer_copy, %function
init_img_buffer_copy:
.LFB118:
	.loc 2 295 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	push	{r7}
	.cfi_def_cfa_offset 4
	.cfi_offset 7, -4
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	.loc 2 296 0
	ldr	r3, .L86
	movs	r2, #0
	str	r2, [r3]
	.loc 2 297 0
	ldr	r3, .L86+4
	mov	r2, #115200
	str	r2, [r3]
	.loc 2 299 0
	ldr	r2, .L86+8
	ldr	r3, .L86+12
	str	r2, [r3]
	.loc 2 300 0
	ldr	r2, .L86+16
	ldr	r3, .L86+20
	str	r2, [r3]
	.loc 2 302 0
	ldr	r3, .L86+24
	movs	r2, #0
	strb	r2, [r3]
	.loc 2 303 0
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	ldr	r7, [sp], #4
	.cfi_restore 7
	.cfi_def_cfa_offset 0
	bx	lr
.L87:
	.align	2
.L86:
	.word	img_buff_n
	.word	img_buff_tot
	.word	clock_img
	.word	img_buff_src
	.word	image_buffer
	.word	img_buff_dst
	.word	img_buffer_copy_compleated
	.cfi_endproc
.LFE118:
	.size	init_img_buffer_copy, .-init_img_buffer_copy
	.align	2
	.global	process_img_buffer_copy
	.thumb
	.thumb_func
	.type	process_img_buffer_copy, %function
process_img_buffer_copy:
.LFB119:
	.loc 2 306 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r7, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	sub	sp, sp, #8
	.cfi_def_cfa_offset 16
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	.loc 2 309 0
	ldr	r3, .L93
	ldr	r2, [r3]
	ldr	r3, .L93+4
	ldr	r3, [r3]
	cmp	r2, r3
	bge	.L89
	.loc 2 311 0
	ldr	r3, .L93+8
	ldrb	r3, [r3]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L88
	.loc 2 313 0
	ldr	r3, .L93+4
	ldr	r2, [r3]
	ldr	r3, .L93
	ldr	r3, [r3]
	subs	r3, r2, r3
	str	r3, [r7, #4]
	.loc 2 314 0
	ldr	r3, [r7, #4]
	cmp	r3, #64000
	ble	.L91
	.loc 2 315 0
	mov	r3, #64000
	str	r3, [r7, #4]
.L91:
	.loc 2 317 0
	ldr	r3, .L93+12
	ldr	r2, [r3]
	ldr	r3, .L93
	ldr	r3, [r3]
	add	r3, r3, r2
	mov	r1, r3
	.loc 2 318 0
	ldr	r3, .L93+16
	ldr	r2, [r3]
	ldr	r3, .L93
	ldr	r3, [r3]
	add	r3, r3, r2
	.loc 2 317 0
	mov	r2, r3
	ldr	r3, [r7, #4]
	uxth	r3, r3
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	bl	memory_copy
	.loc 2 321 0
	ldr	r3, .L93
	ldr	r2, [r3]
	ldr	r3, [r7, #4]
	add	r3, r3, r2
	ldr	r2, .L93
	str	r3, [r2]
	b	.L88
.L89:
	.loc 2 325 0
	ldr	r3, .L93+20
	movs	r2, #1
	strb	r2, [r3]
.L88:
	.loc 2 326 0
	adds	r7, r7, #8
	.cfi_def_cfa_offset 8
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	pop	{r7, pc}
.L94:
	.align	2
.L93:
	.word	img_buff_n
	.word	img_buff_tot
	.word	DMA2_Stream7_compleated
	.word	img_buff_dst
	.word	img_buff_src
	.word	img_buffer_copy_compleated
	.cfi_endproc
.LFE119:
	.size	process_img_buffer_copy, .-process_img_buffer_copy
	.align	2
	.global	init_clock
	.thumb
	.thumb_func
	.type	init_clock, %function
init_clock:
.LFB120:
	.loc 2 332 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r7, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	.loc 2 333 0
	ldr	r2, .L96
	ldr	r3, .L96
	ldr	r3, [r3, #64]
	orr	r3, r3, #1
	str	r3, [r2, #64]
	.loc 2 338 0
	mov	r3, #1073741824
	movs	r2, #0
	strh	r2, [r3]	@ movhi
	.loc 2 342 0
	mov	r2, #1073741824
	mov	r3, #1073741824
	ldrh	r3, [r3]	@ movhi
	uxth	r3, r3
	orr	r3, r3, #132
	uxth	r3, r3
	strh	r3, [r2]	@ movhi
	.loc 2 346 0
	mov	r2, #1073741824
	mov	r3, #1073741824
	ldrh	r3, [r3, #8]	@ movhi
	uxth	r3, r3
	bic	r3, r3, #7
	uxth	r3, r3
	strh	r3, [r2, #8]	@ movhi
	.loc 2 348 0
	mov	r3, #1073741824
	movw	r2, #41999
	strh	r2, [r3, #40]	@ movhi
	.loc 2 349 0
	mov	r3, #1073741824
	mov	r2, #2000
	str	r2, [r3, #44]
	.loc 2 352 0
	mov	r2, #1073741824
	mov	r3, #1073741824
	ldrh	r3, [r3, #20]	@ movhi
	uxth	r3, r3
	orr	r3, r3, #1
	uxth	r3, r3
	strh	r3, [r2, #20]	@ movhi
	.loc 2 355 0
	mov	r2, #1073741824
	mov	r3, #1073741824
	ldrh	r3, [r3, #12]	@ movhi
	uxth	r3, r3
	orr	r3, r3, #1
	uxth	r3, r3
	strh	r3, [r2, #12]	@ movhi
	.loc 2 358 0
	movs	r0, #28
	bl	NVIC_EnableIRQ
	.loc 2 361 0
	mov	r3, #1073741824
	movs	r2, #1
	strh	r2, [r3]	@ movhi
	.loc 2 362 0
	pop	{r7, pc}
.L97:
	.align	2
.L96:
	.word	1073887232
	.cfi_endproc
.LFE120:
	.size	init_clock, .-init_clock
	.global	show_next_frame
	.data
	.type	show_next_frame, %object
	.size	show_next_frame, 1
show_next_frame:
	.byte	1
	.text
	.align	2
	.global	TIM2_IRQHandler
	.thumb
	.thumb_func
	.type	TIM2_IRQHandler, %function
TIM2_IRQHandler:
.LFB121:
	.loc 2 369 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	push	{r7}
	.cfi_def_cfa_offset 4
	.cfi_offset 7, -4
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	.loc 2 370 0
	mov	r2, #1073741824
	mov	r3, #1073741824
	ldrh	r3, [r3, #16]	@ movhi
	uxth	r3, r3
	bic	r3, r3, #1
	uxth	r3, r3
	strh	r3, [r2, #16]	@ movhi
	.loc 2 371 0
	ldr	r3, .L99
	movs	r2, #1
	strb	r2, [r3]
	.loc 2 372 0
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	ldr	r7, [sp], #4
	.cfi_restore 7
	.cfi_def_cfa_offset 0
	bx	lr
.L100:
	.align	2
.L99:
	.word	show_next_frame
	.cfi_endproc
.LFE121:
	.size	TIM2_IRQHandler, .-TIM2_IRQHandler
	.align	2
	.global	draw_pixelmap
	.thumb
	.thumb_func
	.type	draw_pixelmap, %function
draw_pixelmap:
.LFB122:
	.loc 2 390 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	push	{r7}
	.cfi_def_cfa_offset 4
	.cfi_offset 7, -4
	sub	sp, sp, #28
	.cfi_def_cfa_offset 32
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	str	r0, [r7, #12]
	str	r1, [r7, #8]
	mov	r3, r2
	strh	r3, [r7, #6]	@ movhi
	.loc 2 393 0
	movs	r3, #0
	str	r3, [r7, #20]
	b	.L102
.L103:
	.loc 2 395 0 discriminator 3
	ldr	r3, [r7, #20]
	lsls	r3, r3, #1
	ldr	r2, [r7, #12]
	add	r3, r3, r2
	ldrb	r3, [r3]	@ zero_extendqisi2
	mov	r2, r3
	mov	r3, r2
	lsls	r3, r3, #4
	subs	r3, r3, r2
	lsls	r3, r3, #4
	mov	r1, r3
	ldr	r3, [r7, #20]
	lsls	r3, r3, #1
	ldr	r2, [r7, #12]
	add	r3, r3, r2
	ldrb	r3, [r3, #1]	@ zero_extendqisi2
	add	r3, r3, r1
	lsls	r3, r3, #1
	str	r3, [r7, #16]
	.loc 2 396 0 discriminator 3
	ldrh	r3, [r7, #6]	@ movhi
	uxtb	r1, r3
	ldr	r2, .L104
	ldr	r3, [r7, #16]
	add	r3, r3, r2
	mov	r2, r1
	strb	r2, [r3]
	.loc 2 397 0 discriminator 3
	ldr	r3, [r7, #16]
	adds	r3, r3, #1
	ldrh	r2, [r7, #6]
	lsrs	r2, r2, #8
	uxth	r2, r2
	uxtb	r1, r2
	ldr	r2, .L104
	strb	r1, [r2, r3]
	.loc 2 393 0 discriminator 3
	ldr	r3, [r7, #20]
	adds	r3, r3, #1
	str	r3, [r7, #20]
.L102:
	.loc 2 393 0 is_stmt 0 discriminator 1
	ldr	r2, [r7, #20]
	ldr	r3, [r7, #8]
	cmp	r2, r3
	bcc	.L103
	.loc 2 399 0 is_stmt 1
	adds	r7, r7, #28
	.cfi_def_cfa_offset 4
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	ldr	r7, [sp], #4
	.cfi_restore 7
	.cfi_def_cfa_offset 0
	bx	lr
.L105:
	.align	2
.L104:
	.word	image_buffer
	.cfi_endproc
.LFE122:
	.size	draw_pixelmap, .-draw_pixelmap
	.global	sin_value
	.section	.rodata
	.align	2
	.type	sin_value, %object
	.size	sin_value, 240
sin_value:
	.word	1065353216
	.word	1065261277
	.word	1064986634
	.word	1064532139
	.word	1063902826
	.word	1063105572
	.word	1062149103
	.word	1061043820
	.word	1059802139
	.word	1058437480
	.word	1056964608
	.word	1053835322
	.word	1050556383
	.word	1045751388
	.word	1037439955
	.word	0
	.word	-1110043693
	.word	-1101732260
	.word	-1096927265
	.word	-1093648326
	.word	-1090519040
	.word	-1089046168
	.word	-1087681509
	.word	-1086439828
	.word	-1085334545
	.word	-1084378076
	.word	-1083580822
	.word	-1082951509
	.word	-1082497014
	.word	-1082222371
	.word	-1082130432
	.word	-1082222371
	.word	-1082497014
	.word	-1082951509
	.word	-1083580822
	.word	-1084378076
	.word	-1085334545
	.word	-1086439828
	.word	-1087681509
	.word	-1089046168
	.word	-1090519040
	.word	-1093648326
	.word	-1096927265
	.word	-1101732260
	.word	-1110043693
	.word	0
	.word	1037439955
	.word	1045751388
	.word	1050556383
	.word	1053835322
	.word	1056964608
	.word	1058437480
	.word	1059802139
	.word	1061043820
	.word	1062149103
	.word	1063105572
	.word	1063902826
	.word	1064532139
	.word	1064986634
	.word	1065261277
	.global	cos_value
	.align	2
	.type	cos_value, %object
	.size	cos_value, 240
cos_value:
	.word	0
	.word	-1110043693
	.word	-1101732260
	.word	-1096927265
	.word	-1093648326
	.word	-1090519040
	.word	-1089046168
	.word	-1087681509
	.word	-1086439828
	.word	-1085334545
	.word	-1084378076
	.word	-1083580822
	.word	-1082951509
	.word	-1082497014
	.word	-1082222371
	.word	-1082130432
	.word	-1082222371
	.word	-1082497014
	.word	-1082951509
	.word	-1083580822
	.word	-1084378076
	.word	-1085334545
	.word	-1086439828
	.word	-1087681509
	.word	-1089046168
	.word	-1090519040
	.word	-1093648326
	.word	-1096927265
	.word	-1101732260
	.word	-1110043693
	.word	0
	.word	1037439955
	.word	1045751388
	.word	1050556383
	.word	1053835322
	.word	1056964608
	.word	1058437480
	.word	1059802139
	.word	1061043820
	.word	1062149103
	.word	1063105572
	.word	1063902826
	.word	1064532139
	.word	1064986634
	.word	1065261277
	.word	1065353216
	.word	1065261277
	.word	1064986634
	.word	1064532139
	.word	1063902826
	.word	1063105572
	.word	1062149103
	.word	1061043820
	.word	1059802139
	.word	1058437480
	.word	1056964608
	.word	1053835322
	.word	1050556383
	.word	1045751388
	.word	1037439955
	.text
	.align	2
	.global	abs_f
	.thumb
	.thumb_func
	.type	abs_f, %function
abs_f:
.LFB123:
	.loc 2 530 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	push	{r7}
	.cfi_def_cfa_offset 4
	.cfi_offset 7, -4
	sub	sp, sp, #12
	.cfi_def_cfa_offset 16
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	fsts	s0, [r7, #4]
	.loc 2 531 0
	flds	s15, [r7, #4]
	fcmpezs	s15
	fmstat
	ble	.L112
	.loc 2 531 0 is_stmt 0 discriminator 1
	flds	s15, [r7, #4]
	b	.L109
.L112:
	.loc 2 531 0 discriminator 2
	flds	s15, [r7, #4]
	fnegs	s15, s15
.L109:
	.loc 2 532 0 is_stmt 1 discriminator 4
	fcpys	s0, s15
	adds	r7, r7, #12
	.cfi_def_cfa_offset 4
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	ldr	r7, [sp], #4
	.cfi_restore 7
	.cfi_def_cfa_offset 0
	bx	lr
	.cfi_endproc
.LFE123:
	.size	abs_f, .-abs_f
	.align	2
	.global	draw_line
	.thumb
	.thumb_func
	.type	draw_line, %function
draw_line:
.LFB124:
	.loc 2 536 0
	.cfi_startproc
	@ args = 8, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r7, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	sub	sp, sp, #24
	.cfi_def_cfa_offset 32
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	str	r0, [r7, #12]
	str	r1, [r7, #8]
	str	r2, [r7, #4]
	str	r3, [r7]
	.loc 2 539 0
	ldrb	r3, [r7, #32]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L114
	ldrb	r3, [r3, r2]	@ zero_extendqisi2
	strb	r3, [r7, #32]
	.loc 2 540 0
	ldrb	r3, [r7, #33]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L114
	ldrb	r3, [r3, r2]	@ zero_extendqisi2
	strb	r3, [r7, #33]
	.loc 2 541 0
	ldrb	r3, [r7, #34]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L114
	ldrb	r3, [r3, r2]	@ zero_extendqisi2
	strb	r3, [r7, #34]
	.loc 2 543 0
	ldr	r3, .L114+4
	add	r2, r7, #32
	ldrh	r1, [r2]	@ unaligned
	ldrb	r2, [r2, #2]
	strh	r1, [r3]	@ unaligned
	strb	r2, [r3, #2]
	.loc 2 545 0
	ldr	r3, [r7, #12]
	fmsr	s15, r3	@ int
	fsitos	s14, s15
	ldr	r3, [r7, #4]
	fmsr	s15, r3	@ int
	fsitos	s13, s15
	ldr	r2, [r7]
	ldr	r3, .L114+8
	smull	r0, r1, r2, r3
	adds	r3, r2, r1
	asrs	r1, r3, #8
	asrs	r3, r2, #31
	subs	r3, r1, r3
	mov	r1, #360
	mul	r3, r1, r3
	subs	r3, r2, r3
	ldr	r2, .L114+12
	smull	r1, r2, r2, r3
	asrs	r3, r3, #31
	subs	r3, r2, r3
	ldr	r2, .L114+16
	lsls	r3, r3, #2
	add	r3, r3, r2
	flds	s15, [r3]
	fmuls	s15, s13, s15
	fsubs	s15, s14, s15
	ftosizs	s15, s15
	fmrs	r3, s15	@ int
	str	r3, [r7, #20]
	.loc 2 546 0
	ldr	r3, [r7, #8]
	fmsr	s15, r3	@ int
	fsitos	s14, s15
	ldr	r3, [r7, #4]
	fmsr	s15, r3	@ int
	fsitos	s13, s15
	ldr	r2, [r7]
	ldr	r3, .L114+8
	smull	r0, r1, r2, r3
	adds	r3, r2, r1
	asrs	r1, r3, #8
	asrs	r3, r2, #31
	subs	r3, r1, r3
	mov	r1, #360
	mul	r3, r1, r3
	subs	r3, r2, r3
	ldr	r2, .L114+12
	smull	r1, r2, r2, r3
	asrs	r3, r3, #31
	subs	r3, r2, r3
	ldr	r2, .L114+20
	lsls	r3, r3, #2
	add	r3, r3, r2
	flds	s15, [r3]
	fmuls	s15, s13, s15
	fsubs	s15, s14, s15
	ftosizs	s15, s15
	fmrs	r3, s15	@ int
	str	r3, [r7, #16]
	.loc 2 551 0
	ldr	r3, [r7, #36]
	fmsr	s15, r3	@ int
	fsitos	s15, s15
	ldr	r0, [r7, #12]
	ldr	r1, [r7, #8]
	ldr	r2, [r7, #20]
	ldr	r3, [r7, #16]
	fcpys	s0, s15
	bl	plotLineWidth
	.loc 2 559 0
	adds	r7, r7, #24
	.cfi_def_cfa_offset 8
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	pop	{r7, pc}
.L115:
	.align	2
.L114:
	.word	gamma_correction_LUT
	.word	line_color
	.word	-1240768329
	.word	715827883
	.word	sin_value
	.word	cos_value
	.cfi_endproc
.LFE124:
	.size	draw_line, .-draw_line
	.align	2
	.global	draw_circle
	.thumb
	.thumb_func
	.type	draw_circle, %function
draw_circle:
.LFB125:
	.loc 2 562 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r7, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	sub	sp, sp, #24
	.cfi_def_cfa_offset 32
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	str	r0, [r7, #12]
	str	r1, [r7, #8]
	str	r2, [r7, #4]
	str	r3, [r7]
	.loc 2 565 0
	ldrb	r3, [r7]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L119
	ldrb	r3, [r3, r2]	@ zero_extendqisi2
	strb	r3, [r7]
	.loc 2 566 0
	ldrb	r3, [r7, #1]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L119
	ldrb	r3, [r3, r2]	@ zero_extendqisi2
	strb	r3, [r7, #1]
	.loc 2 567 0
	ldrb	r3, [r7, #2]	@ zero_extendqisi2
	mov	r2, r3
	ldr	r3, .L119
	ldrb	r3, [r3, r2]	@ zero_extendqisi2
	strb	r3, [r7, #2]
	.loc 2 569 0
	ldr	r3, .L119+4
	mov	r2, r3
	mov	r3, r7
	ldrh	r1, [r3]	@ unaligned
	ldrb	r3, [r3, #2]
	strh	r1, [r2]	@ unaligned
	strb	r3, [r2, #2]
	.loc 2 571 0
	movs	r3, #1
	str	r3, [r7, #20]
	b	.L117
.L118:
	.loc 2 572 0 discriminator 3
	ldr	r0, [r7, #12]
	ldr	r1, [r7, #8]
	ldr	r2, [r7, #20]
	bl	plotCircleAA
	.loc 2 571 0 discriminator 3
	ldr	r3, [r7, #20]
	adds	r3, r3, #1
	str	r3, [r7, #20]
.L117:
	.loc 2 571 0 is_stmt 0 discriminator 1
	ldr	r2, [r7, #20]
	ldr	r3, [r7, #4]
	cmp	r2, r3
	ble	.L118
	.loc 2 573 0 is_stmt 1
	adds	r7, r7, #24
	.cfi_def_cfa_offset 8
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	pop	{r7, pc}
.L120:
	.align	2
.L119:
	.word	gamma_correction_LUT
	.word	line_color
	.cfi_endproc
.LFE125:
	.size	draw_circle, .-draw_circle
	.section	.rodata
	.align	2
.LC6:
	.ascii	"INIT COMPLEATED\000"
	.align	2
.LC0:
	.byte	-1
	.byte	-1
	.byte	-1
	.text
	.align	2
	.global	main
	.thumb
	.thumb_func
	.type	main, %function
main:
.LFB126:
	.loc 2 577 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r4, r7, lr}
	.cfi_def_cfa_offset 12
	.cfi_offset 4, -12
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	sub	sp, sp, #44
	.cfi_def_cfa_offset 56
	add	r7, sp, #8
	.cfi_def_cfa 7, 48
	.loc 2 578 0
	movs	r3, #3
	strb	r3, [r7, #31]
	.loc 2 580 0
	movs	r3, #255
	strb	r3, [r7, #12]
	movs	r3, #0
	strb	r3, [r7, #13]
	movs	r3, #0
	strb	r3, [r7, #14]
	.loc 2 581 0
	ldr	r2, .L135
	add	r3, r7, #8
	ldrh	r1, [r2]	@ unaligned
	ldrb	r2, [r2, #2]
	strh	r1, [r3]	@ unaligned
	strb	r2, [r3, #2]
	.loc 2 582 0
	ldr	r2, .L135
	adds	r3, r7, #4
	ldrh	r1, [r2]	@ unaligned
	ldrb	r2, [r2, #2]
	strh	r1, [r3]	@ unaligned
	strb	r2, [r3, #2]
	.loc 2 584 0
	movs	r3, #0
	str	r3, [r7, #24]
	.loc 2 585 0
	movs	r3, #40
	str	r3, [r7, #20]
	.loc 2 586 0
	movs	r3, #7
	str	r3, [r7, #16]
	.loc 2 588 0
	bl	init_UART4
	.loc 2 589 0
	bl	ILI9341_init
	.loc 2 590 0
	bl	init_clock
	.loc 2 592 0
	ldr	r0, .L135+4
	bl	print_terminal
.L133:
	.loc 2 600 0
	ldrb	r3, [r7, #31]	@ zero_extendqisi2
	cmp	r3, #3
	bhi	.L133
	adr	r2, .L124
	ldr	pc, [r2, r3, lsl #2]
	.p2align 2
.L124:
	.word	.L123+1
	.word	.L125+1
	.word	.L126+1
	.word	.L127+1
	.p2align 1
.L123:
	.loc 2 603 0
	bl	init_img_buffer_copy
	.loc 2 604 0
	movs	r3, #1
	strb	r3, [r7, #31]
	.loc 2 605 0
	b	.L122
.L125:
	.loc 2 608 0
	ldr	r3, .L135+8
	ldrb	r3, [r3]	@ zero_extendqisi2
	cmp	r3, #1
	beq	.L128
	.loc 2 609 0
	bl	process_img_buffer_copy
	.loc 2 612 0
	b	.L122
.L128:
	.loc 2 611 0
	movs	r3, #2
	strb	r3, [r7, #31]
	.loc 2 612 0
	b	.L122
.L126:
	.loc 2 615 0
	ldr	r2, [r7, #20]
	mov	r3, r2
	lsls	r3, r3, #1
	add	r3, r3, r2
	lsls	r3, r3, #1
	mov	r4, r3
	mov	r3, sp
	add	r2, r7, #8
	ldr	r2, [r2]
	mov	r1, r2	@ movhi
	strh	r1, [r3]	@ movhi
	adds	r3, r3, #2
	lsrs	r2, r2, #16
	strb	r2, [r3]
	movs	r3, #2
	str	r3, [sp, #4]
	movs	r0, #120
	movs	r1, #120
	movs	r2, #100
	mov	r3, r4
	bl	draw_line
	.loc 2 616 0
	ldr	r2, [r7, #16]
	mov	r3, r2
	lsls	r3, r3, #4
	subs	r3, r3, r2
	lsls	r3, r3, #1
	mov	r4, r3
	mov	r3, sp
	adds	r2, r7, #4
	ldr	r2, [r2]
	mov	r1, r2	@ movhi
	strh	r1, [r3]	@ movhi
	adds	r3, r3, #2
	lsrs	r2, r2, #16
	strb	r2, [r3]
	movs	r3, #3
	str	r3, [sp, #4]
	movs	r0, #120
	movs	r1, #120
	movs	r2, #50
	mov	r3, r4
	bl	draw_line
	.loc 2 617 0
	ldr	r2, [r7, #24]
	mov	r3, r2
	lsls	r3, r3, #1
	add	r3, r3, r2
	lsls	r3, r3, #1
	mov	r4, r3
	mov	r3, sp
	add	r2, r7, #12
	ldr	r2, [r2]
	mov	r1, r2	@ movhi
	strh	r1, [r3]	@ movhi
	adds	r3, r3, #2
	lsrs	r2, r2, #16
	strb	r2, [r3]
	movs	r3, #2
	str	r3, [sp, #4]
	movs	r0, #120
	movs	r1, #120
	movs	r2, #100
	mov	r3, r4
	bl	draw_line
	.loc 2 618 0
	movs	r3, #3
	strb	r3, [r7, #31]
	.loc 2 619 0
	b	.L122
.L127:
	.loc 2 622 0
	ldr	r3, .L135+12
	ldrb	r3, [r3]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L130
	.loc 2 624 0
	movs	r3, #240
	str	r3, [sp]
	ldr	r0, .L135+16
	movs	r1, #40
	movs	r2, #0
	mov	r3, #280
	bl	ILI9341_image
	.loc 2 625 0
	movs	r3, #0
	strb	r3, [r7, #31]
	.loc 2 626 0
	ldr	r3, .L135+12
	movs	r2, #0
	strb	r2, [r3]
	.loc 2 628 0
	ldr	r3, [r7, #24]
	adds	r3, r3, #1
	str	r3, [r7, #24]
	.loc 2 630 0
	ldr	r3, [r7, #24]
	cmp	r3, #60
	bne	.L131
	.loc 2 632 0
	movs	r3, #0
	str	r3, [r7, #24]
	.loc 2 633 0
	ldr	r3, [r7, #20]
	adds	r3, r3, #1
	str	r3, [r7, #20]
.L131:
	.loc 2 636 0
	ldr	r3, [r7, #20]
	cmp	r3, #60
	bne	.L132
	.loc 2 638 0
	movs	r3, #0
	str	r3, [r7, #20]
	.loc 2 639 0
	ldr	r3, [r7, #16]
	adds	r3, r3, #1
	str	r3, [r7, #16]
.L132:
	.loc 2 642 0
	ldr	r3, [r7, #16]
	cmp	r3, #12
	bne	.L130
	.loc 2 643 0
	movs	r3, #0
	str	r3, [r7, #16]
	.loc 2 645 0
	b	.L134
.L130:
.L134:
	nop
.L122:
	.loc 2 647 0 discriminator 1
	b	.L133
.L136:
	.align	2
.L135:
	.word	.LC0
	.word	.LC6
	.word	img_buffer_copy_compleated
	.word	show_next_frame
	.word	image_buffer
	.cfi_endproc
.LFE126:
	.size	main, .-main
	.bss
	.align	2
count.5473:
	.space	4
	.text
.Letext0:
	.file 3 "stm32f4xx.h"
	.file 4 "/home/sharpovici-ostrakovici/stm_dev/STM32F407/gcc-arm-none-eabi/arm-none-eabi/include/machine/_default_types.h"
	.file 5 "/home/sharpovici-ostrakovici/stm_dev/STM32F407/gcc-arm-none-eabi/arm-none-eabi/include/stdint.h"
	.file 6 "<built-in>"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x10ff
	.2byte	0x4
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF232
	.byte	0x1
	.4byte	.LASF233
	.4byte	.LASF234
	.4byte	.Ltext0
	.4byte	.Letext0-.Ltext0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.4byte	.LASF176
	.byte	0x1
	.byte	0x3
	.byte	0x91
	.4byte	0x260
	.uleb128 0x3
	.4byte	.LASF0
	.sleb128 -14
	.uleb128 0x3
	.4byte	.LASF1
	.sleb128 -12
	.uleb128 0x3
	.4byte	.LASF2
	.sleb128 -11
	.uleb128 0x3
	.4byte	.LASF3
	.sleb128 -10
	.uleb128 0x3
	.4byte	.LASF4
	.sleb128 -5
	.uleb128 0x3
	.4byte	.LASF5
	.sleb128 -4
	.uleb128 0x3
	.4byte	.LASF6
	.sleb128 -2
	.uleb128 0x3
	.4byte	.LASF7
	.sleb128 -1
	.uleb128 0x3
	.4byte	.LASF8
	.sleb128 0
	.uleb128 0x3
	.4byte	.LASF9
	.sleb128 1
	.uleb128 0x3
	.4byte	.LASF10
	.sleb128 2
	.uleb128 0x3
	.4byte	.LASF11
	.sleb128 3
	.uleb128 0x3
	.4byte	.LASF12
	.sleb128 4
	.uleb128 0x3
	.4byte	.LASF13
	.sleb128 5
	.uleb128 0x3
	.4byte	.LASF14
	.sleb128 6
	.uleb128 0x3
	.4byte	.LASF15
	.sleb128 7
	.uleb128 0x3
	.4byte	.LASF16
	.sleb128 8
	.uleb128 0x3
	.4byte	.LASF17
	.sleb128 9
	.uleb128 0x3
	.4byte	.LASF18
	.sleb128 10
	.uleb128 0x3
	.4byte	.LASF19
	.sleb128 11
	.uleb128 0x3
	.4byte	.LASF20
	.sleb128 12
	.uleb128 0x3
	.4byte	.LASF21
	.sleb128 13
	.uleb128 0x3
	.4byte	.LASF22
	.sleb128 14
	.uleb128 0x3
	.4byte	.LASF23
	.sleb128 15
	.uleb128 0x3
	.4byte	.LASF24
	.sleb128 16
	.uleb128 0x3
	.4byte	.LASF25
	.sleb128 17
	.uleb128 0x3
	.4byte	.LASF26
	.sleb128 18
	.uleb128 0x3
	.4byte	.LASF27
	.sleb128 19
	.uleb128 0x3
	.4byte	.LASF28
	.sleb128 20
	.uleb128 0x3
	.4byte	.LASF29
	.sleb128 21
	.uleb128 0x3
	.4byte	.LASF30
	.sleb128 22
	.uleb128 0x3
	.4byte	.LASF31
	.sleb128 23
	.uleb128 0x3
	.4byte	.LASF32
	.sleb128 24
	.uleb128 0x3
	.4byte	.LASF33
	.sleb128 25
	.uleb128 0x3
	.4byte	.LASF34
	.sleb128 26
	.uleb128 0x3
	.4byte	.LASF35
	.sleb128 27
	.uleb128 0x3
	.4byte	.LASF36
	.sleb128 28
	.uleb128 0x3
	.4byte	.LASF37
	.sleb128 29
	.uleb128 0x3
	.4byte	.LASF38
	.sleb128 30
	.uleb128 0x3
	.4byte	.LASF39
	.sleb128 31
	.uleb128 0x3
	.4byte	.LASF40
	.sleb128 32
	.uleb128 0x3
	.4byte	.LASF41
	.sleb128 33
	.uleb128 0x3
	.4byte	.LASF42
	.sleb128 34
	.uleb128 0x3
	.4byte	.LASF43
	.sleb128 35
	.uleb128 0x3
	.4byte	.LASF44
	.sleb128 36
	.uleb128 0x3
	.4byte	.LASF45
	.sleb128 37
	.uleb128 0x3
	.4byte	.LASF46
	.sleb128 38
	.uleb128 0x3
	.4byte	.LASF47
	.sleb128 39
	.uleb128 0x3
	.4byte	.LASF48
	.sleb128 40
	.uleb128 0x3
	.4byte	.LASF49
	.sleb128 41
	.uleb128 0x3
	.4byte	.LASF50
	.sleb128 42
	.uleb128 0x3
	.4byte	.LASF51
	.sleb128 43
	.uleb128 0x3
	.4byte	.LASF52
	.sleb128 44
	.uleb128 0x3
	.4byte	.LASF53
	.sleb128 45
	.uleb128 0x3
	.4byte	.LASF54
	.sleb128 46
	.uleb128 0x3
	.4byte	.LASF55
	.sleb128 47
	.uleb128 0x3
	.4byte	.LASF56
	.sleb128 48
	.uleb128 0x3
	.4byte	.LASF57
	.sleb128 49
	.uleb128 0x3
	.4byte	.LASF58
	.sleb128 50
	.uleb128 0x3
	.4byte	.LASF59
	.sleb128 51
	.uleb128 0x3
	.4byte	.LASF60
	.sleb128 52
	.uleb128 0x3
	.4byte	.LASF61
	.sleb128 53
	.uleb128 0x3
	.4byte	.LASF62
	.sleb128 54
	.uleb128 0x3
	.4byte	.LASF63
	.sleb128 55
	.uleb128 0x3
	.4byte	.LASF64
	.sleb128 56
	.uleb128 0x3
	.4byte	.LASF65
	.sleb128 57
	.uleb128 0x3
	.4byte	.LASF66
	.sleb128 58
	.uleb128 0x3
	.4byte	.LASF67
	.sleb128 59
	.uleb128 0x3
	.4byte	.LASF68
	.sleb128 60
	.uleb128 0x3
	.4byte	.LASF69
	.sleb128 61
	.uleb128 0x3
	.4byte	.LASF70
	.sleb128 62
	.uleb128 0x3
	.4byte	.LASF71
	.sleb128 63
	.uleb128 0x3
	.4byte	.LASF72
	.sleb128 64
	.uleb128 0x3
	.4byte	.LASF73
	.sleb128 65
	.uleb128 0x3
	.4byte	.LASF74
	.sleb128 66
	.uleb128 0x3
	.4byte	.LASF75
	.sleb128 67
	.uleb128 0x3
	.4byte	.LASF76
	.sleb128 68
	.uleb128 0x3
	.4byte	.LASF77
	.sleb128 69
	.uleb128 0x3
	.4byte	.LASF78
	.sleb128 70
	.uleb128 0x3
	.4byte	.LASF79
	.sleb128 71
	.uleb128 0x3
	.4byte	.LASF80
	.sleb128 72
	.uleb128 0x3
	.4byte	.LASF81
	.sleb128 73
	.uleb128 0x3
	.4byte	.LASF82
	.sleb128 74
	.uleb128 0x3
	.4byte	.LASF83
	.sleb128 75
	.uleb128 0x3
	.4byte	.LASF84
	.sleb128 76
	.uleb128 0x3
	.4byte	.LASF85
	.sleb128 77
	.uleb128 0x3
	.4byte	.LASF86
	.sleb128 78
	.uleb128 0x3
	.4byte	.LASF87
	.sleb128 79
	.uleb128 0x3
	.4byte	.LASF88
	.sleb128 80
	.uleb128 0x3
	.4byte	.LASF89
	.sleb128 81
	.byte	0
	.uleb128 0x4
	.4byte	.LASF90
	.byte	0x3
	.byte	0xef
	.4byte	0x25
	.uleb128 0x5
	.byte	0x1
	.byte	0x6
	.4byte	.LASF92
	.uleb128 0x4
	.4byte	.LASF91
	.byte	0x4
	.byte	0x1d
	.4byte	0x27d
	.uleb128 0x5
	.byte	0x1
	.byte	0x8
	.4byte	.LASF93
	.uleb128 0x5
	.byte	0x2
	.byte	0x5
	.4byte	.LASF94
	.uleb128 0x4
	.4byte	.LASF95
	.byte	0x4
	.byte	0x2b
	.4byte	0x296
	.uleb128 0x5
	.byte	0x2
	.byte	0x7
	.4byte	.LASF96
	.uleb128 0x4
	.4byte	.LASF97
	.byte	0x4
	.byte	0x3f
	.4byte	0x2a8
	.uleb128 0x5
	.byte	0x4
	.byte	0x5
	.4byte	.LASF98
	.uleb128 0x4
	.4byte	.LASF99
	.byte	0x4
	.byte	0x41
	.4byte	0x2ba
	.uleb128 0x5
	.byte	0x4
	.byte	0x7
	.4byte	.LASF100
	.uleb128 0x5
	.byte	0x8
	.byte	0x5
	.4byte	.LASF101
	.uleb128 0x5
	.byte	0x8
	.byte	0x7
	.4byte	.LASF102
	.uleb128 0x6
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x5
	.byte	0x4
	.byte	0x7
	.4byte	.LASF103
	.uleb128 0x4
	.4byte	.LASF104
	.byte	0x5
	.byte	0x15
	.4byte	0x272
	.uleb128 0x4
	.4byte	.LASF105
	.byte	0x5
	.byte	0x21
	.4byte	0x28b
	.uleb128 0x4
	.4byte	.LASF106
	.byte	0x5
	.byte	0x2c
	.4byte	0x29d
	.uleb128 0x4
	.4byte	.LASF107
	.byte	0x5
	.byte	0x2d
	.4byte	0x2af
	.uleb128 0x7
	.2byte	0xe04
	.byte	0x1
	.2byte	0x130
	.4byte	0x3c5
	.uleb128 0x8
	.4byte	.LASF108
	.byte	0x1
	.2byte	0x132
	.4byte	0x3dc
	.byte	0
	.uleb128 0x8
	.4byte	.LASF109
	.byte	0x1
	.2byte	0x133
	.4byte	0x3e1
	.byte	0x20
	.uleb128 0x8
	.4byte	.LASF110
	.byte	0x1
	.2byte	0x134
	.4byte	0x3f1
	.byte	0x80
	.uleb128 0x8
	.4byte	.LASF111
	.byte	0x1
	.2byte	0x135
	.4byte	0x3e1
	.byte	0xa0
	.uleb128 0x9
	.4byte	.LASF112
	.byte	0x1
	.2byte	0x136
	.4byte	0x3f6
	.2byte	0x100
	.uleb128 0x9
	.4byte	.LASF113
	.byte	0x1
	.2byte	0x137
	.4byte	0x3e1
	.2byte	0x120
	.uleb128 0x9
	.4byte	.LASF114
	.byte	0x1
	.2byte	0x138
	.4byte	0x3fb
	.2byte	0x180
	.uleb128 0x9
	.4byte	.LASF115
	.byte	0x1
	.2byte	0x139
	.4byte	0x3e1
	.2byte	0x1a0
	.uleb128 0x9
	.4byte	.LASF116
	.byte	0x1
	.2byte	0x13a
	.4byte	0x400
	.2byte	0x200
	.uleb128 0x9
	.4byte	.LASF117
	.byte	0x1
	.2byte	0x13b
	.4byte	0x405
	.2byte	0x220
	.uleb128 0xa
	.ascii	"IP\000"
	.byte	0x1
	.2byte	0x13c
	.4byte	0x425
	.2byte	0x300
	.uleb128 0x9
	.4byte	.LASF118
	.byte	0x1
	.2byte	0x13d
	.4byte	0x42a
	.2byte	0x3f0
	.uleb128 0x9
	.4byte	.LASF119
	.byte	0x1
	.2byte	0x13e
	.4byte	0x43b
	.2byte	0xe00
	.byte	0
	.uleb128 0xb
	.4byte	0x2fe
	.4byte	0x3d5
	.uleb128 0xc
	.4byte	0x3d5
	.byte	0x7
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.byte	0x7
	.4byte	.LASF120
	.uleb128 0xd
	.4byte	0x3c5
	.uleb128 0xb
	.4byte	0x2fe
	.4byte	0x3f1
	.uleb128 0xc
	.4byte	0x3d5
	.byte	0x17
	.byte	0
	.uleb128 0xd
	.4byte	0x3c5
	.uleb128 0xd
	.4byte	0x3c5
	.uleb128 0xd
	.4byte	0x3c5
	.uleb128 0xd
	.4byte	0x3c5
	.uleb128 0xb
	.4byte	0x2fe
	.4byte	0x415
	.uleb128 0xc
	.4byte	0x3d5
	.byte	0x37
	.byte	0
	.uleb128 0xb
	.4byte	0x2dd
	.4byte	0x425
	.uleb128 0xc
	.4byte	0x3d5
	.byte	0xef
	.byte	0
	.uleb128 0xd
	.4byte	0x415
	.uleb128 0xb
	.4byte	0x2fe
	.4byte	0x43b
	.uleb128 0xe
	.4byte	0x3d5
	.2byte	0x283
	.byte	0
	.uleb128 0xd
	.4byte	0x2fe
	.uleb128 0xf
	.4byte	.LASF121
	.byte	0x1
	.2byte	0x13f
	.4byte	0x309
	.uleb128 0xb
	.4byte	0x2fe
	.4byte	0x45c
	.uleb128 0xc
	.4byte	0x3d5
	.byte	0x1
	.byte	0
	.uleb128 0xd
	.4byte	0x2e8
	.uleb128 0xd
	.4byte	0x2f3
	.uleb128 0x10
	.byte	0x18
	.byte	0x3
	.2byte	0x1d7
	.4byte	0x4bd
	.uleb128 0x11
	.ascii	"CR\000"
	.byte	0x3
	.2byte	0x1d9
	.4byte	0x43b
	.byte	0
	.uleb128 0x8
	.4byte	.LASF122
	.byte	0x3
	.2byte	0x1da
	.4byte	0x43b
	.byte	0x4
	.uleb128 0x11
	.ascii	"PAR\000"
	.byte	0x3
	.2byte	0x1db
	.4byte	0x43b
	.byte	0x8
	.uleb128 0x8
	.4byte	.LASF123
	.byte	0x3
	.2byte	0x1dc
	.4byte	0x43b
	.byte	0xc
	.uleb128 0x8
	.4byte	.LASF124
	.byte	0x3
	.2byte	0x1dd
	.4byte	0x43b
	.byte	0x10
	.uleb128 0x11
	.ascii	"FCR\000"
	.byte	0x3
	.2byte	0x1de
	.4byte	0x43b
	.byte	0x14
	.byte	0
	.uleb128 0xf
	.4byte	.LASF125
	.byte	0x3
	.2byte	0x1df
	.4byte	0x466
	.uleb128 0x10
	.byte	0x10
	.byte	0x3
	.2byte	0x1e1
	.4byte	0x507
	.uleb128 0x8
	.4byte	.LASF126
	.byte	0x3
	.2byte	0x1e3
	.4byte	0x43b
	.byte	0
	.uleb128 0x8
	.4byte	.LASF127
	.byte	0x3
	.2byte	0x1e4
	.4byte	0x43b
	.byte	0x4
	.uleb128 0x8
	.4byte	.LASF128
	.byte	0x3
	.2byte	0x1e5
	.4byte	0x43b
	.byte	0x8
	.uleb128 0x8
	.4byte	.LASF129
	.byte	0x3
	.2byte	0x1e6
	.4byte	0x43b
	.byte	0xc
	.byte	0
	.uleb128 0xf
	.4byte	.LASF130
	.byte	0x3
	.2byte	0x1e7
	.4byte	0x4c9
	.uleb128 0x10
	.byte	0x88
	.byte	0x3
	.2byte	0x2dd
	.4byte	0x6a2
	.uleb128 0x11
	.ascii	"CR\000"
	.byte	0x3
	.2byte	0x2df
	.4byte	0x43b
	.byte	0
	.uleb128 0x8
	.4byte	.LASF131
	.byte	0x3
	.2byte	0x2e0
	.4byte	0x43b
	.byte	0x4
	.uleb128 0x8
	.4byte	.LASF132
	.byte	0x3
	.2byte	0x2e1
	.4byte	0x43b
	.byte	0x8
	.uleb128 0x11
	.ascii	"CIR\000"
	.byte	0x3
	.2byte	0x2e2
	.4byte	0x43b
	.byte	0xc
	.uleb128 0x8
	.4byte	.LASF133
	.byte	0x3
	.2byte	0x2e3
	.4byte	0x43b
	.byte	0x10
	.uleb128 0x8
	.4byte	.LASF134
	.byte	0x3
	.2byte	0x2e4
	.4byte	0x43b
	.byte	0x14
	.uleb128 0x8
	.4byte	.LASF135
	.byte	0x3
	.2byte	0x2e5
	.4byte	0x43b
	.byte	0x18
	.uleb128 0x8
	.4byte	.LASF109
	.byte	0x3
	.2byte	0x2e6
	.4byte	0x2fe
	.byte	0x1c
	.uleb128 0x8
	.4byte	.LASF136
	.byte	0x3
	.2byte	0x2e7
	.4byte	0x43b
	.byte	0x20
	.uleb128 0x8
	.4byte	.LASF137
	.byte	0x3
	.2byte	0x2e8
	.4byte	0x43b
	.byte	0x24
	.uleb128 0x8
	.4byte	.LASF138
	.byte	0x3
	.2byte	0x2e9
	.4byte	0x44c
	.byte	0x28
	.uleb128 0x8
	.4byte	.LASF139
	.byte	0x3
	.2byte	0x2ea
	.4byte	0x43b
	.byte	0x30
	.uleb128 0x8
	.4byte	.LASF140
	.byte	0x3
	.2byte	0x2eb
	.4byte	0x43b
	.byte	0x34
	.uleb128 0x8
	.4byte	.LASF141
	.byte	0x3
	.2byte	0x2ec
	.4byte	0x43b
	.byte	0x38
	.uleb128 0x8
	.4byte	.LASF113
	.byte	0x3
	.2byte	0x2ed
	.4byte	0x2fe
	.byte	0x3c
	.uleb128 0x8
	.4byte	.LASF142
	.byte	0x3
	.2byte	0x2ee
	.4byte	0x43b
	.byte	0x40
	.uleb128 0x8
	.4byte	.LASF143
	.byte	0x3
	.2byte	0x2ef
	.4byte	0x43b
	.byte	0x44
	.uleb128 0x8
	.4byte	.LASF115
	.byte	0x3
	.2byte	0x2f0
	.4byte	0x44c
	.byte	0x48
	.uleb128 0x8
	.4byte	.LASF144
	.byte	0x3
	.2byte	0x2f1
	.4byte	0x43b
	.byte	0x50
	.uleb128 0x8
	.4byte	.LASF145
	.byte	0x3
	.2byte	0x2f2
	.4byte	0x43b
	.byte	0x54
	.uleb128 0x8
	.4byte	.LASF146
	.byte	0x3
	.2byte	0x2f3
	.4byte	0x43b
	.byte	0x58
	.uleb128 0x8
	.4byte	.LASF117
	.byte	0x3
	.2byte	0x2f4
	.4byte	0x2fe
	.byte	0x5c
	.uleb128 0x8
	.4byte	.LASF147
	.byte	0x3
	.2byte	0x2f5
	.4byte	0x43b
	.byte	0x60
	.uleb128 0x8
	.4byte	.LASF148
	.byte	0x3
	.2byte	0x2f6
	.4byte	0x43b
	.byte	0x64
	.uleb128 0x8
	.4byte	.LASF118
	.byte	0x3
	.2byte	0x2f7
	.4byte	0x44c
	.byte	0x68
	.uleb128 0x8
	.4byte	.LASF149
	.byte	0x3
	.2byte	0x2f8
	.4byte	0x43b
	.byte	0x70
	.uleb128 0x11
	.ascii	"CSR\000"
	.byte	0x3
	.2byte	0x2f9
	.4byte	0x43b
	.byte	0x74
	.uleb128 0x8
	.4byte	.LASF150
	.byte	0x3
	.2byte	0x2fa
	.4byte	0x44c
	.byte	0x78
	.uleb128 0x8
	.4byte	.LASF151
	.byte	0x3
	.2byte	0x2fb
	.4byte	0x43b
	.byte	0x80
	.uleb128 0x8
	.4byte	.LASF152
	.byte	0x3
	.2byte	0x2fc
	.4byte	0x43b
	.byte	0x84
	.byte	0
	.uleb128 0xf
	.4byte	.LASF153
	.byte	0x3
	.2byte	0x2fd
	.4byte	0x513
	.uleb128 0x10
	.byte	0x54
	.byte	0x3
	.2byte	0x369
	.4byte	0x88a
	.uleb128 0x11
	.ascii	"CR1\000"
	.byte	0x3
	.2byte	0x36b
	.4byte	0x45c
	.byte	0
	.uleb128 0x8
	.4byte	.LASF109
	.byte	0x3
	.2byte	0x36c
	.4byte	0x2e8
	.byte	0x2
	.uleb128 0x11
	.ascii	"CR2\000"
	.byte	0x3
	.2byte	0x36d
	.4byte	0x45c
	.byte	0x4
	.uleb128 0x8
	.4byte	.LASF138
	.byte	0x3
	.2byte	0x36e
	.4byte	0x2e8
	.byte	0x6
	.uleb128 0x8
	.4byte	.LASF154
	.byte	0x3
	.2byte	0x36f
	.4byte	0x45c
	.byte	0x8
	.uleb128 0x8
	.4byte	.LASF113
	.byte	0x3
	.2byte	0x370
	.4byte	0x2e8
	.byte	0xa
	.uleb128 0x8
	.4byte	.LASF155
	.byte	0x3
	.2byte	0x371
	.4byte	0x45c
	.byte	0xc
	.uleb128 0x8
	.4byte	.LASF115
	.byte	0x3
	.2byte	0x372
	.4byte	0x2e8
	.byte	0xe
	.uleb128 0x11
	.ascii	"SR\000"
	.byte	0x3
	.2byte	0x373
	.4byte	0x45c
	.byte	0x10
	.uleb128 0x8
	.4byte	.LASF117
	.byte	0x3
	.2byte	0x374
	.4byte	0x2e8
	.byte	0x12
	.uleb128 0x11
	.ascii	"EGR\000"
	.byte	0x3
	.2byte	0x375
	.4byte	0x45c
	.byte	0x14
	.uleb128 0x8
	.4byte	.LASF118
	.byte	0x3
	.2byte	0x376
	.4byte	0x2e8
	.byte	0x16
	.uleb128 0x8
	.4byte	.LASF156
	.byte	0x3
	.2byte	0x377
	.4byte	0x45c
	.byte	0x18
	.uleb128 0x8
	.4byte	.LASF150
	.byte	0x3
	.2byte	0x378
	.4byte	0x2e8
	.byte	0x1a
	.uleb128 0x8
	.4byte	.LASF157
	.byte	0x3
	.2byte	0x379
	.4byte	0x45c
	.byte	0x1c
	.uleb128 0x8
	.4byte	.LASF158
	.byte	0x3
	.2byte	0x37a
	.4byte	0x2e8
	.byte	0x1e
	.uleb128 0x8
	.4byte	.LASF159
	.byte	0x3
	.2byte	0x37b
	.4byte	0x45c
	.byte	0x20
	.uleb128 0x8
	.4byte	.LASF160
	.byte	0x3
	.2byte	0x37c
	.4byte	0x2e8
	.byte	0x22
	.uleb128 0x11
	.ascii	"CNT\000"
	.byte	0x3
	.2byte	0x37d
	.4byte	0x43b
	.byte	0x24
	.uleb128 0x11
	.ascii	"PSC\000"
	.byte	0x3
	.2byte	0x37e
	.4byte	0x45c
	.byte	0x28
	.uleb128 0x8
	.4byte	.LASF161
	.byte	0x3
	.2byte	0x37f
	.4byte	0x2e8
	.byte	0x2a
	.uleb128 0x11
	.ascii	"ARR\000"
	.byte	0x3
	.2byte	0x380
	.4byte	0x43b
	.byte	0x2c
	.uleb128 0x11
	.ascii	"RCR\000"
	.byte	0x3
	.2byte	0x381
	.4byte	0x45c
	.byte	0x30
	.uleb128 0x8
	.4byte	.LASF162
	.byte	0x3
	.2byte	0x382
	.4byte	0x2e8
	.byte	0x32
	.uleb128 0x8
	.4byte	.LASF163
	.byte	0x3
	.2byte	0x383
	.4byte	0x43b
	.byte	0x34
	.uleb128 0x8
	.4byte	.LASF164
	.byte	0x3
	.2byte	0x384
	.4byte	0x43b
	.byte	0x38
	.uleb128 0x8
	.4byte	.LASF165
	.byte	0x3
	.2byte	0x385
	.4byte	0x43b
	.byte	0x3c
	.uleb128 0x8
	.4byte	.LASF166
	.byte	0x3
	.2byte	0x386
	.4byte	0x43b
	.byte	0x40
	.uleb128 0x8
	.4byte	.LASF167
	.byte	0x3
	.2byte	0x387
	.4byte	0x45c
	.byte	0x44
	.uleb128 0x8
	.4byte	.LASF168
	.byte	0x3
	.2byte	0x388
	.4byte	0x2e8
	.byte	0x46
	.uleb128 0x11
	.ascii	"DCR\000"
	.byte	0x3
	.2byte	0x389
	.4byte	0x45c
	.byte	0x48
	.uleb128 0x8
	.4byte	.LASF169
	.byte	0x3
	.2byte	0x38a
	.4byte	0x2e8
	.byte	0x4a
	.uleb128 0x8
	.4byte	.LASF170
	.byte	0x3
	.2byte	0x38b
	.4byte	0x45c
	.byte	0x4c
	.uleb128 0x8
	.4byte	.LASF171
	.byte	0x3
	.2byte	0x38c
	.4byte	0x2e8
	.byte	0x4e
	.uleb128 0x11
	.ascii	"OR\000"
	.byte	0x3
	.2byte	0x38d
	.4byte	0x45c
	.byte	0x50
	.uleb128 0x8
	.4byte	.LASF172
	.byte	0x3
	.2byte	0x38e
	.4byte	0x2e8
	.byte	0x52
	.byte	0
	.uleb128 0xf
	.4byte	.LASF173
	.byte	0x3
	.2byte	0x38f
	.4byte	0x6ae
	.uleb128 0x5
	.byte	0x1
	.byte	0x8
	.4byte	.LASF174
	.uleb128 0x12
	.byte	0x3
	.byte	0x2
	.byte	0xa
	.4byte	0x8c4
	.uleb128 0x13
	.ascii	"r\000"
	.byte	0x2
	.byte	0xb
	.4byte	0x896
	.byte	0
	.uleb128 0x13
	.ascii	"g\000"
	.byte	0x2
	.byte	0xc
	.4byte	0x896
	.byte	0x1
	.uleb128 0x13
	.ascii	"b\000"
	.byte	0x2
	.byte	0xd
	.4byte	0x896
	.byte	0x2
	.byte	0
	.uleb128 0x4
	.4byte	.LASF175
	.byte	0x2
	.byte	0xe
	.4byte	0x89d
	.uleb128 0x14
	.4byte	.LASF235
	.byte	0x2
	.byte	0x2
	.byte	0xb7
	.4byte	0x8f0
	.uleb128 0x13
	.ascii	"x\000"
	.byte	0x2
	.byte	0xb8
	.4byte	0x896
	.byte	0
	.uleb128 0x13
	.ascii	"y\000"
	.byte	0x2
	.byte	0xb9
	.4byte	0x896
	.byte	0x1
	.byte	0
	.uleb128 0x15
	.4byte	.LASF236
	.byte	0x1
	.2byte	0x430
	.4byte	.LFB95
	.4byte	.LFE95-.LFB95
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x916
	.uleb128 0x16
	.4byte	.LASF176
	.byte	0x1
	.2byte	0x430
	.4byte	0x260
	.uleb128 0x2
	.byte	0x91
	.sleb128 -9
	.byte	0
	.uleb128 0x17
	.4byte	.LASF196
	.byte	0x2
	.byte	0x1c
	.4byte	0x8c4
	.4byte	.LFB110
	.4byte	.LFE110-.LFB110
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x962
	.uleb128 0x18
	.ascii	"x\000"
	.byte	0x2
	.byte	0x1c
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x18
	.ascii	"y\000"
	.byte	0x2
	.byte	0x1c
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x19
	.ascii	"c\000"
	.byte	0x2
	.byte	0x1e
	.4byte	0x8c4
	.uleb128 0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x19
	.ascii	"idx\000"
	.byte	0x2
	.byte	0x1f
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1a
	.4byte	.LASF178
	.byte	0x2
	.byte	0x29
	.4byte	.LFB111
	.4byte	.LFE111-.LFB111
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x9ac
	.uleb128 0x18
	.ascii	"x\000"
	.byte	0x2
	.byte	0x29
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x18
	.ascii	"y\000"
	.byte	0x2
	.byte	0x29
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x1b
	.4byte	.LASF177
	.byte	0x2
	.byte	0x29
	.4byte	0x8c4
	.uleb128 0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x19
	.ascii	"idx\000"
	.byte	0x2
	.byte	0x2b
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x1c
	.4byte	.LASF179
	.byte	0x2
	.byte	0x32
	.4byte	.LFB112
	.4byte	.LFE112-.LFB112
	.uleb128 0x1
	.byte	0x9c
	.4byte	0xa1f
	.uleb128 0x18
	.ascii	"x\000"
	.byte	0x2
	.byte	0x32
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x18
	.ascii	"y\000"
	.byte	0x2
	.byte	0x32
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x18
	.ascii	"i\000"
	.byte	0x2
	.byte	0x32
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x1d
	.4byte	.LASF180
	.byte	0x2
	.byte	0x34
	.4byte	0x2cf
	.uleb128 0x5
	.byte	0x3
	.4byte	count.5473
	.uleb128 0x19
	.ascii	"a\000"
	.byte	0x2
	.byte	0x36
	.4byte	0xa1f
	.uleb128 0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x1d
	.4byte	.LASF177
	.byte	0x2
	.byte	0x37
	.4byte	0x8c4
	.uleb128 0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1d
	.4byte	.LASF181
	.byte	0x2
	.byte	0x37
	.4byte	0x8c4
	.uleb128 0x2
	.byte	0x91
	.sleb128 -20
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.byte	0x4
	.4byte	.LASF182
	.uleb128 0x1c
	.4byte	.LASF183
	.byte	0x2
	.byte	0x52
	.4byte	.LFB113
	.4byte	.LFE113-.LFB113
	.uleb128 0x1
	.byte	0x9c
	.4byte	0xafb
	.uleb128 0x18
	.ascii	"x0\000"
	.byte	0x2
	.byte	0x52
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x18
	.ascii	"y0\000"
	.byte	0x2
	.byte	0x52
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x18
	.ascii	"x1\000"
	.byte	0x2
	.byte	0x52
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x18
	.ascii	"y1\000"
	.byte	0x2
	.byte	0x52
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x19
	.ascii	"dx\000"
	.byte	0x2
	.byte	0x54
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x1e
	.ascii	"abs\000"
	.byte	0x6
	.byte	0
	.4byte	0x2cf
	.4byte	0xa8d
	.uleb128 0x1f
	.byte	0
	.uleb128 0x19
	.ascii	"sx\000"
	.byte	0x2
	.byte	0x54
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x19
	.ascii	"dy\000"
	.byte	0x2
	.byte	0x55
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x19
	.ascii	"sy\000"
	.byte	0x2
	.byte	0x55
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x19
	.ascii	"err\000"
	.byte	0x2
	.byte	0x56
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x19
	.ascii	"e2\000"
	.byte	0x2
	.byte	0x56
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x19
	.ascii	"x2\000"
	.byte	0x2
	.byte	0x56
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x19
	.ascii	"ed\000"
	.byte	0x2
	.byte	0x57
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x20
	.4byte	.LASF184
	.byte	0x6
	.byte	0
	.4byte	0xafb
	.uleb128 0x21
	.4byte	0xafb
	.byte	0
	.byte	0
	.uleb128 0x5
	.byte	0x8
	.byte	0x4
	.4byte	.LASF185
	.uleb128 0x1c
	.4byte	.LASF186
	.byte	0x2
	.byte	0x6c
	.4byte	.LFB114
	.4byte	.LFE114-.LFB114
	.uleb128 0x1
	.byte	0x9c
	.4byte	0xbf2
	.uleb128 0x18
	.ascii	"x0\000"
	.byte	0x2
	.byte	0x6c
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x18
	.ascii	"y0\000"
	.byte	0x2
	.byte	0x6c
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x18
	.ascii	"x1\000"
	.byte	0x2
	.byte	0x6c
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x18
	.ascii	"y1\000"
	.byte	0x2
	.byte	0x6c
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -64
	.uleb128 0x18
	.ascii	"wd\000"
	.byte	0x2
	.byte	0x6c
	.4byte	0xa1f
	.uleb128 0x3
	.byte	0x91
	.sleb128 -68
	.uleb128 0x19
	.ascii	"dx\000"
	.byte	0x2
	.byte	0x6e
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x1e
	.ascii	"abs\000"
	.byte	0x6
	.byte	0
	.4byte	0x2cf
	.4byte	0xb77
	.uleb128 0x1f
	.byte	0
	.uleb128 0x19
	.ascii	"sx\000"
	.byte	0x2
	.byte	0x6e
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x19
	.ascii	"dy\000"
	.byte	0x2
	.byte	0x6f
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x19
	.ascii	"sy\000"
	.byte	0x2
	.byte	0x6f
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x19
	.ascii	"err\000"
	.byte	0x2
	.byte	0x70
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x19
	.ascii	"e2\000"
	.byte	0x2
	.byte	0x70
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x19
	.ascii	"x2\000"
	.byte	0x2
	.byte	0x70
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x19
	.ascii	"y2\000"
	.byte	0x2
	.byte	0x70
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x19
	.ascii	"ed\000"
	.byte	0x2
	.byte	0x71
	.4byte	0xa1f
	.uleb128 0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x20
	.4byte	.LASF184
	.byte	0x6
	.byte	0
	.4byte	0xafb
	.uleb128 0x21
	.4byte	0xafb
	.byte	0
	.byte	0
	.uleb128 0x1c
	.4byte	.LASF187
	.byte	0x2
	.byte	0x86
	.4byte	.LFB115
	.4byte	.LFE115-.LFB115
	.uleb128 0x1
	.byte	0x9c
	.4byte	0xc91
	.uleb128 0x18
	.ascii	"xm\000"
	.byte	0x2
	.byte	0x86
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x18
	.ascii	"ym\000"
	.byte	0x2
	.byte	0x86
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x18
	.ascii	"r\000"
	.byte	0x2
	.byte	0x86
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x19
	.ascii	"x\000"
	.byte	0x2
	.byte	0x88
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x19
	.ascii	"y\000"
	.byte	0x2
	.byte	0x88
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x19
	.ascii	"i\000"
	.byte	0x2
	.byte	0x89
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x19
	.ascii	"x2\000"
	.byte	0x2
	.byte	0x89
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x19
	.ascii	"e2\000"
	.byte	0x2
	.byte	0x89
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x19
	.ascii	"err\000"
	.byte	0x2
	.byte	0x89
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x22
	.4byte	.LBB2
	.4byte	.LBE2-.LBB2
	.uleb128 0x23
	.ascii	"abs\000"
	.byte	0x6
	.byte	0
	.4byte	0x2cf
	.uleb128 0x1f
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x1c
	.4byte	.LASF188
	.byte	0x2
	.byte	0xce
	.4byte	.LFB116
	.4byte	.LFE116-.LFB116
	.uleb128 0x1
	.byte	0x9c
	.4byte	0xccf
	.uleb128 0x18
	.ascii	"dst\000"
	.byte	0x2
	.byte	0xce
	.4byte	0xccf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x18
	.ascii	"src\000"
	.byte	0x2
	.byte	0xce
	.4byte	0xcd5
	.uleb128 0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x18
	.ascii	"n\000"
	.byte	0x2
	.byte	0xce
	.4byte	0x2e8
	.uleb128 0x2
	.byte	0x91
	.sleb128 -18
	.byte	0
	.uleb128 0x24
	.byte	0x4
	.4byte	0x896
	.uleb128 0x24
	.byte	0x4
	.4byte	0xcdb
	.uleb128 0x25
	.4byte	0x896
	.uleb128 0x26
	.4byte	.LASF189
	.byte	0x2
	.2byte	0x115
	.4byte	.LFB117
	.4byte	.LFE117-.LFB117
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x26
	.4byte	.LASF190
	.byte	0x2
	.2byte	0x126
	.4byte	.LFB118
	.4byte	.LFE118-.LFB118
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x27
	.4byte	.LASF237
	.byte	0x2
	.2byte	0x131
	.4byte	.LFB119
	.4byte	.LFE119-.LFB119
	.uleb128 0x1
	.byte	0x9c
	.4byte	0xd2a
	.uleb128 0x28
	.ascii	"inc\000"
	.byte	0x2
	.2byte	0x133
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x29
	.4byte	.LASF191
	.byte	0x2
	.2byte	0x14c
	.4byte	.LFB120
	.4byte	.LFE120-.LFB120
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x26
	.4byte	.LASF192
	.byte	0x2
	.2byte	0x170
	.4byte	.LFB121
	.4byte	.LFE121-.LFB121
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x2a
	.4byte	.LASF193
	.byte	0x2
	.2byte	0x185
	.4byte	.LFB122
	.4byte	.LFE122-.LFB122
	.uleb128 0x1
	.byte	0x9c
	.4byte	0xdae
	.uleb128 0x16
	.4byte	.LASF194
	.byte	0x2
	.2byte	0x185
	.4byte	0xdae
	.uleb128 0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x16
	.4byte	.LASF195
	.byte	0x2
	.2byte	0x185
	.4byte	0x2fe
	.uleb128 0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x16
	.4byte	.LASF177
	.byte	0x2
	.2byte	0x185
	.4byte	0x2e8
	.uleb128 0x2
	.byte	0x91
	.sleb128 -26
	.uleb128 0x28
	.ascii	"i\000"
	.byte	0x2
	.2byte	0x187
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x28
	.ascii	"idx\000"
	.byte	0x2
	.2byte	0x187
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x24
	.byte	0x4
	.4byte	0x8cf
	.uleb128 0x2b
	.4byte	.LASF197
	.byte	0x2
	.2byte	0x211
	.4byte	0xa1f
	.4byte	.LFB123
	.4byte	.LFE123-.LFB123
	.uleb128 0x1
	.byte	0x9c
	.4byte	0xddc
	.uleb128 0x2c
	.ascii	"x\000"
	.byte	0x2
	.2byte	0x211
	.4byte	0xa1f
	.uleb128 0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x2d
	.4byte	.LASF198
	.byte	0x2
	.2byte	0x217
	.4byte	.LFB124
	.4byte	.LFE124-.LFB124
	.uleb128 0x1
	.byte	0x9c
	.4byte	0xe65
	.uleb128 0x16
	.4byte	.LASF199
	.byte	0x2
	.2byte	0x217
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x16
	.4byte	.LASF200
	.byte	0x2
	.2byte	0x217
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x2c
	.ascii	"r\000"
	.byte	0x2
	.2byte	0x217
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x16
	.4byte	.LASF201
	.byte	0x2
	.2byte	0x217
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x2c
	.ascii	"c\000"
	.byte	0x2
	.2byte	0x217
	.4byte	0x8c4
	.uleb128 0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x2c
	.ascii	"w\000"
	.byte	0x2
	.2byte	0x217
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 4
	.uleb128 0x2e
	.4byte	.LASF202
	.byte	0x2
	.2byte	0x219
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x2e
	.4byte	.LASF203
	.byte	0x2
	.2byte	0x219
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x2d
	.4byte	.LASF204
	.byte	0x2
	.2byte	0x231
	.4byte	.LFB125
	.4byte	.LFE125-.LFB125
	.uleb128 0x1
	.byte	0x9c
	.4byte	0xec1
	.uleb128 0x16
	.4byte	.LASF205
	.byte	0x2
	.2byte	0x231
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x16
	.4byte	.LASF206
	.byte	0x2
	.2byte	0x231
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x2c
	.ascii	"r\000"
	.byte	0x2
	.2byte	0x231
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x2c
	.ascii	"c\000"
	.byte	0x2
	.2byte	0x231
	.4byte	0x8c4
	.uleb128 0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x28
	.ascii	"i\000"
	.byte	0x2
	.2byte	0x233
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x2f
	.4byte	.LASF207
	.byte	0x2
	.2byte	0x240
	.4byte	0x2cf
	.4byte	.LFB126
	.4byte	.LFE126-.LFB126
	.uleb128 0x1
	.byte	0x9c
	.4byte	0xf45
	.uleb128 0x2e
	.4byte	.LASF208
	.byte	0x2
	.2byte	0x242
	.4byte	0x896
	.uleb128 0x2
	.byte	0x91
	.sleb128 -17
	.uleb128 0x2e
	.4byte	.LASF209
	.byte	0x2
	.2byte	0x244
	.4byte	0x8c4
	.uleb128 0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x2e
	.4byte	.LASF210
	.byte	0x2
	.2byte	0x245
	.4byte	0x8c4
	.uleb128 0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x2e
	.4byte	.LASF211
	.byte	0x2
	.2byte	0x246
	.4byte	0x8c4
	.uleb128 0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x28
	.ascii	"sec\000"
	.byte	0x2
	.2byte	0x248
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x28
	.ascii	"min\000"
	.byte	0x2
	.2byte	0x249
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x2e
	.4byte	.LASF212
	.byte	0x2
	.2byte	0x24a
	.4byte	0x2cf
	.uleb128 0x2
	.byte	0x91
	.sleb128 -32
	.byte	0
	.uleb128 0x30
	.4byte	.LASF213
	.byte	0x1
	.2byte	0x51b
	.4byte	0x461
	.uleb128 0xb
	.4byte	0x896
	.4byte	0xf64
	.uleb128 0x31
	.4byte	0x3d5
	.4byte	0x1c1ff
	.byte	0
	.uleb128 0x32
	.4byte	.LASF214
	.byte	0x2
	.byte	0x16
	.4byte	0xf51
	.uleb128 0x5
	.byte	0x3
	.4byte	image_buffer
	.uleb128 0x32
	.4byte	.LASF215
	.byte	0x2
	.byte	0x19
	.4byte	0x896
	.uleb128 0x5
	.byte	0x3
	.4byte	color_hi
	.uleb128 0x32
	.4byte	.LASF216
	.byte	0x2
	.byte	0x19
	.4byte	0x896
	.uleb128 0x5
	.byte	0x3
	.4byte	color_lo
	.uleb128 0x32
	.4byte	.LASF217
	.byte	0x2
	.byte	0x14
	.4byte	0x8c4
	.uleb128 0x5
	.byte	0x3
	.4byte	line_color
	.uleb128 0xb
	.4byte	0x896
	.4byte	0xfb9
	.uleb128 0xe
	.4byte	0x3d5
	.2byte	0x1c1f
	.byte	0
	.uleb128 0x32
	.4byte	.LASF218
	.byte	0x2
	.byte	0x17
	.4byte	0xfa8
	.uleb128 0x5
	.byte	0x3
	.4byte	image_buffer_tmp
	.uleb128 0xb
	.4byte	0x896
	.4byte	0xfd5
	.uleb128 0x33
	.byte	0
	.uleb128 0x34
	.4byte	.LASF219
	.byte	0x2
	.byte	0xad
	.4byte	0xfe0
	.uleb128 0x25
	.4byte	0xfca
	.uleb128 0xb
	.4byte	0x896
	.4byte	0xff5
	.uleb128 0xc
	.4byte	0x3d5
	.byte	0xff
	.byte	0
	.uleb128 0x32
	.4byte	.LASF220
	.byte	0x2
	.byte	0xaf
	.4byte	0x1006
	.uleb128 0x5
	.byte	0x3
	.4byte	gamma_correction_LUT
	.uleb128 0x25
	.4byte	0xfe5
	.uleb128 0x32
	.4byte	.LASF221
	.byte	0x2
	.byte	0xb3
	.4byte	0x101c
	.uleb128 0x5
	.byte	0x3
	.4byte	gamma_correction_LUT2
	.uleb128 0x25
	.4byte	0xfe5
	.uleb128 0xb
	.4byte	0x8cf
	.4byte	0x1031
	.uleb128 0xc
	.4byte	0x3d5
	.byte	0x9
	.byte	0
	.uleb128 0x32
	.4byte	.LASF222
	.byte	0x2
	.byte	0xbc
	.4byte	0x1042
	.uleb128 0x5
	.byte	0x3
	.4byte	clock_addon
	.uleb128 0x25
	.4byte	0x1021
	.uleb128 0x32
	.4byte	.LASF223
	.byte	0x2
	.byte	0xcb
	.4byte	0x896
	.uleb128 0x5
	.byte	0x3
	.4byte	DMA2_Stream7_compleated
	.uleb128 0x35
	.4byte	.LASF224
	.byte	0x2
	.2byte	0x120
	.4byte	0x2cf
	.uleb128 0x5
	.byte	0x3
	.4byte	img_buff_n
	.uleb128 0x35
	.4byte	.LASF225
	.byte	0x2
	.2byte	0x120
	.4byte	0x2cf
	.uleb128 0x5
	.byte	0x3
	.4byte	img_buff_tot
	.uleb128 0x35
	.4byte	.LASF226
	.byte	0x2
	.2byte	0x121
	.4byte	0x2cf
	.uleb128 0x5
	.byte	0x3
	.4byte	img_buff_src
	.uleb128 0x35
	.4byte	.LASF227
	.byte	0x2
	.2byte	0x122
	.4byte	0x2cf
	.uleb128 0x5
	.byte	0x3
	.4byte	img_buff_dst
	.uleb128 0x35
	.4byte	.LASF228
	.byte	0x2
	.2byte	0x123
	.4byte	0x896
	.uleb128 0x5
	.byte	0x3
	.4byte	img_buffer_copy_compleated
	.uleb128 0x35
	.4byte	.LASF229
	.byte	0x2
	.2byte	0x16e
	.4byte	0x896
	.uleb128 0x5
	.byte	0x3
	.4byte	show_next_frame
	.uleb128 0xb
	.4byte	0xa1f
	.4byte	0x10d4
	.uleb128 0xc
	.4byte	0x3d5
	.byte	0x3b
	.byte	0
	.uleb128 0x35
	.4byte	.LASF230
	.byte	0x2
	.2byte	0x191
	.4byte	0x10e6
	.uleb128 0x5
	.byte	0x3
	.4byte	sin_value
	.uleb128 0x25
	.4byte	0x10c4
	.uleb128 0x35
	.4byte	.LASF231
	.byte	0x2
	.2byte	0x1d1
	.4byte	0x10fd
	.uleb128 0x5
	.byte	0x3
	.4byte	cos_value
	.uleb128 0x25
	.4byte	0x10c4
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x4
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2116
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x26
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x27
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2116
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x28
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x29
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2116
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x2a
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2b
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2c
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x2d
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2116
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2e
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x2f
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2116
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x30
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x31
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x32
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x33
	.uleb128 0x21
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x34
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x35
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x1c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.Ltext0
	.4byte	.Letext0-.Ltext0
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF86:
	.ascii	"DCMI_IRQn\000"
.LASF88:
	.ascii	"HASH_RNG_IRQn\000"
.LASF142:
	.ascii	"APB1ENR\000"
.LASF210:
	.ascii	"min_color\000"
.LASF233:
	.ascii	"main.c\000"
.LASF183:
	.ascii	"plotLineAA\000"
.LASF218:
	.ascii	"image_buffer_tmp\000"
.LASF225:
	.ascii	"img_buff_tot\000"
.LASF148:
	.ascii	"APB2LPENR\000"
.LASF76:
	.ascii	"DMA2_Stream5_IRQn\000"
.LASF205:
	.ascii	"x_center\000"
.LASF57:
	.ascii	"SDIO_IRQn\000"
.LASF137:
	.ascii	"APB2RSTR\000"
.LASF30:
	.ascii	"CAN1_SCE_IRQn\000"
.LASF204:
	.ascii	"draw_circle\000"
.LASF87:
	.ascii	"CRYP_IRQn\000"
.LASF42:
	.ascii	"I2C2_ER_IRQn\000"
.LASF64:
	.ascii	"DMA2_Stream0_IRQn\000"
.LASF202:
	.ascii	"x_end\000"
.LASF38:
	.ascii	"TIM4_IRQn\000"
.LASF70:
	.ascii	"ETH_WKUP_IRQn\000"
.LASF103:
	.ascii	"unsigned int\000"
.LASF24:
	.ascii	"DMA1_Stream5_IRQn\000"
.LASF149:
	.ascii	"BDCR\000"
.LASF211:
	.ascii	"hour_color\000"
.LASF97:
	.ascii	"__int32_t\000"
.LASF19:
	.ascii	"DMA1_Stream0_IRQn\000"
.LASF12:
	.ascii	"FLASH_IRQn\000"
.LASF203:
	.ascii	"y_end\000"
.LASF14:
	.ascii	"EXTI0_IRQn\000"
.LASF83:
	.ascii	"OTG_HS_EP1_IN_IRQn\000"
.LASF48:
	.ascii	"EXTI15_10_IRQn\000"
.LASF51:
	.ascii	"TIM8_BRK_TIM12_IRQn\000"
.LASF122:
	.ascii	"NDTR\000"
.LASF145:
	.ascii	"AHB2LPENR\000"
.LASF6:
	.ascii	"PendSV_IRQn\000"
.LASF50:
	.ascii	"OTG_FS_WKUP_IRQn\000"
.LASF60:
	.ascii	"UART4_IRQn\000"
.LASF13:
	.ascii	"RCC_IRQn\000"
.LASF1:
	.ascii	"MemoryManagement_IRQn\000"
.LASF173:
	.ascii	"TIM_TypeDef\000"
.LASF92:
	.ascii	"signed char\000"
.LASF222:
	.ascii	"clock_addon\000"
.LASF159:
	.ascii	"CCER\000"
.LASF107:
	.ascii	"uint32_t\000"
.LASF10:
	.ascii	"TAMP_STAMP_IRQn\000"
.LASF167:
	.ascii	"BDTR\000"
.LASF108:
	.ascii	"ISER\000"
.LASF182:
	.ascii	"float\000"
.LASF75:
	.ascii	"OTG_FS_IRQn\000"
.LASF129:
	.ascii	"HIFCR\000"
.LASF184:
	.ascii	"sqrt\000"
.LASF139:
	.ascii	"AHB1ENR\000"
.LASF163:
	.ascii	"CCR1\000"
.LASF151:
	.ascii	"SSCGR\000"
.LASF165:
	.ascii	"CCR3\000"
.LASF111:
	.ascii	"RSERVED1\000"
.LASF66:
	.ascii	"DMA2_Stream2_IRQn\000"
.LASF157:
	.ascii	"CCMR2\000"
.LASF43:
	.ascii	"SPI1_IRQn\000"
.LASF32:
	.ascii	"TIM1_BRK_TIM9_IRQn\000"
.LASF236:
	.ascii	"NVIC_EnableIRQ\000"
.LASF102:
	.ascii	"long long unsigned int\000"
.LASF209:
	.ascii	"sec_color\000"
.LASF158:
	.ascii	"RESERVED7\000"
.LASF55:
	.ascii	"DMA1_Stream7_IRQn\000"
.LASF179:
	.ascii	"setPixelAA\000"
.LASF95:
	.ascii	"__uint16_t\000"
.LASF21:
	.ascii	"DMA1_Stream2_IRQn\000"
.LASF74:
	.ascii	"CAN2_SCE_IRQn\000"
.LASF29:
	.ascii	"CAN1_RX1_IRQn\000"
.LASF221:
	.ascii	"gamma_correction_LUT2\000"
.LASF196:
	.ascii	"get_pixel\000"
.LASF190:
	.ascii	"init_img_buffer_copy\000"
.LASF135:
	.ascii	"AHB3RSTR\000"
.LASF206:
	.ascii	"y_center\000"
.LASF199:
	.ascii	"x_start\000"
.LASF90:
	.ascii	"IRQn_Type\000"
.LASF234:
	.ascii	"/home/sharpovici-ostrakovici/stm_dev/STM32F407/exer"
	.ascii	"cise/project\000"
.LASF78:
	.ascii	"DMA2_Stream7_IRQn\000"
.LASF46:
	.ascii	"USART2_IRQn\000"
.LASF153:
	.ascii	"RCC_TypeDef\000"
.LASF81:
	.ascii	"I2C3_ER_IRQn\000"
.LASF11:
	.ascii	"RTC_WKUP_IRQn\000"
.LASF85:
	.ascii	"OTG_HS_IRQn\000"
.LASF155:
	.ascii	"DIER\000"
.LASF123:
	.ascii	"M0AR\000"
.LASF141:
	.ascii	"AHB3ENR\000"
.LASF212:
	.ascii	"hour\000"
.LASF16:
	.ascii	"EXTI2_IRQn\000"
.LASF180:
	.ascii	"count\000"
.LASF136:
	.ascii	"APB1RSTR\000"
.LASF41:
	.ascii	"I2C2_EV_IRQn\000"
.LASF59:
	.ascii	"SPI3_IRQn\000"
.LASF200:
	.ascii	"y_start\000"
.LASF174:
	.ascii	"char\000"
.LASF37:
	.ascii	"TIM3_IRQn\000"
.LASF230:
	.ascii	"sin_value\000"
.LASF195:
	.ascii	"pixels_sz\000"
.LASF232:
	.ascii	"GNU C 4.9.3 20141119 (release) [ARM/embedded-4_9-br"
	.ascii	"anch revision 218278] -mlittle-endian -mthumb -mcpu"
	.ascii	"=cortex-m4 -mthumb-interwork -mfloat-abi=hard -mfpu"
	.ascii	"=fpv4-sp-d16 -g -O0 -fsingle-precision-constant\000"
.LASF191:
	.ascii	"init_clock\000"
.LASF187:
	.ascii	"plotCircleAA\000"
.LASF23:
	.ascii	"DMA1_Stream4_IRQn\000"
.LASF104:
	.ascii	"uint8_t\000"
.LASF164:
	.ascii	"CCR2\000"
.LASF198:
	.ascii	"draw_line\000"
.LASF166:
	.ascii	"CCR4\000"
.LASF154:
	.ascii	"SMCR\000"
.LASF52:
	.ascii	"TIM8_UP_TIM13_IRQn\000"
.LASF215:
	.ascii	"color_hi\000"
.LASF181:
	.ascii	"backcolor\000"
.LASF101:
	.ascii	"long long int\000"
.LASF8:
	.ascii	"WWDG_IRQn\000"
.LASF73:
	.ascii	"CAN2_RX1_IRQn\000"
.LASF127:
	.ascii	"HISR\000"
.LASF147:
	.ascii	"APB1LPENR\000"
.LASF68:
	.ascii	"DMA2_Stream4_IRQn\000"
.LASF217:
	.ascii	"line_color\000"
.LASF219:
	.ascii	"clock_img\000"
.LASF2:
	.ascii	"BusFault_IRQn\000"
.LASF130:
	.ascii	"DMA_TypeDef\000"
.LASF34:
	.ascii	"TIM1_TRG_COM_TIM11_IRQn\000"
.LASF229:
	.ascii	"show_next_frame\000"
.LASF18:
	.ascii	"EXTI4_IRQn\000"
.LASF125:
	.ascii	"DMA_Stream_TypeDef\000"
.LASF9:
	.ascii	"PVD_IRQn\000"
.LASF26:
	.ascii	"ADC_IRQn\000"
.LASF189:
	.ascii	"DMA2_Stream7_IRQHandler\000"
.LASF114:
	.ascii	"ICPR\000"
.LASF143:
	.ascii	"APB2ENR\000"
.LASF175:
	.ascii	"COLOR\000"
.LASF58:
	.ascii	"TIM5_IRQn\000"
.LASF144:
	.ascii	"AHB1LPENR\000"
.LASF39:
	.ascii	"I2C1_EV_IRQn\000"
.LASF27:
	.ascii	"CAN1_TX_IRQn\000"
.LASF105:
	.ascii	"uint16_t\000"
.LASF20:
	.ascii	"DMA1_Stream1_IRQn\000"
.LASF176:
	.ascii	"IRQn\000"
.LASF197:
	.ascii	"abs_f\000"
.LASF28:
	.ascii	"CAN1_RX0_IRQn\000"
.LASF134:
	.ascii	"AHB2RSTR\000"
.LASF119:
	.ascii	"STIR\000"
.LASF201:
	.ascii	"angle\000"
.LASF216:
	.ascii	"color_lo\000"
.LASF49:
	.ascii	"RTC_Alarm_IRQn\000"
.LASF223:
	.ascii	"DMA2_Stream7_compleated\000"
.LASF109:
	.ascii	"RESERVED0\000"
.LASF138:
	.ascii	"RESERVED1\000"
.LASF113:
	.ascii	"RESERVED2\000"
.LASF115:
	.ascii	"RESERVED3\000"
.LASF117:
	.ascii	"RESERVED4\000"
.LASF118:
	.ascii	"RESERVED5\000"
.LASF150:
	.ascii	"RESERVED6\000"
.LASF94:
	.ascii	"short int\000"
.LASF160:
	.ascii	"RESERVED8\000"
.LASF161:
	.ascii	"RESERVED9\000"
.LASF77:
	.ascii	"DMA2_Stream6_IRQn\000"
.LASF80:
	.ascii	"I2C3_EV_IRQn\000"
.LASF98:
	.ascii	"long int\000"
.LASF45:
	.ascii	"USART1_IRQn\000"
.LASF170:
	.ascii	"DMAR\000"
.LASF61:
	.ascii	"UART5_IRQn\000"
.LASF237:
	.ascii	"process_img_buffer_copy\000"
.LASF194:
	.ascii	"pixels\000"
.LASF213:
	.ascii	"ITM_RxBuffer\000"
.LASF0:
	.ascii	"NonMaskableInt_IRQn\000"
.LASF65:
	.ascii	"DMA2_Stream1_IRQn\000"
.LASF152:
	.ascii	"PLLI2SCFGR\000"
.LASF35:
	.ascii	"TIM1_CC_IRQn\000"
.LASF54:
	.ascii	"TIM8_CC_IRQn\000"
.LASF25:
	.ascii	"DMA1_Stream6_IRQn\000"
.LASF177:
	.ascii	"color\000"
.LASF132:
	.ascii	"CFGR\000"
.LASF178:
	.ascii	"set_pixel\000"
.LASF56:
	.ascii	"FSMC_IRQn\000"
.LASF15:
	.ascii	"EXTI1_IRQn\000"
.LASF188:
	.ascii	"memory_copy\000"
.LASF121:
	.ascii	"NVIC_Type\000"
.LASF62:
	.ascii	"TIM6_DAC_IRQn\000"
.LASF131:
	.ascii	"PLLCFGR\000"
.LASF40:
	.ascii	"I2C1_ER_IRQn\000"
.LASF124:
	.ascii	"M1AR\000"
.LASF79:
	.ascii	"USART6_IRQn\000"
.LASF193:
	.ascii	"draw_pixelmap\000"
.LASF7:
	.ascii	"SysTick_IRQn\000"
.LASF146:
	.ascii	"AHB3LPENR\000"
.LASF120:
	.ascii	"sizetype\000"
.LASF100:
	.ascii	"long unsigned int\000"
.LASF36:
	.ascii	"TIM2_IRQn\000"
.LASF231:
	.ascii	"cos_value\000"
.LASF140:
	.ascii	"AHB2ENR\000"
.LASF106:
	.ascii	"int32_t\000"
.LASF156:
	.ascii	"CCMR1\000"
.LASF84:
	.ascii	"OTG_HS_WKUP_IRQn\000"
.LASF226:
	.ascii	"img_buff_src\000"
.LASF235:
	.ascii	"pixel\000"
.LASF5:
	.ascii	"DebugMonitor_IRQn\000"
.LASF128:
	.ascii	"LIFCR\000"
.LASF3:
	.ascii	"UsageFault_IRQn\000"
.LASF93:
	.ascii	"unsigned char\000"
.LASF99:
	.ascii	"__uint32_t\000"
.LASF31:
	.ascii	"EXTI9_5_IRQn\000"
.LASF4:
	.ascii	"SVCall_IRQn\000"
.LASF53:
	.ascii	"TIM8_TRG_COM_TIM14_IRQn\000"
.LASF162:
	.ascii	"RESERVED10\000"
.LASF168:
	.ascii	"RESERVED11\000"
.LASF169:
	.ascii	"RESERVED12\000"
.LASF171:
	.ascii	"RESERVED13\000"
.LASF172:
	.ascii	"RESERVED14\000"
.LASF72:
	.ascii	"CAN2_RX0_IRQn\000"
.LASF110:
	.ascii	"ICER\000"
.LASF67:
	.ascii	"DMA2_Stream3_IRQn\000"
.LASF44:
	.ascii	"SPI2_IRQn\000"
.LASF116:
	.ascii	"IABR\000"
.LASF63:
	.ascii	"TIM7_IRQn\000"
.LASF220:
	.ascii	"gamma_correction_LUT\000"
.LASF91:
	.ascii	"__uint8_t\000"
.LASF126:
	.ascii	"LISR\000"
.LASF22:
	.ascii	"DMA1_Stream3_IRQn\000"
.LASF71:
	.ascii	"CAN2_TX_IRQn\000"
.LASF96:
	.ascii	"short unsigned int\000"
.LASF17:
	.ascii	"EXTI3_IRQn\000"
.LASF207:
	.ascii	"main\000"
.LASF228:
	.ascii	"img_buffer_copy_compleated\000"
.LASF186:
	.ascii	"plotLineWidth\000"
.LASF192:
	.ascii	"TIM2_IRQHandler\000"
.LASF112:
	.ascii	"ISPR\000"
.LASF214:
	.ascii	"image_buffer\000"
.LASF224:
	.ascii	"img_buff_n\000"
.LASF185:
	.ascii	"double\000"
.LASF227:
	.ascii	"img_buff_dst\000"
.LASF82:
	.ascii	"OTG_HS_EP1_OUT_IRQn\000"
.LASF47:
	.ascii	"USART3_IRQn\000"
.LASF33:
	.ascii	"TIM1_UP_TIM10_IRQn\000"
.LASF89:
	.ascii	"FPU_IRQn\000"
.LASF69:
	.ascii	"ETH_IRQn\000"
.LASF208:
	.ascii	"init_image_buffer_state\000"
.LASF133:
	.ascii	"AHB1RSTR\000"
	.ident	"GCC: (GNU Tools for ARM Embedded Processors) 4.9.3 20141119 (release) [ARM/embedded-4_9-branch revision 218278]"
