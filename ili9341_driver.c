#include "ili9341_driver.h"

void init_ILI9341_GPIO()
{
	// All GPIO ports are connected to AHB1 bus
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;

	// (reference manual pg.278) 
	// Set GPIOs mode as alternate function mode
	GPIOA->MODER &= ~( GPIO_MODER_MODER8  |
										 GPIO_MODER_MODER9  |
										 GPIO_MODER_MODER10 );


	GPIOA->MODER |= 	GPIO_MODER_MODER8_0 
									| GPIO_MODER_MODER9_0
									| GPIO_MODER_MODER10_0;


	// (reference manual pg.279)
	// Set GPIOs output type to push-pull configuration
	GPIOA->OTYPER &= ~( GPIO_OTYPER_OT_8  |
											GPIO_OTYPER_OT_9  |
											GPIO_OTYPER_OT_10 );	


	// (reference manual pg.279)
	// Set GPIOs output speed to 'High speed'
	GPIOA->OSPEEDR |=   GPIO_OSPEEDER_OSPEEDR8
										| GPIO_OSPEEDER_OSPEEDR9
										| GPIO_OSPEEDER_OSPEEDR10;			


	// (reference manual pg.280)
	// Set GPIOs configuration to no pull-up, pull-down except
	// for PA10 which will be in pull-up configuration
	GPIOA->PUPDR &= ~( GPIO_PUPDR_PUPDR8  |
										 GPIO_PUPDR_PUPDR9  |
										 GPIO_PUPDR_PUPDR10 );

	GPIOA->PUPDR |= GPIO_PUPDR_PUPDR10_0;
}

// (ILI9341 datasheet pg.11)
// D/C signal is '0' for comman transfer.
void ILI9341_write_cmd(uint32_t cmd)
{
    ILI9341_DC_0();
    SPI1_write(cmd);
}


// (ILI9341 datasheet pg.11)
// D/C signal is '1' for data transfer.
void ILI9341_write_data(uint32_t data)
{
    ILI9341_DC_1();
    SPI1_write(data);
}

void ILI9341_write(uint32_t data)
{
    SPI1_write(data);
}




/*

//( https://github.com/adafruit/Adafruit_ILI9341/blob/master/Adafruit_ILI9341.cpp)
// Initialization sequence for ILI9341

	 
static const uint8_t ILI9341_init_cmd[] = {
  0xEF, 3, 0x03, 0x80, 0x02,
  0xCF, 3, 0x00, 0xC1, 0x30,
  0xED, 4, 0x64, 0x03, 0x12, 0x81,
  0xE8, 3, 0x85, 0x00, 0x78,
  0xCB, 5, 0x39, 0x2C, 0x00, 0x34, 0x02,
  0xF7, 1, 0x20,
  0xEA, 2, 0x00, 0x00,
  ILI9341_PWCTR1  , 1, 0x23,             // Power control VRH[5:0]
  ILI9341_PWCTR2  , 1, 0x10,             // Power control SAP[2:0];BT[3:0]
  ILI9341_VMCTR1  , 2, 0x3e, 0x28,       // VCM control
  ILI9341_VMCTR2  , 1, 0x86,             // VCM control2
  ILI9341_MADCTL  , 1, 0x48,             // Memory Access Control
  ILI9341_VSCRSADD, 1, 0x00,             // Vertical scroll zero
  ILI9341_PIXFMT  , 1, 0x55,
  ILI9341_FRMCTR1 , 2, 0x00, 0x18,
  ILI9341_DFUNCTR , 3, 0x08, 0x82, 0x27, // Display Function Control
  0xF2, 1, 0x00,                         // 3Gamma Function Disable
  ILI9341_GAMMASET , 1, 0x01,             // Gamma curve selected
  ILI9341_GMCTRP1 , 15, 0x0F, 0x31, 0x2B, 0x0C, 0x0E, 0x08, // Set Gamma
    0x4E, 0xF1, 0x37, 0x07, 0x10, 0x03, 0x0E, 0x09, 0x00,
  ILI9341_GMCTRN1 , 15, 0x00, 0x0E, 0x14, 0x03, 0x11, 0x07, // Set Gamma
    0x31, 0xC1, 0x48, 0x08, 0x0F, 0x0C, 0x31, 0x36, 0x0F,
  ILI9341_SLPOUT  , 0x80,                // Exit Sleep
  ILI9341_DISPON  , 0x80,                // Display on
  0x00                                   // End of list
};

*/

void write_cmd(uint8_t cmd)
{
	ILI9341_write_cmd(cmd);
}

void write_data(uint8_t data)
{
	ILI9341_write_data(data);
}


//				  ILI9341_init_cmd[] format
// ----------------------------------------------
// commaned args_count {args} wait_ms_after_cmd
// ----------------------------------------------
static const uint8_t ILI9341_init_cmd[] = {
	0x3A, 1, 0x55, 0,
  // (ILI9341 datasheet pg.134)
	// COLMOD: Pixel Format Set
	// 0x55 is binary '01010101'b => 16bits/pixel
	
	0xB1, 2, 0x0, 0x1F, 0,
  // (ILI9341 datasheet pg.155)
	// FRMCTR1: Frame Rate Control
	// 1st arg: 0x0 => DIVA(division ratio) => 1
	// 2nd arg: 0x1F => RTNA(clocks per line) => 31 clocks
	// !!! ->TRY 0x10 sa RTNA<- !!!

	0x36,	1, 0xE8, 0,
  // (ILI9341 datasheet pg.127)
	// MADCTL: Memory Access Control
	// MY[7]  =>  Row Addess Order
	// MX[6]  =>  Column Address Order
	// MV[5]  =>  Row / Column Exchange
	// ML[4]  =>  Vertical Refresh Order
	// BGR[3] =>  RGB-BGR Order
	// MH[2]	=>  Horizontal Refresh Order
	// 1st arg: 0xE8 => MY MX MV BGR


  0xF2, 1, 0x08, 0,   

 	0x26, 1, 0x01, 0,            // Gamma curve selected
  
	0xE0 , 15, 0x1F, 0x1A, 0x18, 0x0A, 0x0F, 0x06, // Set Gamma
    0x45, 0x87, 0x32, 0x0A, 0x07, 0x02, 0x07, 0x05, 0x00, 0,
  
	0xE1 , 15, 0x00, 0x25, 0x27, 0x05, 0x10, 0x09, // Set Gamma
    0x3A, 0x78, 0x4D, 0x05, 0x18, 0x0D, 0x38, 0x3A, 0x1F, 0,
	


	0x11, 0, 5, 
  // (ILI9341 datasheet pg.101)
	// SLPOUT: Sleep Out
	// This command turns off sleep mode.
	// It is necessary to wait 5msec before sending a next command.

	0x29, 0, 0,
  // (ILI9341 datasheet pg.109)
	// This command is used to recover from DISPLAY OFF mode.
	// Output from the Frame Memory is enabled.

	0x0
};


 
/*-------------------------------------------------------------------*/
/*  TFT reset                                                        */
/*-------------------------------------------------------------------*/
void ILI9341_init()
{
	const uint8_t *commands = ILI9341_init_cmd;
	uint8_t argc;

	init_SPI1();
	init_ILI9341_GPIO();

	/*
    wait_ms(200);
    ILI9341_CS_1();
    ILI9341_DC_1();
	*/

 	// (ILI9341 datasheet pg.11)
	// Reset signal is active low, which mean 0V is logical 1
	// and 3.3V is logical 0. So logic for the RESET pin is
	// inverted.
	ILI9341_RESET_1();
  wait_ms(200);
	
	ILI9341_RESET_0();
  wait_us(10);
   
	ILI9341_RESET_1();
  wait_ms(120);
    
	ILI9341_CS_0();
  wait_ms(10);
 
	while(*commands != 0x0)
	{
			ILI9341_write_cmd(*(commands++));

			if((argc = *(commands++)) != 0)
				while(argc--) ILI9341_write_data(*(commands++));

			wait_ms(*(commands++));
	}

}


void ILI9341_set_window(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1)
{

 	// (ILI9341 datasheet pg.110)
	// CASET: Column Address Set
	// 1st arg: SC[15:8]
	// 2nd arg: SC[7:0]
	// 3rd arg: EC[15:8]
	// 4th arg: EC[7:0]
	// Have to be true: SC[15:0] <= EC[15:0]
	// This command defines area of frame memory where
	// MCU can access/write data.
	// SC -> leftmost column
	// EC -> rightmost column
	ILI9341_write_cmd(0x2A);
  ILI9341_write_data(x0 >> 8);
  ILI9341_write_data(x0);
  ILI9341_write_data(x1 >> 8);
  ILI9341_write_data(x1);


	// (ILI9341 datasheet pg.112)
	// PASET: Page Address Set
	// 1st arg: SP[15:8]
	// 2nd arg: SP[7:0]
	// 3rd arg: EP[15:8]
	// 4th arg: EP[7:0]
	// Have to be true: SP[15:0] <= EP[15:0]
	// This command defines area of frame memory where
	// MCU can access/write data.
	// SC -> top row
	// EC -> bottom row

	ILI9341_write_cmd(0x2B);
  ILI9341_write_data(y0 >> 8);
  ILI9341_write_data(y0);
  ILI9341_write_data(y1 >> 8);
  ILI9341_write_data(y1);

}

void ILI9341_rectangle(uint16_t color, uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1)
{
	uint32_t i;
	uint32_t sz = (((uint32_t)(x1)) - x0)*(y1 - y0);

	ILI9341_set_window(x0, y0, x1, y1);

	// (ILI9341 datasheet pg.114)
	// RAMWR: Memory Write
	// This command is used to transfer data from MCU to frame memory.
	// Column register and the page register are reset to the 
	// Start Column/Start Page position (SC/SP).
	ILI9341_write_cmd(0x2C); // Memory Write

	// Wait to next command finish its execution.
	wait_us(20);
	ILI9341_DC_1();


	for (i = 0u; i < sz; ++i)
	{
		ILI9341_write(color & 0xff);
		ILI9341_write(color >> 8);     
	} 
}











void ILI9341_set_window2(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1)
{

 	// (ILI9341 datasheet pg.110)
	// CASET: Column Address Set
	// 1st arg: SC[15:8]
	// 2nd arg: SC[7:0]
	// 3rd arg: EC[15:8]
	// 4th arg: EC[7:0]
	// Have to be true: SC[15:0] <= EC[15:0]
	// This command defines area of frame memory where
	// MCU can access/write data.
	// SC -> leftmost column
	// EC -> rightmost column
	ILI9341_write_cmd(0x2B);
  ILI9341_write_data(x0 >> 8);
  ILI9341_write_data(x0);
  ILI9341_write_data(x1 >> 8);
  ILI9341_write_data(x1);


	// (ILI9341 datasheet pg.112)
	// PASET: Page Address Set
	// 1st arg: SP[15:8]
	// 2nd arg: SP[7:0]
	// 3rd arg: EP[15:8]
	// 4th arg: EP[7:0]
	// Have to be true: SP[15:0] <= EP[15:0]
	// This command defines area of frame memory where
	// MCU can access/write data.
	// SC -> top row
	// EC -> bottom row

	ILI9341_write_cmd(0x2A);
  ILI9341_write_data(y0 >> 8);
  ILI9341_write_data(y0);
  ILI9341_write_data(y1 >> 8);
  ILI9341_write_data(y1);

}




void ILI9341_image(char img[], uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1)
{
	uint32_t i;
	uint32_t sz = (((uint32_t)(x1)) - x0)*(y1 - y0);

	ILI9341_set_window(x0, y0, x1-1, y1-1);


	// (ILI9341 datasheet pg.114)
	// RAMWR: Memory Write
	// This command is used to transfer data from MCU to frame memory.
	// Column register and the page register are reset to the 
	// Start Column/Start Page position (SC/SP).
	ILI9341_write_cmd(0x2C); // Memory Write

	// Wait to next command finish its execution.
	wait_us(20);
	ILI9341_DC_1();


	
	for (i = 0u; i < sz; ++i)
	{	
		ILI9341_write(img[2*i]);     
		ILI9341_write(img[2*i + 1]);     
	}
	
}




