	.syntax unified
	.cpu cortex-m4
	.eabi_attribute 27, 3
	.eabi_attribute 28, 1
	.fpu fpv4-sp-d16
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 1
	.eabi_attribute 30, 6
	.eabi_attribute 34, 1
	.eabi_attribute 18, 4
	.thumb
	.file	"line_draw.c"
	.text
.Ltext0:
	.cfi_sections	.debug_frame
	.align	2
	.global	myPixel
	.thumb
	.thumb_func
	.type	myPixel, %function
myPixel:
.LFB0:
	.file 1 "line_draw.c"
	.loc 1 17 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	push	{r7}
	.cfi_def_cfa_offset 4
	.cfi_offset 7, -4
	sub	sp, sp, #28
	.cfi_def_cfa_offset 32
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	str	r0, [r7, #12]
	str	r1, [r7, #8]
	str	r2, [r7, #4]
	.loc 1 19 0
	ldr	r2, [r7, #8]
	mov	r3, r2
	lsls	r3, r3, #4
	subs	r3, r3, r2
	lsls	r3, r3, #4
	mov	r2, r3
	ldr	r3, [r7, #4]
	add	r3, r3, r2
	lsls	r3, r3, #1
	str	r3, [r7, #20]
	.loc 1 21 0
	ldr	r3, [r7, #20]
	ldr	r2, [r7, #12]
	add	r3, r3, r2
	ldr	r2, .L2
	ldrb	r2, [r2]	@ zero_extendqisi2
	strb	r2, [r3]
	.loc 1 22 0
	ldr	r3, [r7, #20]
	adds	r3, r3, #1
	ldr	r2, [r7, #12]
	add	r3, r3, r2
	ldr	r2, .L2+4
	ldrb	r2, [r2]	@ zero_extendqisi2
	strb	r2, [r3]
	.loc 1 23 0
	adds	r7, r7, #28
	.cfi_def_cfa_offset 4
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	ldr	r7, [sp], #4
	.cfi_restore 7
	.cfi_def_cfa_offset 0
	bx	lr
.L3:
	.align	2
.L2:
	.word	color_hi
	.word	color_lo
	.cfi_endproc
.LFE0:
	.size	myPixel, .-myPixel
	.align	2
	.global	myLine
	.thumb
	.thumb_func
	.type	myLine, %function
myLine:
.LFB1:
	.loc 1 28 0
	.cfi_startproc
	@ args = 4, pretend = 0, frame = 40
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r7, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	sub	sp, sp, #40
	.cfi_def_cfa_offset 48
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	str	r0, [r7, #12]
	str	r1, [r7, #8]
	str	r2, [r7, #4]
	str	r3, [r7]
	.loc 1 30 0
	movs	r3, #0
	strb	r3, [r7, #35]
	.loc 1 32 0
	ldr	r2, [r7, #48]
	ldr	r3, [r7, #4]
	subs	r3, r2, r3
	str	r3, [r7, #28]
	.loc 1 33 0
	ldr	r2, [r7]
	ldr	r3, [r7, #8]
	subs	r3, r2, r3
	str	r3, [r7, #24]
	.loc 1 34 0
	ldr	r3, [r7, #28]
	eor	r2, r3, r3, asr #31
	sub	r2, r2, r3, asr #31
	ldr	r3, [r7, #24]
	cmp	r3, #0
	it	lt
	rsblt	r3, r3, #0
	cmp	r2, r3
	ble	.L5
.LBB2:
	.loc 1 35 0
	ldr	r3, [r7, #28]
	str	r3, [r7, #16]
	.loc 1 36 0
	ldr	r3, [r7, #24]
	str	r3, [r7, #28]
	.loc 1 37 0
	ldr	r3, [r7, #16]
	str	r3, [r7, #24]
	.loc 1 38 0
	movs	r3, #1
	strb	r3, [r7, #35]
.L5:
.LBE2:
	.loc 1 41 0
	ldr	r3, [r7, #24]
	cmp	r3, #0
	bne	.L6
	.loc 1 41 0 is_stmt 0 discriminator 1
	movs	r3, #0
	str	r3, [r7, #20]
	b	.L7
.L6:
	.loc 1 42 0 is_stmt 1
	ldr	r3, [r7, #28]
	lsls	r2, r3, #8
	ldr	r3, [r7, #24]
	sdiv	r3, r2, r3
	str	r3, [r7, #20]
.L7:
	.loc 1 44 0
	ldrb	r3, [r7, #35]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L8
	.loc 1 45 0
	ldr	r3, [r7, #24]
	cmp	r3, #0
	ble	.L9
	.loc 1 46 0
	ldr	r2, [r7, #24]
	ldr	r3, [r7, #4]
	add	r3, r3, r2
	str	r3, [r7, #24]
	.loc 1 47 0
	ldr	r3, [r7, #8]
	lsls	r3, r3, #8
	adds	r3, r3, #128
	str	r3, [r7, #36]
	b	.L10
.L11:
	.loc 1 48 0 discriminator 3
	ldr	r3, [r7, #36]
	asrs	r3, r3, #8
	ldr	r0, [r7, #12]
	mov	r1, r3
	ldr	r2, [r7, #4]
	bl	myPixel
	.loc 1 49 0 discriminator 3
	ldr	r2, [r7, #36]
	ldr	r3, [r7, #20]
	add	r3, r3, r2
	str	r3, [r7, #36]
	.loc 1 47 0 discriminator 3
	ldr	r3, [r7, #4]
	adds	r3, r3, #1
	str	r3, [r7, #4]
.L10:
	.loc 1 47 0 is_stmt 0 discriminator 1
	ldr	r2, [r7, #4]
	ldr	r3, [r7, #24]
	cmp	r2, r3
	ble	.L11
	.loc 1 51 0 is_stmt 1
	b	.L4
.L9:
	.loc 1 53 0
	ldr	r2, [r7, #24]
	ldr	r3, [r7, #4]
	add	r3, r3, r2
	str	r3, [r7, #24]
	.loc 1 54 0
	ldr	r3, [r7, #8]
	lsls	r3, r3, #8
	adds	r3, r3, #128
	str	r3, [r7, #36]
	b	.L13
.L14:
	.loc 1 55 0 discriminator 3
	ldr	r3, [r7, #36]
	asrs	r3, r3, #8
	ldr	r0, [r7, #12]
	mov	r1, r3
	ldr	r2, [r7, #4]
	bl	myPixel
	.loc 1 56 0 discriminator 3
	ldr	r2, [r7, #36]
	ldr	r3, [r7, #20]
	subs	r3, r2, r3
	str	r3, [r7, #36]
	.loc 1 54 0 discriminator 3
	ldr	r3, [r7, #4]
	subs	r3, r3, #1
	str	r3, [r7, #4]
.L13:
	.loc 1 54 0 is_stmt 0 discriminator 1
	ldr	r2, [r7, #4]
	ldr	r3, [r7, #24]
	cmp	r2, r3
	bge	.L14
	.loc 1 58 0 is_stmt 1 discriminator 2
	b	.L4
.L8:
	.loc 1 61 0
	ldr	r3, [r7, #24]
	cmp	r3, #0
	ble	.L15
	.loc 1 62 0
	ldr	r2, [r7, #24]
	ldr	r3, [r7, #8]
	add	r3, r3, r2
	str	r3, [r7, #24]
	.loc 1 63 0
	ldr	r3, [r7, #4]
	lsls	r3, r3, #8
	adds	r3, r3, #128
	str	r3, [r7, #36]
	b	.L16
.L17:
	.loc 1 64 0 discriminator 3
	ldr	r3, [r7, #36]
	asrs	r3, r3, #8
	ldr	r0, [r7, #12]
	ldr	r1, [r7, #8]
	mov	r2, r3
	bl	myPixel
	.loc 1 65 0 discriminator 3
	ldr	r2, [r7, #36]
	ldr	r3, [r7, #20]
	add	r3, r3, r2
	str	r3, [r7, #36]
	.loc 1 63 0 discriminator 3
	ldr	r3, [r7, #8]
	adds	r3, r3, #1
	str	r3, [r7, #8]
.L16:
	.loc 1 63 0 is_stmt 0 discriminator 1
	ldr	r2, [r7, #8]
	ldr	r3, [r7, #24]
	cmp	r2, r3
	ble	.L17
	.loc 1 67 0 is_stmt 1 discriminator 3
	b	.L4
.L15:
	.loc 1 69 0
	ldr	r2, [r7, #24]
	ldr	r3, [r7, #8]
	add	r3, r3, r2
	str	r3, [r7, #24]
	.loc 1 70 0
	ldr	r3, [r7, #4]
	lsls	r3, r3, #8
	adds	r3, r3, #128
	str	r3, [r7, #36]
	b	.L18
.L19:
	.loc 1 71 0 discriminator 3
	ldr	r3, [r7, #36]
	asrs	r3, r3, #8
	ldr	r0, [r7, #12]
	ldr	r1, [r7, #8]
	mov	r2, r3
	bl	myPixel
	.loc 1 72 0 discriminator 3
	ldr	r2, [r7, #36]
	ldr	r3, [r7, #20]
	subs	r3, r2, r3
	str	r3, [r7, #36]
	.loc 1 70 0 discriminator 3
	ldr	r3, [r7, #8]
	subs	r3, r3, #1
	str	r3, [r7, #8]
.L18:
	.loc 1 70 0 is_stmt 0 discriminator 1
	ldr	r2, [r7, #8]
	ldr	r3, [r7, #24]
	cmp	r2, r3
	bge	.L19
.L4:
	.loc 1 75 0 is_stmt 1
	adds	r7, r7, #40
	.cfi_def_cfa_offset 8
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	pop	{r7, pc}
	.cfi_endproc
.LFE1:
	.size	myLine, .-myLine
	.align	2
	.global	mySquare
	.thumb
	.thumb_func
	.type	mySquare, %function
mySquare:
.LFB2:
	.loc 1 77 0
	.cfi_startproc
	@ args = 4, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r4, r5, r7, lr}
	.cfi_def_cfa_offset 16
	.cfi_offset 4, -16
	.cfi_offset 5, -12
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	sub	sp, sp, #24
	.cfi_def_cfa_offset 40
	add	r7, sp, #8
	.cfi_def_cfa 7, 32
	str	r0, [r7, #12]
	str	r1, [r7, #8]
	str	r2, [r7, #4]
	str	r3, [r7]
	.loc 1 78 0
	ldr	r3, [r7, #32]
	str	r3, [sp]
	ldr	r0, [r7, #12]
	ldr	r1, [r7, #8]
	ldr	r2, [r7, #4]
	ldr	r3, [r7]
	bl	myLine
	.loc 1 79 0
	ldr	r2, [r7, #4]
	ldr	r3, [r7, #32]
	subs	r2, r2, r3
	ldr	r3, [r7]
	adds	r4, r2, r3
	ldr	r2, [r7]
	ldr	r3, [r7, #8]
	subs	r2, r2, r3
	ldr	r3, [r7, #32]
	add	r3, r3, r2
	str	r3, [sp]
	ldr	r0, [r7, #12]
	ldr	r1, [r7]
	ldr	r2, [r7, #32]
	mov	r3, r4
	bl	myLine
	.loc 1 80 0
	ldr	r2, [r7, #4]
	ldr	r3, [r7, #32]
	subs	r2, r2, r3
	ldr	r3, [r7, #8]
	adds	r4, r2, r3
	ldr	r2, [r7]
	ldr	r3, [r7, #8]
	subs	r2, r2, r3
	ldr	r3, [r7, #4]
	add	r3, r3, r2
	str	r3, [sp]
	ldr	r0, [r7, #12]
	ldr	r1, [r7, #8]
	ldr	r2, [r7, #4]
	mov	r3, r4
	bl	myLine
	.loc 1 81 0
	ldr	r2, [r7, #4]
	ldr	r3, [r7, #32]
	subs	r2, r2, r3
	ldr	r3, [r7, #8]
	adds	r1, r2, r3
	ldr	r2, [r7]
	ldr	r3, [r7, #8]
	subs	r2, r2, r3
	ldr	r3, [r7, #4]
	adds	r5, r2, r3
	ldr	r2, [r7, #4]
	ldr	r3, [r7, #32]
	subs	r2, r2, r3
	ldr	r3, [r7]
	adds	r4, r2, r3
	ldr	r2, [r7]
	ldr	r3, [r7, #8]
	subs	r2, r2, r3
	ldr	r3, [r7, #32]
	add	r3, r3, r2
	str	r3, [sp]
	ldr	r0, [r7, #12]
	mov	r2, r5
	mov	r3, r4
	bl	myLine
	.loc 1 82 0
	adds	r7, r7, #16
	.cfi_def_cfa_offset 16
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	pop	{r4, r5, r7, pc}
	.cfi_endproc
.LFE2:
	.size	mySquare, .-mySquare
	.align	2
	.global	myRect
	.thumb
	.thumb_func
	.type	myRect, %function
myRect:
.LFB3:
	.loc 1 85 0
	.cfi_startproc
	@ args = 4, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r7, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	sub	sp, sp, #24
	.cfi_def_cfa_offset 32
	add	r7, sp, #8
	.cfi_def_cfa 7, 24
	str	r0, [r7, #12]
	str	r1, [r7, #8]
	str	r2, [r7, #4]
	str	r3, [r7]
	.loc 1 86 0
	ldr	r3, [r7, #4]
	str	r3, [sp]
	ldr	r0, [r7, #12]
	ldr	r1, [r7, #8]
	ldr	r2, [r7, #4]
	ldr	r3, [r7]
	bl	myLine
	.loc 1 87 0
	ldr	r3, [r7, #24]
	str	r3, [sp]
	ldr	r0, [r7, #12]
	ldr	r1, [r7]
	ldr	r2, [r7, #4]
	ldr	r3, [r7]
	bl	myLine
	.loc 1 88 0
	ldr	r3, [r7, #24]
	str	r3, [sp]
	ldr	r0, [r7, #12]
	ldr	r1, [r7]
	ldr	r2, [r7, #24]
	ldr	r3, [r7, #8]
	bl	myLine
	.loc 1 89 0
	ldr	r3, [r7, #4]
	str	r3, [sp]
	ldr	r0, [r7, #12]
	ldr	r1, [r7, #8]
	ldr	r2, [r7, #24]
	ldr	r3, [r7, #8]
	bl	myLine
	.loc 1 90 0
	adds	r7, r7, #16
	.cfi_def_cfa_offset 8
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	pop	{r7, pc}
	.cfi_endproc
.LFE3:
	.size	myRect, .-myRect
.Letext0:
	.file 2 "line_draw.h"
	.file 3 "<built-in>"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x222
	.2byte	0x4
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF16
	.byte	0x1
	.4byte	.LASF17
	.4byte	.LASF18
	.4byte	.Ltext0
	.4byte	.Letext0-.Ltext0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x2
	.byte	0x4
	.4byte	0x30
	.uleb128 0x3
	.byte	0x1
	.byte	0x8
	.4byte	.LASF2
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x2
	.byte	0x5
	.4byte	0x42
	.uleb128 0x3
	.byte	0x1
	.byte	0x8
	.4byte	.LASF3
	.uleb128 0x4
	.4byte	.LASF5
	.byte	0x1
	.byte	0x11
	.4byte	.LFB0
	.4byte	.LFE0-.LFB0
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x93
	.uleb128 0x5
	.4byte	.LASF4
	.byte	0x1
	.byte	0x11
	.4byte	0x93
	.uleb128 0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x6
	.ascii	"x\000"
	.byte	0x1
	.byte	0x11
	.4byte	0x99
	.uleb128 0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x6
	.ascii	"y\000"
	.byte	0x1
	.byte	0x11
	.4byte	0x99
	.uleb128 0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x7
	.ascii	"idx\000"
	.byte	0x1
	.byte	0x13
	.4byte	0x99
	.uleb128 0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x8
	.byte	0x4
	.4byte	0x25
	.uleb128 0x9
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0xa
	.4byte	.LASF6
	.byte	0x1
	.byte	0x1c
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x163
	.uleb128 0x5
	.4byte	.LASF4
	.byte	0x1
	.byte	0x1c
	.4byte	0x93
	.uleb128 0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0x6
	.ascii	"x\000"
	.byte	0x1
	.byte	0x1c
	.4byte	0x99
	.uleb128 0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x6
	.ascii	"y\000"
	.byte	0x1
	.byte	0x1c
	.4byte	0x99
	.uleb128 0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x6
	.ascii	"x2\000"
	.byte	0x1
	.byte	0x1c
	.4byte	0x99
	.uleb128 0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x6
	.ascii	"y2\000"
	.byte	0x1
	.byte	0x1c
	.4byte	0x99
	.uleb128 0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x7
	.ascii	"j\000"
	.byte	0x1
	.byte	0x1d
	.4byte	0x99
	.uleb128 0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0xb
	.4byte	.LASF7
	.byte	0x1
	.byte	0x1e
	.4byte	0x37
	.uleb128 0x2
	.byte	0x91
	.sleb128 -13
	.uleb128 0xb
	.4byte	.LASF8
	.byte	0x1
	.byte	0x20
	.4byte	0x99
	.uleb128 0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0xb
	.4byte	.LASF9
	.byte	0x1
	.byte	0x21
	.4byte	0x99
	.uleb128 0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0xc
	.ascii	"abs\000"
	.byte	0x3
	.byte	0
	.4byte	0x99
	.4byte	0x13c
	.uleb128 0xd
	.byte	0
	.uleb128 0xb
	.4byte	.LASF10
	.byte	0x1
	.byte	0x28
	.4byte	0x99
	.uleb128 0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0xe
	.4byte	.LBB2
	.4byte	.LBE2-.LBB2
	.uleb128 0xb
	.4byte	.LASF11
	.byte	0x1
	.byte	0x23
	.4byte	0x99
	.uleb128 0x2
	.byte	0x91
	.sleb128 -32
	.byte	0
	.byte	0
	.uleb128 0xa
	.4byte	.LASF12
	.byte	0x1
	.byte	0x4d
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x1b9
	.uleb128 0x5
	.4byte	.LASF4
	.byte	0x1
	.byte	0x4d
	.4byte	0x93
	.uleb128 0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x6
	.ascii	"x\000"
	.byte	0x1
	.byte	0x4d
	.4byte	0x99
	.uleb128 0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x6
	.ascii	"y\000"
	.byte	0x1
	.byte	0x4d
	.4byte	0x99
	.uleb128 0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0x6
	.ascii	"x2\000"
	.byte	0x1
	.byte	0x4d
	.4byte	0x99
	.uleb128 0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x6
	.ascii	"y2\000"
	.byte	0x1
	.byte	0x4d
	.4byte	0x99
	.uleb128 0x2
	.byte	0x91
	.sleb128 0
	.byte	0
	.uleb128 0xa
	.4byte	.LASF13
	.byte	0x1
	.byte	0x55
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x20f
	.uleb128 0x5
	.4byte	.LASF4
	.byte	0x1
	.byte	0x55
	.4byte	0x93
	.uleb128 0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x6
	.ascii	"x\000"
	.byte	0x1
	.byte	0x55
	.4byte	0x99
	.uleb128 0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x6
	.ascii	"y\000"
	.byte	0x1
	.byte	0x55
	.4byte	0x99
	.uleb128 0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0x6
	.ascii	"x2\000"
	.byte	0x1
	.byte	0x55
	.4byte	0x99
	.uleb128 0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x6
	.ascii	"y2\000"
	.byte	0x1
	.byte	0x55
	.4byte	0x99
	.uleb128 0x2
	.byte	0x91
	.sleb128 0
	.byte	0
	.uleb128 0xf
	.4byte	.LASF14
	.byte	0x2
	.byte	0xc
	.4byte	0x30
	.uleb128 0xf
	.4byte	.LASF15
	.byte	0x2
	.byte	0xd
	.4byte	0x30
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2116
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x1c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.Ltext0
	.4byte	.Letext0-.Ltext0
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF8:
	.ascii	"shortLen\000"
.LASF9:
	.ascii	"longLen\000"
.LASF11:
	.ascii	"swap\000"
.LASF14:
	.ascii	"color_hi\000"
.LASF3:
	.ascii	"unsigned char\000"
.LASF16:
	.ascii	"GNU C 4.9.3 20141119 (release) [ARM/embedded-4_9-br"
	.ascii	"anch revision 218278] -mlittle-endian -mthumb -mcpu"
	.ascii	"=cortex-m4 -mthumb-interwork -mfloat-abi=hard -mfpu"
	.ascii	"=fpv4-sp-d16 -g -O0 -fsingle-precision-constant\000"
.LASF7:
	.ascii	"yLonger\000"
.LASF1:
	.ascii	"bool\000"
.LASF13:
	.ascii	"myRect\000"
.LASF5:
	.ascii	"myPixel\000"
.LASF17:
	.ascii	"line_draw.c\000"
.LASF2:
	.ascii	"char\000"
.LASF18:
	.ascii	"/home/sharpovici-ostrakovici/stm_dev/STM32F407/exer"
	.ascii	"cise/project\000"
.LASF0:
	.ascii	"SURFACE\000"
.LASF4:
	.ascii	"surface\000"
.LASF10:
	.ascii	"decInc\000"
.LASF15:
	.ascii	"color_lo\000"
.LASF6:
	.ascii	"myLine\000"
.LASF12:
	.ascii	"mySquare\000"
	.ident	"GCC: (GNU Tools for ARM Embedded Processors) 4.9.3 20141119 (release) [ARM/embedded-4_9-branch revision 218278]"
